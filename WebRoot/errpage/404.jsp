<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>404 Not Found</title>
  </head>
  <body>
  </body>
  <script type="text/javascript">
  	var width = document.documentElement.clientWidth + "px";
  	document.body.setAttribute("style", "background:url('errpage/404.jpg') no-repeat center;background-size:"+ width +";");
  </script>
</html>
