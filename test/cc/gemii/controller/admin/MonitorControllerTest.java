package cc.gemii.controller.admin;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import cc.gemii.AbstractBaseControllerTest;

public class MonitorControllerTest extends AbstractBaseControllerTest{

	@Test
	public void searchMonitorRoom_test(){
		MockHttpServletRequestBuilder req = MockMvcRequestBuilders.get("/admin/operategroup/search")
				.param("page", "3")
				.param("size","20");
		try {
			MvcResult result = mockMvc.perform(req).andReturn();
			MockHttpServletResponse response = result.getResponse();
			System.out.println(response.getContentAsString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
