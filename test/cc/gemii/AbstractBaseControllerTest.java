package cc.gemii;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-*.xml","classpath:spring/springmvc.xml"})
@WebAppConfiguration
public abstract class AbstractBaseControllerTest {

	@Autowired  
    private WebApplicationContext webApplicationContext;
	
	protected MockMvc mockMvc ;  
    
    @Before  
    public void setUp() throws Exception  
    {  
        mockMvc = MockMvcBuilders.webAppContextSetup( webApplicationContext ).build();  
    }  
}
