package cc.gemii;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractBaseStressTest extends  AbstractBaseTest{
	
	protected abstract int getThreadCount();
	
	protected abstract void runMehtod();

	protected void runTestCase(){
		List<Thread> threadList = new ArrayList<Thread>();
		for(int i = 0; i < getThreadCount();i++){
			threadList.add(new Thread(new Runnable() {
				@Override
				public void run() {
					runMehtod();
				}
			}));
		}
		for (Thread thread : threadList) {
			thread.start();
		}
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
