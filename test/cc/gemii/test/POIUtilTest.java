package cc.gemii.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cc.gemii.AbstractBaseTest;
import cc.gemii.pojo.RoomInfo;
import cc.gemii.service.RoomService;
import cc.gemii.util.POIUtil;

public class POIUtilTest extends AbstractBaseTest {

	@Autowired
	private RoomService roomService;
	
	@Test
	public void readExcel_test() throws FileNotFoundException{
		String fileName = "utilTest.xlsx";
		String filePath = "/Users/yk/Desktop/";
		FileInputStream fis = new FileInputStream(new File(filePath+fileName));
		List<POITestObj> result = POIUtil.readExcel(fileName, fis, POITestObj.class);
		for (Object object : result) {
			System.out.println(object);
		}
	}
	
	@Test
	public void exportExcel_test() throws FileNotFoundException{
		String fileName = "ddexport.xls";
		String filePath = "/Users/yk/Desktop/";
		File file = new File(filePath+fileName);
		String[] headers = {"群ID","群名","由创群Id","群成员人数"};
		String title = "爱婴岛群成员统计表";
		List<RoomInfo> roomInfoList = roomService.findRoomStatisticsInfo();
		try {
			POIUtil.exportExcel(title, headers, roomInfoList,new FileOutputStream(file));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
