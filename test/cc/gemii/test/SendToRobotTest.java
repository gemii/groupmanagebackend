package cc.gemii.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;

import cc.gemii.AbstractBaseTest;
import cc.gemii.mapper.custom.RobotMapper;
import cc.gemii.po.Monitorrobotbind;
import cc.gemii.pojo.SendMessageBoundary;
import cc.gemii.service.MonitorService;
import cc.gemii.service.impl.MonitorServiceImpl;
import cc.gemii.util.RedisUtil;

public class SendToRobotTest extends AbstractBaseTest {
	

	
	@Autowired
	private MonitorService monitorService;
	
	@Autowired
	private RobotMapper robotMapper;
	
//	http://nfs.gemii.cc/microtask/nbYke28M98gkeydk.png
	
	@Test
	public void test(){
		String[] roomids=new String[]{"TEST0030","TEST0031","TEST0032"
				,"TEST0033","TEST0034","TEST0035","TEST0036","TEST0037"
				,"TEST0038","TEST0039","TEST0040","TEST0041","TEST0042"};
		Map<String, Object> tagMap = this.sendRobotClassify(22, roomids);
		
		try {
			this.sendToRobot(tagMap, 2, "http://nfs.gemii.cc/microtask/nbYke28M98gkeydk.png", "nbYke28M98gkeydk.png");
//			this.sendToRobot(tagMap, 2, "http://nfs.gemii.cc/microtask/nbYke28M98gkeydk.png", "nbYke28M98gkeydk");
//			this.sendToRobot(tagMap, 2, "http://nfs.gemii.cc/microtask/nbYke28M98gkeydk.png", "nbYke28M98gkeydk");
//			this.sendToRobot(tagMap, 2, "http://nfs.gemii.cc/microtask/nbYke28M98gkeydk.png", "nbYke28M98gkeydk");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void sendToRobot(Map<String, Object> tagMap, Integer MsgType, String content, String fileName) throws Exception{
		Set<Entry<String, Object>> set = tagMap.entrySet();
		for (Entry<String, Object> entry : set) {
			Map<String, Object> sendMap = new HashMap<String, Object>();
			sendMap.put("MsgType", MsgType);
			Map<String, Object> Msg = new HashMap<String, Object>();
			Msg.put("Uin", robotMapper.getRobotIDByRobotTag(entry.getKey()));
			Msg.put("RoomIDs", (List<String>)entry.getValue());
			Msg.put("Content", content);
			Msg.put("FileName", fileName);
			sendMap.put("Msg", Msg);
			RedisUtil.publishMessage(entry.getKey() + "_", JSON.toJSONString(sendMap));
			System.out.println(entry.getKey() + "  aa  " +sendMap);
		}
	}
	
	private Map<String, Object> sendRobotClassify(Integer mID, String[] roomIDs){
		Map<String, Object> tagMap = new HashMap<String, Object>();
		String tag = "";
		Monitorrobotbind bind = null;
		List<String> classifyRoomIDs = null;
		Map<String, Object> param = null;
		for (String roomID : roomIDs) {
			param = new HashMap<String, Object>();
			param.put("mID", mID);
			param.put("roomID", roomID);
			bind = robotMapper.getSendRobotByRoomID(param);
			if(bind == null)
				continue ;
			tag = bind.getRobottag();
			if(tagMap.containsKey(tag)){
				classifyRoomIDs = (List<String>) tagMap.get(tag);
				classifyRoomIDs.add(roomID);
				tagMap.put(tag, classifyRoomIDs);
			}else {
				classifyRoomIDs = new ArrayList<String>();
				classifyRoomIDs.add(roomID);
				tagMap.put(tag, classifyRoomIDs);
			}
		}
		return tagMap;
	}
}
