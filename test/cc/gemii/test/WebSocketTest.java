package cc.gemii.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cc.gemii.service.KeywordService;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.MD5Utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class WebSocketTest {

	@Test
	public void sendTest() throws IOException {
		System.out.println(MD5Utils.md5Encode("gemii@123.cc"));
	}

	@Test
	public void fileTest() {
		try {
			File file = new File("");
			System.out.println(file.getCanonicalPath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void keywordTest() {
		ApplicationContext ac = new ClassPathXmlApplicationContext(
				"classpath:spring/applicationContext-*.xml");
		KeywordService keywordService = ac.getBean(KeywordService.class);
		List<String> keywords = keywordService.getKeywordByRoomID("DS00000190");
		for (String string : keywords) {
			System.out.println(string);
		}
	}

	@Test
	public void sendMsgTest() {
		
		String uin = "211150211";
		String[] roomIDs = { "TEST0033"};
		Map<String, Object> sendMap = new HashMap<String, Object>();
		sendMap.put("MsgType", 1);
		Map<String, Object> Msg = new HashMap<String, Object>();
		Msg.put("Uin", uin);
		Msg.put("RoomIDs", Arrays.asList(roomIDs));
		Msg.put("Content", "测试消息是否发送成功！");
		sendMap.put("Msg", Msg);
		System.out.println(JSON.toJSONString(sendMap));
	}

	@Test
	public void lightTest() {
		boolean[] light = { true, true, true, true, true, true, true, true,
				true, true, true, true, true, true, true, true, true, true,
				true, true };
		for (int i = 1; i <= 20; i++) {
			int temp = i;
			while (temp <= 20) {
				if (temp % i == 0) {
					System.out.println(light[0]);
					light[temp - 1] = !light[temp - 1];
				}
				temp++;
			}
		}
		int i = 0;
		for (boolean b : light) {
			++i;
			if (b == false) {
				System.out.println(i);
			}
		}
	}

	@Test
	public void md5test() {
		System.out.println(MD5Utils.md5Encode("woshi39"));
	}

	@Test
	public void integerTest() {
		Integer i = 200001;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("i", 200001);
		System.out.println(200001 == (Integer) map.get("i"));
	}

	@Test
	public void wyethKickTest() {
		String soapRequestData = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				+ "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
				+ "<soap12:Body>"
				+ " <GetGroupKickNumber xmlns=\"http://tempuri.org/\">"
				+ " <token>aspx.2309ikdml</token>"
				+ " <beginDate>2017-02-08</beginDate>"
				+ " <endDate>2017-02-08</endDate>" + "</GetGroupKickNumber>"
				+ " </soap12:Body>" + "</soap12:Envelope>";
		String res = HttpClientUtil
				.doSOAP("http://crmapi.etocrm.com:9944/mgservice.asmx?op=GetGroupKickNumber",
						soapRequestData);
		String json = res.substring(res.indexOf("{"), res.lastIndexOf("}") + 1);
		Map<String, Object> map = (Map<String, Object>) JSONObject.parse(json);
		System.out.println(map);
	}

	@Test
	public void uploadTest() {
		try {
			CloseableHttpClient client = HttpClients.createDefault();
			String textFileName = "/Users/jinzili/zili.jin/chat_record/ChatRecord_20170225.zip";
			File file = new File(textFileName);
			HttpPost post = new HttpPost(
					"http://file.gemii.cc:8080/apps/files/ajax/upload.php");

			post.setHeader(
					"Cookie",
					"oc_sessionPassphrase=7%2BMCYoqAztioji7Z0m%2FXGvNLszedtgmBNd50I3VbNybzVDrWfue3TlODMH89mHa8PfIcsO%2BSpue0BT1cMEDLcQWTWIQKoMVGiZ67%2BzzgpTW58O%2BVNkSc5QiO4B%2FZosDh; ocs15hj2rq9m=hv4ljijmeq5tkfqmcgbe9d4su5");
			post.setHeader(
					"requesttoken",
					"ABMhCQRgFHsCNzIeHSgyMg0oBSIFfy02BRRcCwAPHmk=:uxy0O/f5cGpGEKxUdCDh65Zqwp1IFj/FgUVnYli4v2Y=");
			post.setHeader("Content-Type",
					"multipart/form-data; boundary=----WebKitFormBoundaryYraS9EKcA9lPLBlF");
			post.setHeader("OCS-APIREQUEST", "true");
			post.setHeader("X-Requested-With", "XMLHttpRequest");
			post.setHeader("Content-Disposition", "form-data; name='/test'");
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addBinaryBody("files[]", new FileInputStream(file),
					ContentType.DEFAULT_BINARY, "ChatRecord_20170225.zip");
			HttpEntity entity = builder.build();
			//
			post.setEntity(entity);
			HttpResponse response = client.execute(post);
			String resultString = EntityUtils.toString(response.getEntity(),
					"utf-8");
			System.out.println(resultString);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
