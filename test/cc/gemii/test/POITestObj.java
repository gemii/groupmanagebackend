package cc.gemii.test;

public class POITestObj {
	
	private String feild1;
	private String feild2;
	
	public String getFeild1() {
		return feild1;
	}
	public void setFeild1(String feild1) {
		this.feild1 = feild1;
	}
	public String getFeild2() {
		return feild2;
	}
	public void setFeild2(String feild2) {
		this.feild2 = feild2;
	}
	@Override
	public String toString() {
		return "POITestObj [feild1=" + feild1 + ", feild2=" + feild2 + "]";
	}
	
}
	
