package cc.gemii.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;

import cc.gemii.AbstractBaseTest;
import cc.gemii.pojo.SendMessageBoundary;

public class KeywordServiceImplTest extends AbstractBaseTest{

	@Autowired
	private KeywordService keywordService;
	
	@Autowired
	private Properties propertyConfigurer;
	
	@Test
	public void testKeyWord(){
		List<String> keywords = keywordService.getKeywordByRoomID("LB0069");
		StringBuffer sb  = new StringBuffer();
		for(String str:keywords){
			sb.append(str+",");
		}
		String words = sb.toString();
		words = words.substring(0,words.length()-1);
		System.out.println(words);
	}
	
	@Test
	public void openHaoqiKeyword(){
		String open = propertyConfigurer.getProperty("haoqi_keywords_switch");
		System.out.println(open);
	}
	
	@Test
	public void getKeywordByRoomID_test(){
		List<String> keywords = keywordService.getKeywordByRoomID("DS00000190");
		for (String string : keywords) {
			System.out.println(string);
		}
		
//		SendMessageBoundary sb=new SendMessageBoundary();
//		List<String> info=new ArrayList<>();
//		
//		info.add("DS11372");
//		info.add("DS10961");
//		
//		Map<String, Object> sendMap = new HashMap<String, Object>();
//		sendMap.put("MsgType", 2);
//		Map<String, Object> Msg = new HashMap<String, Object>();
//		Msg.put("Uin", "122661399"); 
//		Msg.put("RoomIDs", info);
//		Msg.put("Content", "http://nfs.gemii.cc/microtask/XcmnVSmO72X9yyHh.png"); 
//		Msg.put("FileName", "XcmnVSmO72X9yyHh");
//		sendMap.put("Msg", Msg);
//		System.out.println(JSON.toJSONString(sendMap));
		
//		{"Msg":{"Uin":"122661399","FileName":"XcmnVSmO72X9yyHh","RoomIDs":["DS10957","DS10961","DS10963","DS10966","DS10969","DS10972","DS10980","DS10983","DS10985","DS11016","DS11034","DS11039","DS11041","DS11045","DS11073","DS11097","DS11101","DS11103","DS11105","DS11117","DS11122","DS11142","DS11151","DS11154","DS11171","DS11178","DS11186","DS11209","DS11240","DS11274","DS11294","DS11295","DS11315","DS11347","DS11348","DS11353","DS11357","DS11372","DS11390","DS11394","DS11395","DS11397","DS11402","DS11412","DS11413","DS11423","DS11431","DS11441","DS11450","DS11453","DS11457","DS11540","DS11632","DS11643","DS11646"],"Content":"http://nfs.gemii.cc/microtask/XcmnVSmO72X9yyHh.png"},"MsgType":2}
		
	}
}
