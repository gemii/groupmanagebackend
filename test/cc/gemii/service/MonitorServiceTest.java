package cc.gemii.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.AbstractBaseTest;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.admin.ImportedMonitorRoomItem;
import cc.gemii.util.POIUtil;

public class MonitorServiceTest  extends AbstractBaseTest{


	@Autowired
	private MonitorService monitorService;
	
	@Test
	@Rollback(true)
	public void importMonitorRoom_test() throws FileNotFoundException{
//		String fileName = "dd1.xlsx";
//		String filePath = "/Users/yk/Desktop/";
//		File file = new File(filePath+fileName);
		Integer belong = 2;
//		List<ImportedMonitorRoomItem> importedMonitorRoomItems = POIUtil.readExcel(fileName,new FileInputStream(file), ImportedMonitorRoomItem.class);
		List<ImportedMonitorRoomItem> importedMonitorRoomItems = new ArrayList<ImportedMonitorRoomItem>();
		ImportedMonitorRoomItem importedMonitorRoomItem = new ImportedMonitorRoomItem();
		importedMonitorRoomItem.setRoomName("爱婴岛测试群");
		importedMonitorRoomItem.setName("name1");
		importedMonitorRoomItems.add(importedMonitorRoomItem);
		GeneralContentResult<Map<String,Object>> result = monitorService.importMonitorRoom(importedMonitorRoomItems, belong);
		System.out.println(JSONObject.toJSONString(result));
	}
	
}
