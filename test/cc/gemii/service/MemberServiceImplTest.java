package cc.gemii.service;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpRequest;
import org.apache.struts2.ServletActionContext;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;

import cc.gemii.AbstractBaseTest;
import cc.gemii.mapper.custom.RobotMapper;
import cc.gemii.po.Monitorrobotbind;
import cc.gemii.po.Wechatroommemberinfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;
import cc.gemii.util.RedisUtil;
import net.sf.json.JSONObject;

public class MemberServiceImplTest extends AbstractBaseTest {

	@Autowired
	private MemberService memberService;

	@Autowired
	private MonitorService monitorService;

	@Autowired
	private RoomService roomService;

	@Autowired
	private MicroTaskService microTaskService;

	@Autowired
	private RobotMapper robotMapper;
	
	@Autowired
	private Properties propertyConfigurer;

	@Test
	public void testRemoveConllection(){
		List<String> list1 = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		list1.add("1111");
		list1.add("23");
		list1.add("sdc");
		list1.add("ffg");
		list1.add("111");
		list1.add("133");
		list2.add("1111");
		list2.add("23");
		list2.add("adc");
		list1.removeAll(list2);
		for(String str:list1){
			System.out.println(str);
		}
	}
	
	@Test
	public void sendMessage(){
		Map<String, Object> sendMap = new HashMap<String, Object>();
		sendMap.put("MsgType", 1);
		Map<String, Object> Msg = new HashMap<String, Object>();
		Map<String,Object> map = new HashMap<String,Object>();
		List<String> list = new ArrayList<String>();
		list.add("92ACF61CEEA1F7D0199EF3D0C63D04F8");
		map.put("roomIds", "");
		Msg.put("Uin", "FDBFB2B1EE75CA333AADE7D934F8468D"); //122661399
		//Msg.put("RoomIDs", (List<String>)entry.getValue());
		Msg.put("u_roomId","BA06A0CCB4A1352CA4D774DE5C8E6138");
		Msg.put("Content", "😊test"); 
		Msg.put("FileName", "");
		Msg.put("atAll", "0");
		Msg.put("memberIds", list);
		sendMap.put("Msg", Msg);
		RedisUtil.publishMessage("20170630_", JSON.toJSONString(sendMap));
	}
	
	@Test
	public void getHaoQiWechatMembers_test() {
		String roomId = "TEST0003";
		List<Wechatroommemberinfo> wechatMembers = memberService.getHaoQiWechatMembers(roomId);
		System.out.println(JSONObject.fromObject(wechatMembers).toString());
	}

	@Test
	public void deleteMemberInfo() {
		memberService.deleteRoomMemberInfo("TEST0009", "1703893904","1");
	}

	@Test
	public void substr() {
		String url = "http://nfs.gemii.cc/microtask/nbYke28M98gkeydk.png";
		System.out.println(url.substring(url.lastIndexOf("/") + 1));
	}

	/**
	 * 微任务编辑查询时间
	 */
	@Test
	public void microTaskTime() {
		Pagination page = new Pagination();
		long startTime = System.currentTimeMillis();
		// 获取csr 群信息
		System.out.println("-------------------------获取csr 群信息查询时间");
		long startTime1 = System.currentTimeMillis();
//		roomService.getCSRRoom(page);
		long endTime1 = System.currentTimeMillis();
		System.out.println("time1-all:" + (endTime1 - startTime1));

		// 根据任务ID获取此任务详细信息
		System.out.println("-------------------------根据任务ID获取此任务详细信息查询时间");
		long startTime2 = System.currentTimeMillis();
		microTaskService.getDetailByID(100250);
		long endTime2 = System.currentTimeMillis();
		System.out.println("time2-all:" + (endTime2 - startTime2));

		long endTime = System.currentTimeMillis();
		System.out.println("整个过程查询时间：" + (endTime - startTime));
	}

	@Test
	public void getRobotTagMap() {
		String rooms = "TEST0033,TEST0034,TEST0035,TEST0036,TEST0037,TEST0038,TEST0039,TEST0040,"
				+ "TEST0041,TEST0042,TEST0043,TEST0044,TEST0045,TEST0046,TEST0047,TEST0048,TEST0049,TEST0050,"
				+ "TEST0051,TEST0052,TESTN0001,TESTN0002,TESTN0004";
		String[] roomIds = rooms.split(",");
		Map<String, Object> tagMap = sendRobotClassify(22, roomIds);
		
	}

	/*
	 * 将收到的roomIDs,以对应不同的机器人tag区分,
	 */
	private Map<String, Object> sendRobotClassify(Integer mID, String[] roomIDs) {
		Map<String, Object> tagMap = new HashMap<String, Object>();
		String tag = "";
		Monitorrobotbind bind = null;
		List<String> classifyRoomIDs = null;
		Map<String, Object> param = null;
		for (String roomID : roomIDs) {
			param = new HashMap<String, Object>();
			param.put("mID", mID);
			param.put("roomID", roomID);
			bind = robotMapper.getSendRobotByRoomID(param);
			if (bind == null)
				continue;
			tag = bind.getRobottag();
			if (tagMap.containsKey(tag)) {
				classifyRoomIDs = (List<String>) tagMap.get(tag);
				classifyRoomIDs.add(roomID);
				tagMap.put(tag, classifyRoomIDs);
			} else {
				classifyRoomIDs = new ArrayList<String>();
				classifyRoomIDs.add(roomID);
				tagMap.put(tag, classifyRoomIDs);
			}
		}
		return tagMap;
	}

	@Test
	public void testDFA(){
		String keyWords = propertyConfigurer.getProperty("haoqi_keywords");
		String[] words = keyWords.split(",");
		Set<String> keyWordSet = new HashSet<String>();
		for(String word:words){
			keyWordSet.add(word);
		}
		Map<String,Object>  map = addSensitiveWordToHashMap(keyWordSet);
		System.out.println(map);
	}
	
	private Map<String,Object> addSensitiveWordToHashMap(Set<String> keyWordSet) {
		Map<String,Object> sensitiveWordMap = new HashMap<String,Object>(keyWordSet.size());     //初始化敏感词容器，减少扩容操作
		String key = null;  
		Map<String,Object> nowMap = null;
		Map<String, Object> newWorMap = null;
		//迭代keyWordSet
		Iterator<String> iterator = keyWordSet.iterator();
		while(iterator.hasNext()){
			key = iterator.next();    //关键字
			nowMap = sensitiveWordMap;
			for(int i = 0 ; i < key.length() ; i++){
				String keyChar = String.valueOf(key.charAt(i));       //转换成char型
				Object wordMap = nowMap.get(keyChar);       //获取
				if(wordMap != null){        //如果存在该key，直接赋值
					nowMap = (Map<String,Object>) wordMap;
				}
				else{     //不存在则，则构建一个map，同时将isEnd设置为0，因为他不是最后一个
					newWorMap = new HashMap<String,Object>();
					newWorMap.put("isEnd", "0");     //不是最后一个
					nowMap.put(keyChar,newWorMap);
					nowMap = newWorMap;
				}
				if(i == key.length() - 1){
					nowMap.put("isEnd", "1");    //最后一个
				}
			}
		}
		return sensitiveWordMap;
	}
	
	@Test
	public void testHashMap(){
		Map<String,Object> baseMap = new HashMap<String,Object>(20);
		Map<String,Object> mapi = null;
		Map<String,Object> mapj = null;
		mapi = baseMap;
		for(int i=0; i<5; i++){
			//更改map的值
			mapj = new HashMap<String,Object>();
			mapj.put("isEnd", "0");
			mapi.put(i+"",mapj );
			mapi = mapj;
		}
		
	}
	
}
