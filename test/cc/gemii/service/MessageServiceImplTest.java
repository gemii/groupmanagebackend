package cc.gemii.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import cc.gemii.AbstractBaseTest;

public class MessageServiceImplTest extends AbstractBaseTest{

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private MessageService messageService;
	
	@Test
	public void handleHaoQiMessage_test(){
		StringBuffer sb = new StringBuffer();
		sb.append("{\"MsgId\": \"3740749394524872791\",");
		sb.append("\"UserNickName\": \"幸福双人行\",");
		sb.append("\"AppMsgType\": 0,");
		sb.append("\"RoomID\": \"wyeth3304\",");
		sb.append("\"CreateTime\": \"2017-05-04 10:05:53\",");
		sb.append("\"Content\": \"红屁股漏尿我都不想上班哦结团断层\",");
		sb.append("\"MemberID\": \"62506d377db4248dfc1e6dbde27530b4\",");
		sb.append("\"UserDisplayName\": \"懂事妈妈+林静媛\",");
		sb.append("\"MsgType\": 1}");
		Map<String, Object> map = (Map<String, Object>) JSON.parse(sb.toString());
		messageService.handleHaoQiMessage(map);
	}
	
	@Test
	public void handelMessage(){
		Map<String, Object> map = new HashMap<>();
		map.put("isLegal", "0");
		map.put("UserDisplayName", "\u8BF7\u53EB\u6211\u5C0F\u7A33\u7A33");
		map.put("MsgId", "201708021080978463");
		map.put("MemberID", "8A46B1608E49956F884B741816DADFCC");
		map.put("UserNickName", "\u8BF7\u53EB\u6211\u5C0F\u7A33\u7A33");
		map.put("Content", "@\u7F8E\u5988\u73ED\u957F@@\u5B89\u5C0F\u4E03\u5988\u5988\u8FDD\u53CD\u4E86\u7FA4\u89C4");
		map.put("MsgType", 1);
		map.put("AppMsgType", 0);
		map.put("RoomID", "MS120");
		map.put("CreateTime", "2017-08-0216: 41: 31");
		map.put("MemberIcon", "http: //wx.qlogo.cn/mmhead/ver_1/Fw5oyonv5WA06PeEAl9bQOOibN1DIf3Z8I2UGAtiaIbpxvLq2ia1MvkvYdShrSlf88iabvQkPOeNVZ56LbY8ExKAFUnshcFwbLvFHSbl1FZDHB0/132");
		messageService.handleMessage(map, true);
	}
	
	@Test
	public void regexTest(){
		String content = "12345@班长@请叫我小稳稳妈妈违反了群规";
		String regex = "^@\\S*@\\S*违反了群规$";
		boolean b = content.matches(regex);
		System.out.println(b);
	}
	
	public static void main(String[] args) {
		String content = "@班长@@请叫我小稳稳妈妈违反了群规";
		if(content.contains("妈妈违反了群规")){
			String username = content.substring(0, content.indexOf("妈妈违反了群规")).trim();
			String[] usernames = username.split("@");
			String regex = "^@\\S*@\\S*妈妈违反了群规$";
			System.out.println(regex);
			boolean matched = content.matches(regex);
			System.out.println(matched);
			System.out.println(username);
			System.out.println(usernames[2]);
		}
	}
	
}
