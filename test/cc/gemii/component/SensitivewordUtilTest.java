package cc.gemii.component;

import java.util.Properties;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cc.gemii.AbstractBaseTest;
import cc.gemii.component.sensitive.SensitivewordUtil;
import cc.gemii.component.sensitive.SensitivewordUtil.MatchType;

public class SensitivewordUtilTest extends AbstractBaseTest{

	private static final String HAOQI_SENSITIVEWORD_KEY = "test";
	
	@Autowired
	private Properties propertyConfigurer;
	
	@Test
	public void isContaintSensitiveWord_test(){
		String content = "大叫好我你好";
//		if(!SensitivewordUtil.hasInit(HAOQI_SENSITIVEWORD_KEY)){
//			String keyWords = propertyConfigurer.getProperty("haoqi_keywords");
//			SensitivewordUtil.initSensitiveWord(HAOQI_SENSITIVEWORD_KEY, keyWords);
//		}
		boolean isContain = SensitivewordUtil.isContaintSensitiveWord(HAOQI_SENSITIVEWORD_KEY,content,MatchType.MIN_MATCHTYPE);
		System.out.println(isContain+"-"+content);
	}
	
}
