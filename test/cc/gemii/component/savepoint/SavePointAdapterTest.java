package cc.gemii.component.savepoint;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cc.gemii.AbstractBaseTest;

public class SavePointAdapterTest extends AbstractBaseTest{

	@Autowired
	private SavePointAdapter savePointAdapter;
	
	@Test
	public void savePoint_test(){
		PointDTO pointDTO = new PointDTO();
		pointDTO.setPageName("关键字匹配");
		pointDTO.setPointName("匹配到关键字");
		pointDTO.setUserKey("test001");
		pointDTO.setContent("这里保护关键字");
		savePointAdapter.savePoint(pointDTO);
	}
}
