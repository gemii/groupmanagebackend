package cc.gemii.component.wechat;

import java.io.UnsupportedEncodingException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cc.gemii.AbstractBaseTest;

public class SendTemplateMsgTest extends AbstractBaseTest{
	
	@Autowired 
	private SendTemplateMsg sendTemplateMsg;
	
	@Test
	public void sendHaoqTemplate_test() throws UnsupportedEncodingException{
		//组装模版消息与发送
		TemplateMsg templateMsg = new TemplateMsg();
		templateMsg.setFirst(new String[] { "necheng你是否对" });
		templateMsg.setKeyword1(new String[] { "一号群1s", "李女士", "这里有关键词？", "key1,key2" });
		templateMsg.setOpenid("oNPcuvwnC1M3Z4sE--iIF3W5VzkE");
		sendTemplateMsg.sendHaoqTemplate(templateMsg);
	}
}
