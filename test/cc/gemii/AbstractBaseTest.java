package cc.gemii;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * 抽象测试基础类
 * <p>所有要使用容器的测试类 都可以 继承该类</p>
 * */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/applicationContext-*.xml")
public abstract class AbstractBaseTest {

}
