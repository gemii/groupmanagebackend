package cc.gemii.listener;

import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import cc.gemii.component.HandleMessageThread;
import cc.gemii.component.RedisThread;
import cc.gemii.service.MonitorService;
import cc.gemii.util.ParamUtil;
import cc.gemii.util.RedisUtil;

public class InitThreadStart implements ServletContextListener {

	private Logger logger = Logger.getLogger(this.getClass());
	private static HandleMessageThread handleMessageThread = null;
	
	/**
	 * tomcat容器关闭执行
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sc) {
		//停止监听线程
		RedisThread.stopSubThread();
		//停止处理特殊消息线程
		handleMessageThread.shutdown();
		try {
			handleMessageThread.join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		//删除redis ip地址监听记录
		RedisUtil.del(RedisUtil.getRealIP());
		//删除redis ip初始化监听记录
		RedisUtil.hdel(ParamUtil.INIT_SUBSCRIBE, RedisUtil.getRealIP());
		//释放jedis pool资源
		RedisUtil.destroyJedisPool();
		try {
			//反注册 jdbc
			DriverManager.deregisterDriver(DriverManager.getDrivers().nextElement());
		} catch (SQLException e) {
			logger.error(e.getMessage(),e);
		}
	}

	/**
	 * tomcat容器启动执行
	 */
	@Override
	public void contextInitialized(ServletContextEvent sc) {
		MonitorService monitorService = WebApplicationContextUtils
				.getWebApplicationContext(sc.getServletContext()).getBean(MonitorService.class);
		String[] channels = monitorService.getInitChannel();
		logger.info("init:" + StringUtils.join(channels, ","));
		// 处理关键词 @管理员消息线程
		handleMessageThread = new HandleMessageThread();
		handleMessageThread.start();
		// 设置初始监听值
		RedisUtil.hset(ParamUtil.INIT_SUBSCRIBE, RedisUtil.getRealIP(), StringUtils.join(channels, ","));
		// 监听初始频道 服务器ip
		new RedisThread(channels).start();
	}

}
