package cc.gemii.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cc.gemii.mapper.custom.TimeTaskMapper;
import cc.gemii.service.MicroTaskService;

@Component
@Lazy(false)
public class TimeTask {

	@Autowired
	private TimeTaskMapper timeTaskMapper;
	
	@Autowired
	private MicroTaskService microTaskService;
	
	//每两分钟
	@Scheduled(cron = "0 0/2 * ? * * ")
	public void alterRoomMessageTable(){
		//更新过期
		timeTaskMapper.taskExpire();
		//检查是否有活动开始展示,若有则通过websocket发送给前端
		//microTaskService.noticeMonitor();
	}
	
//	每晚两点,删除未使用的班长  机器人 绑定关系
//	@Scheduled(cron = "0 0 2 ? * * ")
//	public void deleteUnusedMonitorRobotBind(){
//		timeTaskMapper.deleteUnusedMonitorRobotBind();
//	}
}
