package cc.gemii.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;

import cc.gemii.pojo.BatchIntoGroup;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.LoveBabyInsert;
import cc.gemii.pojo.UpdateParamEntity;
import cc.gemii.service.FileUpService;

@Controller
@RequestMapping("/file")
public class FileUpController {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private FileUpService fileUpService;
	
	@RequestMapping(value = "/upfile",method = RequestMethod.POST)
	@ResponseBody
	public  CommonResult  selectRobotId(MultipartFile file,@RequestParam("weCall")String weCall,@RequestParam("theme")String theme,@RequestParam("introduction")String introduction,@RequestParam("count")String count,@RequestParam("type")String type,@RequestParam("serNum")String serNum,@RequestParam("code")String code){
		BatchIntoGroup batchIntoGroup=new BatchIntoGroup();
		batchIntoGroup.setCount(count);
		batchIntoGroup.setIntroduction(introduction);
		batchIntoGroup.setTheme(theme);
		batchIntoGroup.setType(type);
		batchIntoGroup.setWeCall(weCall);
		batchIntoGroup.setCode(code);
		try {
			return fileUpService.insertFileInfo(file,batchIntoGroup,serNum);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "系统繁忙");
		}
		
	} 
	
	@RequestMapping(value="/updateInfo",method = RequestMethod.POST)
	@ResponseBody
	public CommonResult updateRoomInfo(String params){
		Map<String, Object> param=JSON.parseObject(params);
		UpdateParamEntity updateParamEntity=new UpdateParamEntity();
		updateParamEntity.setTaskId(param.get("taskId").toString());
		updateParamEntity.setData((List<LoveBabyInsert>)JSON.parseArray(param.get("data").toString(),LoveBabyInsert.class));
		try {
			return fileUpService.updateRoomInfo(updateParamEntity);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "系统繁忙");
		}
	}
	
	@RequestMapping(value="/matchGroup",method = RequestMethod.POST)
	@ResponseBody
	public CommonResult buildMatchGroup(MultipartFile file, String type){
		try {
			return fileUpService.buildMatchGroup(file,type);//type=2为合作网红群type=1为惠氏千群新增医院名称
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "系统繁忙");
		}
	}
	
	
	@RequestMapping(value="/batchUpdateRoomname",method = RequestMethod.POST)
	@ResponseBody
	public CommonResult batchUpdateRoomname(MultipartFile file){
		try {
			return fileUpService.batchUpdateRoomname(file);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "系统繁忙");
		}
	}
	
	@RequestMapping(value="/updateCurrentCountAll")
	@ResponseBody
	public CommonResult updateCurrentCountAll(){
		try {
			return fileUpService.updateCurrentCountAll();
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "系统繁忙");
		}
	}
}
