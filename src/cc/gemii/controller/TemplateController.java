package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.QuestionTemplate;
import cc.gemii.service.TemplateService;

@Controller
@RequestMapping("/template")
public class TemplateController {

	@Autowired
	private TemplateService templateService;
	
	/**
	 * 
	 * @param content 内容
	 * @return
	 * TODO 获取模板消息
	 */
	@RequestMapping("/searchTemplate")
	@ResponseBody
	public CommonResult searchTemplate(@RequestParam String content,
			@RequestParam Integer belong,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return templateService.searchTemplate(belong, content);
	}
	
	/**
	 * 
	 * @param template 模板包装类
	 * @return
	 * TODO 新增模板
	 */
	@RequestMapping("/addTemplate")
	@ResponseBody
	public CommonResult addTemplate(QuestionTemplate template,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return templateService.addTemplate(template);
	}
}
