package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Monitor;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.KeywordService;

@Controller
@RequestMapping("/keyword")
public class KeywordController {

	@Autowired
	private KeywordService keywordService;
	
	/**
	 * 
	 * TODO 获取全局关键词列表
	 */
	@RequestMapping("/getGlobalKeyword")
	@ResponseBody
	public CommonResult getGlobalKeyword(@RequestParam Integer belong,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.getGlobalKeyword(belong);
	}
	
	/**
	 * 
	 * @param mID 用户ID
	 * @return
	 * TODO 根据用户ID获取群关键词
	 */
	@RequestMapping("/getKeywordByMid")
	@ResponseBody
	public CommonResult getKeywordByMid(HttpServletResponse response,
			@RequestParam String mID){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.getKeywordByMid(mID);
	}
	
	/**
	 * 
	 * @param mID 用户ID
	 * @param keyword 要删除的关键词
	 * @return
	 * TODO 用户删除关键词
	 */
	@RequestMapping("/deleteKeyword")
	@ResponseBody
	public CommonResult deleteKeyword(@RequestParam String mID,
			@RequestParam String keyword,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.deleteKeyword(mID,keyword);
	}
	
	/**
	 * 
	 * @param roomIDs 关键词对对应群列表
	 * @param keyword 要增加的关键词
	 * @param mID 用户ID
	 * @return
	 * TODO 用户增加关键词
	 */
	@RequestMapping("/insertKeyword")
	@ResponseBody
	public CommonResult insertKeyword(@RequestParam String[] roomIDs,
			@RequestParam String keyword,
			@RequestParam String mID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.insertKeyword(mID,roomIDs,keyword,"开启");
	}
	
	/**
	 * 
	 * @param keyword 关键词
	 * @param mID 用户ID
	 * @return
	 * TODO 用户更改关键词状态(开启、关闭)
	 */
	@RequestMapping("/modifyStatus")
	@ResponseBody
	public CommonResult modifyStatus(@RequestParam String keyword,
			@RequestParam String mID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.modifyStatus(keyword,mID);
	}
	
	/**
	 * 
	 * @param mID 用户ID
	 * @param keyword 关键词
	 * @param roomIDs 更改后的群列表
	 * @return
	 * TODO 用户更改关键词对应群列表
	 */
	@RequestMapping("/updateKeyword")
	@ResponseBody
	public CommonResult updateKeyword(@RequestParam String mID,
			@RequestParam String keyword,
			@RequestParam String[] roomIDs,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.updateKeyword(mID,keyword,roomIDs);
	}
	
	/**
	 * 
	* @Title: initKeyWordMap 
	* @Description: TODO 初始化关键字
	* @param @param roomId 数据库存在的任意roomId
	* @param @param response
	* @param @return    设定文件 
	* @return CommonResult    返回类型 
	* @throws
	 */
	@RequestMapping("/initKeyword")
	@ResponseBody
	public CommonResult initKeyWordMap(HttpServletResponse response,@RequestParam String owner){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.initKeyWordMap(owner);
	}
	
}
