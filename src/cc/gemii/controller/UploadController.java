package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.UploadService;

@Controller
@RequestMapping("/upload")
public class UploadController {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private UploadService uploadService;
	
	@RequestMapping("/uploadMicroTaskFile")
	@ResponseBody
	public CommonResult uploadMicroTaskFile(MultipartFile file, 
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return uploadService.uploadMicroTaskFile(file);
	}
	
	/**
	 * 
	 * @param base64String base64标准格式
	 * @return
	 * TODO
	 */
	@RequestMapping("/uploadBase64")
	@ResponseBody
	private CommonResult uploadBase64(@RequestParam String base64String,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		logger.info(base64String);
		return uploadService.uploadBase64(base64String);
	}
	
	@RequestMapping("/uploadFile")
	@ResponseBody
	public CommonResult uploadFile(MultipartFile file, 
			@RequestParam String dir,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return uploadService.uploadFile(dir, file);
	}
}
