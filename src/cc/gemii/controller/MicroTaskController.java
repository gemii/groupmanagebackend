package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.MicrotaskWithBLOBs;
import cc.gemii.po.Microtaskimagetext;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;
import cc.gemii.service.MicroTaskService;

@Controller
@RequestMapping("/micro")
public class MicroTaskController {

	@Autowired
	private MicroTaskService microTaskService;
	
	@RequestMapping("/login")
	@ResponseBody
	public CommonResult login(@RequestParam String userName,
			@RequestParam String password,
			@RequestParam Integer type,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return microTaskService.login(userName, password, type);
	}
	
	/**
	 * 
	 * @param microTask 微任务包装类
	 * @param imageText 微任务图文
	 * @param roomIDs 微任务对应群列表
	 * @return
	 * TODO 创建微任务
	 */
	@RequestMapping("/createOrUpdate")
	@ResponseBody
	public CommonResult createOrUpdated(MicrotaskWithBLOBs microTask,
			Microtaskimagetext imageText,
			@RequestParam String[] roomIDs,
			@RequestParam Integer isImageText,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return microTaskService.createOrUpdated(microTask, imageText, roomIDs, isImageText);
	}
	
	/**
	 * 
	 * @param area 
	 * @return
	 * TODO 根据区域获取包含城市
	 */
	@RequestMapping("/getCityByArea")
	@ResponseBody
	public CommonResult getCityByArea(@RequestParam String area,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return microTaskService.getCityByArea(area);
	}
	
	/**
	 * 
	 * @param search  查找字符串
	 * @return
	 * TODO
	 */
	@RequestMapping("/taskList")
	@ResponseBody
	public CommonResult getMicroTaskList(@RequestParam String search,
			@RequestParam Integer belong,
			Pagination page,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return microTaskService.getMicroTaskList(search, belong, page);
	}
	
	/**
	 * 
	 * @param taskID 任务ID
	 * @return
	 * TODO 删除任务
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public CommonResult delete(@RequestParam Integer taskID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return microTaskService.delete(taskID);
	}
	
	/**
	 * 
	 * @param taskID 任务ID
	 * @return
	 * TODO 根据任务ID获取此任务详细信息
	 */
	@RequestMapping("/detail")
	@ResponseBody
	public CommonResult getDetailByID(@RequestParam Integer taskID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return microTaskService.getDetailByID(taskID);
	}
	
	/**
	 * 
	 * @param taskID 任务ID
	 * @return
	 * TODO 启用 停用切换
	 */
	@RequestMapping("/taskEnable")
	@ResponseBody
	public CommonResult updateTaskEnable(@RequestParam Integer taskID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return microTaskService.updateTaskEnable(taskID);
	}
	
	/**
	 * 
	 * @param taskID
	 * @param response
	 * @return
	 * TODO 复制任务
	 */
	@RequestMapping("/cp")
	@ResponseBody
	public CommonResult cpMicroTask(@RequestParam Integer taskID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return microTaskService.cpMicroTask(taskID);
	}
	
	/**
	 * 
	 * @param taskID 任务ID
	 * @return
	 * TODO 获取任务图文信息
	 */
	@RequestMapping("/getImageTextByTaskID")
	@ResponseBody
	public CommonResult getImageTextByTaskID(@RequestParam Integer taskID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return microTaskService.getImageTextByTaskID(taskID);
	}
}
