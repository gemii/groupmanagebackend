package cc.gemii.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.RoomInfo;
import cc.gemii.service.RoomService;
import cc.gemii.util.POIExportUtil;

@Controller
@RequestMapping("/download")
public class DownloadController {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private RoomService roomService;
	
	@RequestMapping("/downloadRoomMemberInfo")
	@ResponseBody
	public void downloadMemberStatistics(HttpServletResponse response) throws Exception{
		DateFormat df = new SimpleDateFormat("MMddHHmm");
		String[] headers = {"群ID","群名","由创群Id","群成员人数"};
		String title = "爱婴岛群成员统计表";
		String filename = "爱婴岛群成员统计表"+df.format(new Date())+".xls";
		List<RoomInfo> roomInfoList = roomService.findRoomStatisticsInfo();
		try {
			response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
			POIExportUtil.exportExcel(title, headers, roomInfoList, response.getOutputStream());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception();
		}
	}
	
}
