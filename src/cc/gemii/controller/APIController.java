package cc.gemii.controller;

import java.io.File;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.APIService;

@Controller
@RequestMapping("/api")
public class APIController {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private APIService apiService;
	
	/**
	 * 
	 * @param start 开始时间 格式yyyy-MM-dd HH:mm:ss
	 * @param end 结束时间 格式yyyy-MM-dd HH:mm:ss
	 * @return
	 * TODO 获取聊天记录   wyeth
	 */
	@RequestMapping("/getChatRecord")
	@ResponseBody
	public CommonResult getChatRecord(@RequestParam String start,
			@RequestParam String end,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return apiService.getChatRecord(start, end);
	}
	
	/**
	 * 
	 * @param roomIDs 群ID数组
	 * @param response
	 * @return
	 * TODO 获取群信息  nplus
	 */
	@RequestMapping("/roomInfo")
	@ResponseBody
	public CommonResult getRoomInfo(@RequestParam String[] roomIDs,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return apiService.getRoomInfo(roomIDs);
	}
	
	@RequestMapping("/getChatRecordExcel")
	@ResponseBody
	public Object getChatRecordExcel(@RequestParam String start,
			@RequestParam String end,
			@RequestParam String[] roomIDs,
			HttpServletRequest request,
			HttpServletResponse response){
		try {
			response.setHeader("Access-Control-Allow-Origin", "*");
			HttpHeaders headers = new HttpHeaders();  
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			String path = request.getSession().getServletContext().getRealPath("") + "file/";
			Map<String, Object> result = apiService.getChatRecordExcel(roomIDs, start, end, path);
			if(result == null)
				return CommonResult.build(-1, "error");
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(String.valueOf(result.get("fileName")),"UTF-8")); 
			return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray((File) result.get("file")),  headers, HttpStatus.CREATED);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(-1, "error");
		}
	}
	
	@RequestMapping("/getRoomMemReport")
	@ResponseBody
	public CommonResult getRoomMemReport(@RequestParam String date,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return apiService.getRoomMemReport(date);
	}
	
	@RequestMapping("/getRoomMsgReport")
	@ResponseBody
	public CommonResult getRoomMsgReport(@RequestParam String date,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return apiService.getRoomMsgReport(date);
	}
}
