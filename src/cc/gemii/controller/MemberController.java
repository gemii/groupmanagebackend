package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.MemberService;

@Controller
@RequestMapping("/member")
public class MemberController {

	@Autowired
	private MemberService memberService;
	
	@RequestMapping("/updateIsLegal")
	@ResponseBody
	public CommonResult updateIsLegal(@RequestParam String roomID,
			@RequestParam String memberID,
			@RequestParam String isLegal,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return memberService.updateIsLegal(roomID,memberID,isLegal);
	}
	
	/**
	 * 删除群成员信息
	 * @param roomID 群ID
	 * @param memberID	成员ID
	 * @param response
	 * @return
	 */
	@RequestMapping("/deleteRoomMemberInfo")
	@ResponseBody
	public CommonResult deleteRoomMemberInfo(@RequestParam String roomID,
			@RequestParam String memberID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return memberService.deleteRoomMemberInfo(roomID,memberID,"1");
	}
	
	/**
	 * 
	 * @param roomID 群id
	 * @param memberID 成员id
	 * @return
	 * TODO	获取群成员信息
	 */
	@RequestMapping("/getMemberInfo")
	@ResponseBody
	public CommonResult getMemberInfo(@RequestParam String roomID,
			@RequestParam String memberID,
			@RequestParam Integer belong,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return memberService.getMemberInfo(roomID, memberID, belong);
	}
	
	/**
	 * TODO 获取群成员标签列表
	 */
	@RequestMapping("/getTagList")
	@ResponseBody
	public CommonResult getTagList(@RequestParam Integer belong,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return memberService.getTagList(belong);
	}
	
	/**
	 * 
	 * @param roomID 群ID
	 * @param memberID 成员ID
	 * @param tags 标签列表
	 * @return
	 * TODO 用户更新群成员标签
	 */
	@RequestMapping("/updateMemberTag")
	@ResponseBody
	public CommonResult updateMemberTag(@RequestParam String roomID,
			@RequestParam String memberID,
			@RequestParam Integer[] tags,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return memberService.updateMemberTag(roomID, memberID, tags);
	}
}
