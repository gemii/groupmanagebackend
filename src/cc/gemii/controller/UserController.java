package cc.gemii.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.SendMessageBoundary;
import cc.gemii.service.MonitorService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private MonitorService monitorService;
	
	/**
	 * 
	 * @param username 用户名
	 * @param password 密码
	 * @return
	 * TODO 登陆
	 */
	@RequestMapping("/login")
	@ResponseBody
	public CommonResult login(@RequestParam String username,
			@RequestParam String password,
			HttpServletResponse response
			){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return monitorService.login(username,password);
	}
	
	/**
	 * 
	 * @param data 加密信息 cookie
	 * @return
	 * TODO 根据cookie获取用户信息
	 */
	@RequestMapping("/getUserInfo")
	@ResponseBody
	public CommonResult getUserInfo(@RequestParam String data,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return monitorService.getUserInfo(data);
	}
	
	/**
	 * 
	 * @param sendMessage 发送信息包装类
	 * @return
	 * TODO 用户发送群消息
	 */
	@RequestMapping("/sendMessage")
	@ResponseBody
	public CommonResult sendMessage(SendMessageBoundary sendMessage,
			HttpServletRequest request,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return monitorService.sendMessage(request, sendMessage);
	}
	
	/**
	 * 
	 * @param mID 用户ID
	 * @param action 打卡  上班 下班  午休
	 * @return
	 * TODO 用户打卡
	 */
	@RequestMapping("/monitorRecord")
	@ResponseBody
	public CommonResult insertMonitorRecord(@RequestParam Integer mID,@RequestParam String action,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return monitorService.insertMonitorRecord(mID,action);
	}
	
	/**
	 * 
	 * @param mID 用户ID
	 * @return
	 * TODO 班长管理界面
	 */
	@RequestMapping("/getMonitorInfo")
	@ResponseBody
	public CommonResult getMonitorInfo(@RequestParam Integer mID,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return monitorService.getMonitorInfo(mID);
	}
	
	/**
	 * 
	 * @param robotTag 机器人标签
	 * @return
	 * TODO 班长账号登陆
	 */
	@RequestMapping("/monitorLogin")
	@ResponseBody
	public CommonResult monitorLogin(@RequestParam String robotTag,
			HttpServletResponse response,HttpServletRequest request){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return monitorService.monitorLogin(robotTag,request);
	}
	
	/**
	 * 
	 * @param roomID 群ID
	 * @param mID 用户ID
	 * @return
	 * TODO 根据群ID获取发送机器人名称
	 */
	@RequestMapping("/getSendRobotByRoomID")
	@ResponseBody
	public CommonResult getSendRobotByRoomID(@RequestParam String roomID,
			@RequestParam Integer mID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return monitorService.getSendRobotByRoomID(mID, roomID);
	}
	
	/**
	 * 
	 * @param mID 用户ID
	 * @return
	 * TODO 根据用户获取监听频道
	 */
	@RequestMapping("/getChannelByMid")
	@ResponseBody
	public CommonResult getChannelByMid(@RequestParam Integer mID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return CommonResult.ok(monitorService.getChannelByMid(mID));
	}
	
	/**
	 * 
	 * @param monitorName  班长机器人名称
	 * @param mID 班长ID
	 * @return
	 * TODO 用户增加班长机器人
	 */
	@RequestMapping("/addSendRobot")
	@ResponseBody
	public CommonResult addSendSobot(@RequestParam String monitorName,
			@RequestParam Integer mID,
			HttpServletResponse response,
			HttpServletRequest request){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return monitorService.addSendRobot(monitorName, mID,request);
	}
	
	/**
	 * 
	 * @param mID 班长ID
	 * @param roomID 群ID
	 * @return
	 * TODO
	 */
	@RequestMapping("/getMicroTask")
	@ResponseBody
	public CommonResult getMicroTask(@RequestParam Integer mID,
			@RequestParam(required=false) String roomID,
			@RequestParam(required=false) Integer belong,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return monitorService.getMicroTask(mID, roomID, belong);
	}
	
	@RequestMapping("/updateRoomOwner")
	@ResponseBody
	public CommonResult updateRoomOwner(MultipartFile file){
		return monitorService.updateRoomOwner(file);
	}
	
}
