package cc.gemii.controller.admin;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.GeneralPagingResult;
import cc.gemii.data.admin.ImportedMonitorRoomItem;
import cc.gemii.data.admin.MonitorRoomInfo;
import cc.gemii.service.MonitorService;
import cc.gemii.util.POIUtil;

/**
 * 管理员管理班长的controller
 * */
@Controller
@RequestMapping(value = "/admin")
public class MonitorController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private MonitorService monitorService;
	
	/**
	 * 批量更新 群与班长的关系
	 * @param file 
	 * 		  六列 roomId(群ID) innerId(群ID) roomName(群名) monitorId(班长ID) userName(班长登录名) name(班长名);
	 * @param belong 1 gemii，2 wyeth 
	 * */
	@RequestMapping(value = "/monitorroom/import", method = RequestMethod.POST)
	@ResponseBody
	public GeneralContentResult<Map<String,Object>> importMonitorRoom(MultipartFile file,@RequestParam("belong") Integer belong){
		GeneralContentResult<Map<String,Object>> result = new GeneralContentResult<Map<String,Object>>();
		try {
			List<ImportedMonitorRoomItem> importedMonitorRoomItems = POIUtil.readExcel(file.getOriginalFilename(), file.getInputStream(), ImportedMonitorRoomItem.class);
			log.info("接收参数：belong="+belong);
			log.info("接收参数：importedMonitorRoomItems="+JSONObject.toJSONString(importedMonitorRoomItems));
			result = monitorService.importMonitorRoom(importedMonitorRoomItems, belong);
			log.info("导入结果：result="+JSONObject.toJSONString(result));
			return result;
		} catch (IOException e) {
			log.error(e.getMessage(),e);
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 根据条件查询群以及对应的班长
	 * @param monitorRoomInfo
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value = "/operategroup/search", method = RequestMethod.GET)
	@ResponseBody
	public GeneralPagingResult<MonitorRoomInfo> searchMonitorRoom(MonitorRoomInfo monitorRoomInfo,@RequestParam("page") Integer page,@RequestParam("size") Integer size){
		GeneralPagingResult<MonitorRoomInfo> result = new GeneralPagingResult<MonitorRoomInfo>();
		if(null == monitorRoomInfo){
			result.setResultCode(ResultCode.PARAMETER_NULL);
			result.setDetailDescription("传入参数为NULL");
			return result;
		}
		result = monitorService.searchMonitorRoom(monitorRoomInfo,page,size);
		return result;
	}
	
	/**
	 * 删除班长与群的关联关系(单条记录)
	 * @param monitorRoomId
	 * @return
	 */
	@RequestMapping(value = "/operategroup/delete", method = RequestMethod.GET)
	@ResponseBody
	public GeneralContentResult<Map<String,Object>> deleteMonitorRoom(@RequestParam("monitorRoomId") String monitorRoomId){
		GeneralContentResult<Map<String,Object>> result = new GeneralContentResult<Map<String,Object>>();
		try {
			log.info("传入参数，要删除的班长与群关联ID : " + monitorRoomId);
			monitorService.deleteMonitorRoom(monitorRoomId);
			result.setResultCode(ResultCode.OPERATION_SUCCESS);
			result.setDetailDescription("操作成功");
			return result;
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription(e.getMessage());
			return result;
		}
	}
	
}
