package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Wechatroommessagespecial;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.MessageService;

@Controller
@RequestMapping("/message")
public class MessageController {

	@Autowired
	private MessageService messageService;
	
	/**
	 * 
	 * @param roomID 群ID
	 * @param timestamp 时间戳
	 * @param type 类型 last上一页 next下一页 limit转到指定消息
	 * @return
	 * TODO 获取普通群消息
	 */
	@RequestMapping("/getChatRecord")
	@ResponseBody
	public CommonResult getChatRecord(@RequestParam String roomID,
			@RequestParam String timestamp,
			@RequestParam String type,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return messageService.getChatRecord(roomID, timestamp, type);
	}
	
	/**
	 * 
	 * @param mID 班长ID
	 * @param timestamp 时间戳
	 * @param type keyword关键字 admin管理员
	 * @return
	 * TODO 获取特殊消息
	 */
	@RequestMapping("/getSpecialRecord")
	@ResponseBody
	public CommonResult getSpecialRecord(@RequestParam Integer mID,
			@RequestParam String timestamp,
			@RequestParam String type,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return messageService.getSpecialRecord(mID, timestamp, type);
	}
	
	/**
	 * 
	 * @param mID 班长ID
	 * @param roomID 群ID
	 * @param type read 群消息 keyword 关键字 admin 管理员
	 * @return
	 * TODO 消息置为已读
	 */
	
	@RequestMapping("/modifyMessageStatus")
	@ResponseBody
	public CommonResult modifyMessageStatus(@RequestParam(required=false) Integer mID,
			@RequestParam(required=false) String roomID,
			@RequestParam String type,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return messageService.modifyMessageStatus(mID, roomID, type);
	}
	
	
	/**
	 * 
	 * @param read
	 * TODO 用户点击关键字和@管理员消息
	 */
	@RequestMapping("/readMessage")
	@ResponseBody
	public CommonResult readMessage(Wechatroommessagespecial read,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return messageService.readMessage(read);
	}
}
