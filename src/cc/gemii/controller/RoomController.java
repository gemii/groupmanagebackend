package cc.gemii.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;
import cc.gemii.service.RoomService;

@Controller
@RequestMapping("/room")
public class RoomController {

	@Autowired
	private RoomService roomService;
	
	/**
	 * 
	 * @param mID 用户ID
	 * @return
	 * TODO 获取用户对应群列表
	 */
	@RequestMapping("/getRoomByMid")
	@ResponseBody
	public CommonResult getRoomByMid(HttpServletResponse response
			,@RequestParam Integer mID){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.getRoomByMid(mID);
	}
	
	/**
	 * 
	 * @param roomID 群ID
	 * @return
	 * TODO 获取群内成员列表
	 */
	@RequestMapping("/getMemberByRoomID")
	@ResponseBody
	public CommonResult getMemberByRid(HttpServletResponse response,
			@RequestParam String roomID){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.getMemberByRid(roomID);
	}
	
	/**
	 * 
	 * @param map
	 * @return
	 * TODO 获取制定群指定用户昵称的成员
	 */
	@RequestMapping("/getMemberByMap")
	@ResponseBody
	public CommonResult getMemberByMap(HttpServletResponse response
			,@RequestParam Map<String,Object> map){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.getMemberByMap(map);
	}
	
	/**
	 * 
	 * @param mID 用户ID
	 * @param keyword 关键词
	 * @return 
	 * TODO 根据关键词对应群列表
	 */
	@RequestMapping("/getRoomByKeyword")
	@ResponseBody
	public CommonResult getRoomByKeyword(@RequestParam Integer mID,
			@RequestParam String keyword,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.getRoomByKeyword(mID,keyword);
	}
	
	/**
	 * 
	 * @param mID 用户ID
	 * @return
	 * TODO 获取用户对于群列表的关键词
	 */
	@RequestMapping("/getRoomKeyword")
	@ResponseBody
	public CommonResult getRoomKeyword(@RequestParam Integer mID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.getRoomKeyword(mID);
	}
	
	/**
	 * 
	 * @param tags 标签
	 * @param roomID 群ID
	 * @return
	 * TODO 新增群标签
	 */
	@RequestMapping("/insertRoomTag")
	@ResponseBody
	public CommonResult insertRoomTag(@RequestParam String[] tags,
			@RequestParam String roomID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.insertRoomTag(roomID, tags);
	}
	
	/**
	 * 
	 * @param roomID
	 * @param tag
	 * @return
	 * TODO 删除群标签
	 */
	@RequestMapping("/deleteRoomTag")
	@ResponseBody
	public CommonResult deleteRoomTag(@RequestParam String roomID,
			@RequestParam String tag,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.deleteRoomTag(roomID, tag);
	}
	
	/**
	 * 
	 * @param roomID 群id
	 * @return
	 * TODO 获取群对应的标签
	 */
	@RequestMapping("/getRoomTagByRoomID")
	@ResponseBody
	public CommonResult getRoomTagByRoomID(@RequestParam String roomID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.getRoomTagByRoomID(roomID);
	}
	
	/**
	 * 
	 * @param mID 班长ID
	 * @param roomID 群ID
	 * TODO 班长标记群
	 */
	@RequestMapping("/markRoom")
	@ResponseBody
	public CommonResult markRoom(@RequestParam Integer mID,
			@RequestParam String roomID,
			@RequestParam String action,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.markRoom(mID, roomID, action);
	}
	
	/**
	 * 
	 * @param mID 用户ID
	 * @return
	 * TODO 获取用户管理的群
	 */
	@RequestMapping("/getManagedRoom")
	@ResponseBody
	public CommonResult getManagedRoom(@RequestParam Integer mID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.getManagedRoom(mID);
	}
	
	/**
	 * 
	 * @param response
	 * @return
	 * TODO 获取csr 群信息
	 */
	@RequestMapping("/getCSRRoom")
	@ResponseBody
	public CommonResult getCSRRoom(HttpServletResponse response,Pagination page,HttpServletRequest request){
		response.setHeader("Access-Control-Allow-Origin", "*");
		String belong = request.getParameter("belong");
		return roomService.getCSRRoom(page,belong);
	}
	
	/**
	 * 
	 * @param roomIDs 群ID字符串  ","分隔
	 * @return
	 * TODO 验证群ID是否正确或是否存在于csr群中
	 */
	@RequestMapping("/validateRoomID")
	@ResponseBody
	public CommonResult validateRoomIDs(@RequestParam String roomIDs,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.validateRoomIDs(roomIDs);
	}
	
	/**
	 * 
	 * @param mID 班长ID
	 * @param percent 百分比
	 * @return
	 * TODO 获取该班长最低活跃度的群，个数为count
	 */
	@RequestMapping("/lowestLiveness")
	@ResponseBody
	public CommonResult liveness(@RequestParam Integer mID,
			@RequestParam String percent,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.lowestLiveness(mID, percent);
	}
	
	/**
	 * 
	 * @param mID 班长ID
	 * @return
	 * TODO 群活跃度列表
	 */
	@RequestMapping("/livenessList")
	@ResponseBody
	public CommonResult livenessList(@RequestParam Integer mID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return roomService.livenessList(mID);
	}
}
