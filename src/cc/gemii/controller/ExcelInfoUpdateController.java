package cc.gemii.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.ExcelInfoUpdateService;

@Controller
@RequestMapping("/excel")
public class ExcelInfoUpdateController {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private ExcelInfoUpdateService excelInfoUpdateService;
	
	@RequestMapping(value = "/excelInfoUpdate",method = RequestMethod.POST)
	@ResponseBody
	public  CommonResult  excelInfoUpdate(MultipartFile file){
		try {
			return excelInfoUpdateService.excelInfoUpdate(file);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "系统繁忙");
		}
		
	} 
	
}
