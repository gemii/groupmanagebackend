package cc.gemii.constant;

public class ResultCode {
	/**操作成功*/
	public final static String OPERATION_SUCCESS = "100";
	/**操作异常*/
	public final static String OPERATION_ERROR = "101";
	/**操作失败*/
	public final static String OPERATION_FAIL = "102";
	
	/**操作失败*/
	public final static String OPERATION_NOT_MATCH_DATA = "201";
	
	/**已存在**/
	public final static String INFO_EXIST="300";
	
	/**参数为NULL*/
	public final static String PARAMETER_NULL = "103";

}
