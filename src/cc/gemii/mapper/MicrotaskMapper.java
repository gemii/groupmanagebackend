package cc.gemii.mapper;

import cc.gemii.po.Microtask;
import cc.gemii.po.MicrotaskExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MicrotaskMapper {
    int countByExample(MicrotaskExample example);

    int deleteByExample(MicrotaskExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Microtask record);

    int insertSelective(Microtask record);

    List<Microtask> selectByExampleWithBLOBs(MicrotaskExample example);

    List<Microtask> selectByExample(MicrotaskExample example);

    Microtask selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Microtask record, @Param("example") MicrotaskExample example);

    int updateByExampleWithBLOBs(@Param("record") Microtask record, @Param("example") MicrotaskExample example);

    int updateByExample(@Param("record") Microtask record, @Param("example") MicrotaskExample example);

    int updateByPrimaryKeySelective(Microtask record);

    int updateByPrimaryKeyWithBLOBs(Microtask record);

    int updateByPrimaryKey(Microtask record);
}