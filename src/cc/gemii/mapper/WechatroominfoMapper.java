package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatroominfo;

public interface WechatroominfoMapper {

	void update(Wechatroominfo wechatroominfo);

	Wechatroominfo selectRoomByRoomId(@Param("roomid")String roomid);

	Wechatroominfo selectRoomByInnerId(@Param("innerid")String innerid);

	Wechatroominfo selectRoomByRoomName(@Param("roomname")String roomname);
	
	List<Wechatroominfo> selectRoomInfo(Map<String,String> reqMap);

}
