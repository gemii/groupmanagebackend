package cc.gemii.mapper;

import cc.gemii.po.Monitorrobotbind;
import cc.gemii.po.MonitorrobotbindExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface MonitorrobotbindMapper {
    int countByExample(MonitorrobotbindExample example);

    int deleteByExample(MonitorrobotbindExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Monitorrobotbind record);

    int insertSelective(Monitorrobotbind record);

    List<Monitorrobotbind> selectByExample(MonitorrobotbindExample example);

    Monitorrobotbind selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Monitorrobotbind record, @Param("example") MonitorrobotbindExample example);

    int updateByExample(@Param("record") Monitorrobotbind record, @Param("example") MonitorrobotbindExample example);

    int updateByPrimaryKeySelective(Monitorrobotbind record);

    int updateByPrimaryKey(Monitorrobotbind record);

	List<Monitorrobotbind> selectMonitorrobotbindByMap(Map<String, Object> map);
}