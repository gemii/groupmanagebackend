package cc.gemii.mapper;

import cc.gemii.po.Monitorworkrecord;
import cc.gemii.po.MonitorworkrecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MonitorworkrecordMapper {
    int countByExample(MonitorworkrecordExample example);

    int deleteByExample(MonitorworkrecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Monitorworkrecord record);

    int insertSelective(Monitorworkrecord record);

    List<Monitorworkrecord> selectByExample(MonitorworkrecordExample example);

    Monitorworkrecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Monitorworkrecord record, @Param("example") MonitorworkrecordExample example);

    int updateByExample(@Param("record") Monitorworkrecord record, @Param("example") MonitorworkrecordExample example);

    int updateByPrimaryKeySelective(Monitorworkrecord record);

    int updateByPrimaryKey(Monitorworkrecord record);
}