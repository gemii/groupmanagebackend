package cc.gemii.mapper;

import java.util.List;

import cc.gemii.pojo.HaoQiUserInfoVO;

public interface HaoQiUserInfoMapper {

	/**
	 * 查询已被邀请的入群的好奇用户
	 * */
	public List<HaoQiUserInfoVO> selectHaoQiUserInfoByRoomId(String roomId);
}
