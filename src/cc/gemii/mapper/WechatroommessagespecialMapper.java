package cc.gemii.mapper;

import cc.gemii.po.Wechatroommessagespecial;
import cc.gemii.po.WechatroommessagespecialExample;
import cc.gemii.po.WechatroommessagespecialKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WechatroommessagespecialMapper {
    int countByExample(WechatroommessagespecialExample example);

    int deleteByExample(WechatroommessagespecialExample example);

    int deleteByPrimaryKey(WechatroommessagespecialKey key);

    int insert(Wechatroommessagespecial record);

    int insertSelective(Wechatroommessagespecial record);

    List<Wechatroommessagespecial> selectByExampleWithBLOBs(WechatroommessagespecialExample example);

    List<Wechatroommessagespecial> selectByExample(WechatroommessagespecialExample example);

    Wechatroommessagespecial selectByPrimaryKey(WechatroommessagespecialKey key);

    int updateByExampleSelective(@Param("record") Wechatroommessagespecial record, @Param("example") WechatroommessagespecialExample example);

    int updateByExampleWithBLOBs(@Param("record") Wechatroommessagespecial record, @Param("example") WechatroommessagespecialExample example);

    int updateByExample(@Param("record") Wechatroommessagespecial record, @Param("example") WechatroommessagespecialExample example);

    int updateByPrimaryKeySelective(Wechatroommessagespecial record);

    int updateByPrimaryKeyWithBLOBs(Wechatroommessagespecial record);

    int updateByPrimaryKey(Wechatroommessagespecial record);
}