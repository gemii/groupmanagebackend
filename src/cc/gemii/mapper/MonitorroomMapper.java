package cc.gemii.mapper;

import cc.gemii.po.Monitorroom;
import cc.gemii.po.MonitorroomExample;
import cc.gemii.pojo.ExcelTemplate;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MonitorroomMapper {
    int countByExample(MonitorroomExample example);

    int deleteByExample(MonitorroomExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Monitorroom record);

    int insertSelective(Monitorroom record);

    List<Monitorroom> selectByExample(MonitorroomExample example);

    Monitorroom selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Monitorroom record, @Param("example") MonitorroomExample example);

    int updateByExample(@Param("record") Monitorroom record, @Param("example") MonitorroomExample example);

    int updateByPrimaryKeySelective(Monitorroom record);

    int updateByPrimaryKey(Monitorroom record);

	void updateMonitorRoomByExcelTemplate(ExcelTemplate eTemplate);

	List<Monitorroom> findMonitorRoomByExcelTemplate(ExcelTemplate eTemplate);

	List<Monitorroom> selectByRoomId(@Param("RoomID") String roomId);
}