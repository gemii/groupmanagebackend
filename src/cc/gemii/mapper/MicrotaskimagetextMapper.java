package cc.gemii.mapper;

import cc.gemii.po.Microtaskimagetext;
import cc.gemii.po.MicrotaskimagetextExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MicrotaskimagetextMapper {
    int countByExample(MicrotaskimagetextExample example);

    int deleteByExample(MicrotaskimagetextExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Microtaskimagetext record);

    int insertSelective(Microtaskimagetext record);

    List<Microtaskimagetext> selectByExample(MicrotaskimagetextExample example);

    Microtaskimagetext selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Microtaskimagetext record, @Param("example") MicrotaskimagetextExample example);

    int updateByExample(@Param("record") Microtaskimagetext record, @Param("example") MicrotaskimagetextExample example);

    int updateByPrimaryKeySelective(Microtaskimagetext record);

    int updateByPrimaryKey(Microtaskimagetext record);
}