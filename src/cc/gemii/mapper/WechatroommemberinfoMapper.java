package cc.gemii.mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.BlackWhiteListDO;
import cc.gemii.po.DeleteMemberHistory;
import cc.gemii.po.Wechatroommemberinfo;
import cc.gemii.po.WechatroommemberinfoExample;
import cc.gemii.po.WechatroommemberinfoKey;

public interface WechatroommemberinfoMapper {
    int countByExample(WechatroommemberinfoExample example);

    int deleteByExample(WechatroommemberinfoExample example);

    int deleteByPrimaryKey(WechatroommemberinfoKey key);

    int insert(Wechatroommemberinfo record);

    int insertSelective(Wechatroommemberinfo record);

    List<Wechatroommemberinfo> selectByExample(WechatroommemberinfoExample example);

    Wechatroommemberinfo selectByPrimaryKey(WechatroommemberinfoKey key);

    int updateByExampleSelective(@Param("record") Wechatroommemberinfo record, @Param("example") WechatroommemberinfoExample example);

    int updateByExample(@Param("record") Wechatroommemberinfo record, @Param("example") WechatroommemberinfoExample example);

    int updateByPrimaryKeySelective(Wechatroommemberinfo record);

    int updateByPrimaryKey(Wechatroommemberinfo record);
    
    /**
     * 根据群ID 与 昵称集合 查询群里匹配到的用户
     * @param roomId 群ID
     * @parma nickNames 昵称集合
     * @return 微信群中的用户
     * */
    List<Wechatroommemberinfo> selectExistUserByNickName(@Param("roomId") String roomId,@Param("nickNames") Set<String> nickNames);

    /**
     * 删除群成员信息
     * @param example
     */
	void deleteRoomMemberInfo(WechatroommemberinfoExample example);

	/**
	 * 
	* @Title: findUUserIdByMemberId 
	* @Description: 根据群成员Id查询u_userId
	* @param @param memberID
	* @return String    返回类型 
	* @throws
	 */
	Wechatroommemberinfo findUUserIdByMemberId(Map<String,String> param);

	/**
	 * 
	* @Title: insertDeleteMemberHistory 
	* @Description: TODO 插入踢人历史记录
	* @param @param history    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	void insertDeleteMemberHistory(DeleteMemberHistory history);

	/**
	 * 
	* @Title: getMemberInfoByMap 
	* @Description: TODO 获取入群类型
	* @param @param paramMap
	* @param @return    设定文件 
	* @return Wechatroommemberinfo    返回类型 
	* @throws
	 */
	List<Wechatroommemberinfo> getMemberInfoByMap(Map<String, String> paramMap);

	/**
	 * 
	* @Title: insertWhileList 
	* @Description: 插入白名单数据
	* @param @param data    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	void insertWhileList(Map<String, String> data);

	/**
	 * 
	* @Title: updateWhileList 
	* @Description: 取消白名单数据 
	* @param @param data    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	void updateWhileList(Map<String, String> data);

	/**
	 * 
	* @Title: findWhileListByMap 
	* @Description: 查询是否存在白名单记录
	* @param @param data
	* @param @return    设定文件 
	* @return String    返回类型 
	* @throws
	 */
	String findWhileListByMap(Map<String, String> data);

	String findWhiteProId(Map<String, String> map);

	/**
	 * 根据memberid去BlackWhiteList比对
	 * @param openid
	 * @return
	 */
	List<BlackWhiteListDO> selectWhiteByMemberId(Map<String, String> map);

}