package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cc.gemii.data.admin.MonitorRoomInfo;
import cc.gemii.po.Monitor;
import cc.gemii.po.MonitorExample;

public interface MonitorMapper {
    int countByExample(MonitorExample example);

    int deleteByExample(MonitorExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Monitor record);

    int insertSelective(Monitor record);

    List<Monitor> selectByExample(MonitorExample example);

    Monitor selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Monitor record, @Param("example") MonitorExample example);

    int updateByExample(@Param("record") Monitor record, @Param("example") MonitorExample example);

    int updateByPrimaryKeySelective(Monitor record);

    int updateByPrimaryKey(Monitor record);

	Integer findMonitorMaxTag();
	
	List<Monitor> selectMonitor(Map<String,String> reqMap);

	Page<MonitorRoomInfo> searchMonitorRoom(MonitorRoomInfo monitorRoomInfo);

	void deleteMonitorRoom(String monitorRoomId);
}