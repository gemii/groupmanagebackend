package cc.gemii.mapper;

import cc.gemii.po.Roomtagbind;
import cc.gemii.po.RoomtagbindExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RoomtagbindMapper {
    int countByExample(RoomtagbindExample example);

    int deleteByExample(RoomtagbindExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Roomtagbind record);

    int insertSelective(Roomtagbind record);

    List<Roomtagbind> selectByExample(RoomtagbindExample example);

    Roomtagbind selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Roomtagbind record, @Param("example") RoomtagbindExample example);

    int updateByExample(@Param("record") Roomtagbind record, @Param("example") RoomtagbindExample example);

    int updateByPrimaryKeySelective(Roomtagbind record);

    int updateByPrimaryKey(Roomtagbind record);
}