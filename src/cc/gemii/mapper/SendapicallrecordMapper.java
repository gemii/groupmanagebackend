package cc.gemii.mapper;

import cc.gemii.po.Sendapicallrecord;
import cc.gemii.po.SendapicallrecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SendapicallrecordMapper {
    int countByExample(SendapicallrecordExample example);

    int deleteByExample(SendapicallrecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Sendapicallrecord record);

    int insertSelective(Sendapicallrecord record);

    List<Sendapicallrecord> selectByExample(SendapicallrecordExample example);

    Sendapicallrecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Sendapicallrecord record, @Param("example") SendapicallrecordExample example);

    int updateByExample(@Param("record") Sendapicallrecord record, @Param("example") SendapicallrecordExample example);

    int updateByPrimaryKeySelective(Sendapicallrecord record);

    int updateByPrimaryKey(Sendapicallrecord record);
}