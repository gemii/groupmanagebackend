package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.TagBoundary;

public interface TagMapper {

	List<TagBoundary> getRoomTagByRoomID(String roomid);

	void insertRoomTag(Map<String, Object> param);

}
