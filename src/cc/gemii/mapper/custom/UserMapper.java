package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.po.Monitor;
import cc.gemii.pojo.MicroTaskCustom;
import cc.gemii.pojo.QuestionTemplate;
import cc.gemii.pojo.RoomInfo;

public interface UserMapper {
	List<QuestionTemplate> searchTemplate(Map<String, Object> param);

	Integer getMonitorByRoomID(String roomID);

	String getMonitorNameByRid(String roomID);

	Monitor getMonitor(Map<String, String> param);

	void insertSendRobot(Map<String, Object> param);

	List<MicroTaskCustom> getMicroTask(Map<String, Object> param);

	List<RoomInfo> getRoomInfoByTaskID(Map<String, Object> taskRoomInfo_param);

	String getMonitorNameByRoomID(String roomID);
}
