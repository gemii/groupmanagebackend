package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.ChatRecordRes;
import cc.gemii.pojo.ChatRecordResCustom;
import cc.gemii.pojo.RoomInfo;
import cc.gemii.pojo.RoomInfoAPI;
import cc.gemii.pojo.RoomMemReportResp;
import cc.gemii.pojo.RoomMsgReportResp;

public interface APIMapper {

	List<RoomInfoAPI> getRoomInfo(Map<String, Object> param);

	List<ChatRecordResCustom> getChatRecordExcel(Map<String, Object> param);

	List<RoomInfo> getRooms();

	List<ChatRecordRes> getChatRecord(Map<String, Object> param);

	List<RoomMemReportResp> getRoomMemReport(String date);

	List<RoomMsgReportResp> getRoomMsgReport(String date);
}
