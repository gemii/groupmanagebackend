package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.po.Wechatroommessagespecial;
import cc.gemii.pojo.RoomMessageRecord;

public interface MessageMapper {

	List<RoomMessageRecord> getChatRecordLast(Map<String, String> param);

	List<RoomMessageRecord> getChatRecordNext(Map<String, String> param);

	List<RoomMessageRecord> getChatRecordLimit(Map<String, String> param);

	void modifyMessageStatus(Map<String, Object> param);

	List<RoomMessageRecord> getSpecialMessageLast(Map<String, Object> param);

	void modifySpecialMessageStatus(Map<String, Object> param);

	void insertSpecialMessage(Wechatroommessagespecial specialMessage);

	void updateSepcialMessageClick(String msgid);
}
