package cc.gemii.mapper.custom;

public interface TimeTaskMapper {

	void taskExpire();

	void deleteUnusedMonitorRobotBind();

}
