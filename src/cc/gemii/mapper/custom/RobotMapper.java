package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.po.Monitorrobotbind;
import cc.gemii.po.Wechatrobotinfo;
import cc.gemii.pojo.MonitorRobotBindCustom;

public interface RobotMapper {

	Monitorrobotbind getSendRobotByRoomID(Map<String, Object> param);

	String getRobotIDByRobotTag(String key);

	Wechatrobotinfo getRobotByRobotTag(String robottag);

	List<MonitorRobotBindCustom> getSendRobotByMid(Integer mID);

	String getChannelByRoomID(String roomID);

	List<String> getAllChannels();

	String getMaxTag();

	void insertSendRobotRole(String nextTag);

}
