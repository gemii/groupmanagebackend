package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.po.Roomkeyword;

public interface KeywordMapper {

	void deleteKeyword(Map<String, String> map);

	void insertKeyword(Map<String, Object> map);

	void modifyStatus(Map<String, String> map);

	Roomkeyword getKeyword(Map<String, Object> map);

	List<String> getKeywordByRoomID(String roomID);

	List<Roomkeyword> isExists(Map<String, String> map);

	List<String> getKeywordByRidG(String roomID);
	
	String selectBelong(String mid);

	List<String> getKeyWordByMap(Map<String, String> param);

	List<String> selectCommonKeyWord();

	List<String> selectCommonDelKeyWord();

	List<String> selectLoveBabyKeyWord();

	List<String> selectFrisoKeyWord();

	List<String> selectLoveBabyDelKeyWord();

	List<String> selectFrisoDelKeyWord();

	List<String> selectKeyWordByKey(String owner);

	List<String> selectDelKeyWordByKey(String owner);

	List<String> selectCustomKeyWordByRoomid(String roomid);

	List<String> selectRoomidsByKeyWord(Map<String, String> map);
}
