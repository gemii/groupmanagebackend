package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.po.Membertag;

public interface MemberMapper {

	List<Membertag> getMemberTags(Map<String, String> param);

	void insertMemberTagBind(Map<String, Object> param);

	void deleteMemberTagBind(Map<String, Object> param);

	List<Membertag> getGlobalMemberTags(Map<String, Object> param);

	List<Membertag> getDIYMemberTags(Map<String, Object> param);

}
