package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.MicroTaskCustom;
import cc.gemii.pojo.TaskUser;


public interface MicroTaskMapper {

	int getMicroTaskListCount(Map<String, Object> param);

	void insertMicroTaskRoomID(Map<String, Object> param);

	List<String> validateRoomIDs(Map<String, Object> param);

	List<String> getCityByArea(String area);

	List<MicroTaskCustom> getMicroTaskList(Map<String, Object> param);

	void delete(Integer taskID);

	MicroTaskCustom getDetailByID(Integer taskID);

	List<String> getRoomIDsByTaskID(Integer taskID);

	void updateTaskEnable(Integer taskID);

	void deleteMicroTaskRoomID(Integer id);

	TaskUser login(Map<String, Object> param);

	void updateImageText(Map<String, Object> param);

}
