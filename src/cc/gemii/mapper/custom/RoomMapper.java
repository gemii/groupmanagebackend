package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.po.Monitor;
import cc.gemii.po.Wechatroominfo;
import cc.gemii.pojo.ExcelTemplate;
import cc.gemii.pojo.RoomInfo;
import cc.gemii.pojo.RoomKeywordCustom;
import cc.gemii.pojo.WeekLiveness;

public interface RoomMapper {

	List<RoomInfo> getRoomByMid(Map<String, Object> param);

	List<RoomKeywordCustom> getKeywordByMid(String mID);

	List<RoomInfo> getRoomByKeyword(Map<String, Object> map);

	List<String> getRoomByRobotTag(String robottag);

	List<RoomInfo> getManagedRoom(Map<String, Object> param);

	void markRoom(Map<String, Object> param);

	List<RoomInfo> getKeywordMessageUnread(Map<String, Object> param);

	List<RoomInfo> getAdminMessageUnread(Map<String, Object> param);

	List<RoomInfo> getCSRRoom(String owner);
	
	List<RoomInfo> getCSRRoomByPage(Map<String, Object> param);

	List<String> validateRoomIDs(Map<String, Object> param);

	List<String> lowestLiveness(Map<String, Object> param);

	List<WeekLiveness> livenessList(Map<String, Object> param);
	
	RoomInfo selectWechatRoomByRoomId(String roomId);

	List<Wechatroominfo> getRoomByExcelTemplate(ExcelTemplate eTemplate);

	String findURoomIdByRoomId(String roomId);

	String findRoomByRoomName(String roomName);

	List<RoomInfo> findRoomStatisticsInfo();

	Monitor findMonitorByRoomId(String roomid);

	String selectOwnerByRoomId(String roomID);
}
