package cc.gemii.mapper;

import cc.gemii.po.Knowledgeanswer;
import cc.gemii.po.KnowledgeanswerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface KnowledgeanswerMapper {
    int countByExample(KnowledgeanswerExample example);

    int deleteByExample(KnowledgeanswerExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Knowledgeanswer record);

    int insertSelective(Knowledgeanswer record);

    List<Knowledgeanswer> selectByExampleWithBLOBs(KnowledgeanswerExample example);

    List<Knowledgeanswer> selectByExample(KnowledgeanswerExample example);

    Knowledgeanswer selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Knowledgeanswer record, @Param("example") KnowledgeanswerExample example);

    int updateByExampleWithBLOBs(@Param("record") Knowledgeanswer record, @Param("example") KnowledgeanswerExample example);

    int updateByExample(@Param("record") Knowledgeanswer record, @Param("example") KnowledgeanswerExample example);

    int updateByPrimaryKeySelective(Knowledgeanswer record);

    int updateByPrimaryKeyWithBLOBs(Knowledgeanswer record);

    int updateByPrimaryKey(Knowledgeanswer record);
}