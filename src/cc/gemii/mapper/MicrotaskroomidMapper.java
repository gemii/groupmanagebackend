package cc.gemii.mapper;

import cc.gemii.po.Microtaskroomid;
import cc.gemii.po.MicrotaskroomidExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MicrotaskroomidMapper {
    int countByExample(MicrotaskroomidExample example);

    int deleteByExample(MicrotaskroomidExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Microtaskroomid record);

    int insertSelective(Microtaskroomid record);

    List<Microtaskroomid> selectByExample(MicrotaskroomidExample example);

    Microtaskroomid selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Microtaskroomid record, @Param("example") MicrotaskroomidExample example);

    int updateByExample(@Param("record") Microtaskroomid record, @Param("example") MicrotaskroomidExample example);

    int updateByPrimaryKeySelective(Microtaskroomid record);

    int updateByPrimaryKey(Microtaskroomid record);
}