package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatroominfo;
import cc.gemii.pojo.BuildMatchGroup;
import cc.gemii.pojo.LoveBabyInsert;

public interface FileUpMapper {
	
	void insertLoveBabyInfo(List<LoveBabyInsert> list);
	
	void insertMsInfo(List<LoveBabyInsert> list);
	
	int selectIsRepeat(@Param("roomName")String roomName);
	
	void updateInfo(@Param("roomId")String id,@Param("uRoomId")String uRoomId,@Param("createTime")String createTime);
	
	List<LoveBabyInsert> selectLoveBabyInfo(String taskId);
	
	void insertBatchInfo(@Param("task_id")String taskId,@Param("qr_code")String qrCode,@Param("verify_code")String code);

	void insertRoomInfo(List<BuildMatchGroup> list);
	
	void updateRoomInfo(@Param("id")String id,@Param("starTime")String starTime,@Param("endTime")String endTime);
	
	void insertKeyWord(@Param("id")String id,@Param("city")String city,@Param("keyword")String keyword,@Param("start")String star,@Param("end")String end);

	void insertLyInfo(List<LoveBabyInsert> list);

	List<Wechatroominfo> selectRoomInfoByRoomName(@Param("roomName")String roomName);

	void updateRoomName(@Param("roomName")String newRoomName, @Param("roomId")String roomid);

	List<Wechatroominfo> selectCurrentCountAll();

	void updateCurrentCountAll(@Param("roomId")String roomid, @Param("currentCount")String currentCount);
}
