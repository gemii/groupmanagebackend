package cc.gemii.mapper;

import cc.gemii.po.Knowledgequestion;
import cc.gemii.po.KnowledgequestionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface KnowledgequestionMapper {
    int countByExample(KnowledgequestionExample example);

    int deleteByExample(KnowledgequestionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Knowledgequestion record);

    int insertSelective(Knowledgequestion record);

    List<Knowledgequestion> selectByExampleWithBLOBs(KnowledgequestionExample example);

    List<Knowledgequestion> selectByExample(KnowledgequestionExample example);

    Knowledgequestion selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Knowledgequestion record, @Param("example") KnowledgequestionExample example);

    int updateByExampleWithBLOBs(@Param("record") Knowledgequestion record, @Param("example") KnowledgequestionExample example);

    int updateByExample(@Param("record") Knowledgequestion record, @Param("example") KnowledgequestionExample example);

    int updateByPrimaryKeySelective(Knowledgequestion record);

    int updateByPrimaryKeyWithBLOBs(Knowledgequestion record);

    int updateByPrimaryKey(Knowledgequestion record);
}