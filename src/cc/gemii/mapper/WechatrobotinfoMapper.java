package cc.gemii.mapper;

import cc.gemii.po.Wechatrobotinfo;
import cc.gemii.po.WechatrobotinfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WechatrobotinfoMapper {
    int countByExample(WechatrobotinfoExample example);

    int deleteByExample(WechatrobotinfoExample example);

    int deleteByPrimaryKey(String robottag);

    int insert(Wechatrobotinfo record);

    int insertSelective(Wechatrobotinfo record);

    List<Wechatrobotinfo> selectByExample(WechatrobotinfoExample example);

    Wechatrobotinfo selectByPrimaryKey(String robottag);

    int updateByExampleSelective(@Param("record") Wechatrobotinfo record, @Param("example") WechatrobotinfoExample example);

    int updateByExample(@Param("record") Wechatrobotinfo record, @Param("example") WechatrobotinfoExample example);

    int updateByPrimaryKeySelective(Wechatrobotinfo record);

    int updateByPrimaryKey(Wechatrobotinfo record);
}