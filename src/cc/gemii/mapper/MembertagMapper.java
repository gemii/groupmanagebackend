package cc.gemii.mapper;

import cc.gemii.po.Membertag;
import cc.gemii.po.MembertagExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MembertagMapper {
    int countByExample(MembertagExample example);

    int deleteByExample(MembertagExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Membertag record);

    int insertSelective(Membertag record);

    List<Membertag> selectByExample(MembertagExample example);

    Membertag selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Membertag record, @Param("example") MembertagExample example);

    int updateByExample(@Param("record") Membertag record, @Param("example") MembertagExample example);

    int updateByPrimaryKeySelective(Membertag record);

    int updateByPrimaryKey(Membertag record);
}