package cc.gemii.mapper;

import cc.gemii.po.Roomkeyword;
import cc.gemii.po.RoomkeywordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RoomkeywordMapper {
    int countByExample(RoomkeywordExample example);

    int deleteByExample(RoomkeywordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Roomkeyword record);

    int insertSelective(Roomkeyword record);

    List<Roomkeyword> selectByExample(RoomkeywordExample example);

    Roomkeyword selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Roomkeyword record, @Param("example") RoomkeywordExample example);

    int updateByExample(@Param("record") Roomkeyword record, @Param("example") RoomkeywordExample example);

    int updateByPrimaryKeySelective(Roomkeyword record);

    int updateByPrimaryKey(Roomkeyword record);
}