package cc.gemii.pojo;
/**
 * ClassName:RooomMemReportResp.java Function: Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 29, 2017
 *
 * @see
 */
public class RoomMemReportResp {

	private Integer SeqID;
	
	private String RoomID;
	
	private String Area;
	
	private String Province;
	
	private String City;
	
	private String RelatedCity;
	
	private String Department;
	
	private String RoomAttr;
	
	private String RoomName;
	
	private String HospitalCode;
	
	private String HospitalName;
	
	private String AnchorCity;
	
	private String HelperStatus;
	
	private String HelperStatusChangeTime;
	
	private String MonitorStatus;
	
	private String MonitorStatusChangeTime;
	
	private String UpTime;
	
	private String MonthAge;
	
	private Integer MonitorNum;
	
	private Integer HelperNum;
	
	private Integer RobotNum;
	
	private Integer WhiteListNum;
	
	private Integer Kom;
	
	private Integer NewNumWork;
	
	private Integer NewNumNonWork;
	
	private Integer LeaveNum;
	
	private Integer UserNum;
	
	private Integer TotalNum;
	
	private Integer KickNum;

	public Integer getSeqID() {
		return SeqID;
	}

	public void setSeqID(Integer seqID) {
		SeqID = seqID;
	}

	public String getRoomID() {
		return RoomID;
	}

	public void setRoomID(String roomID) {
		RoomID = roomID;
	}

	public String getArea() {
		return Area;
	}

	public void setArea(String area) {
		Area = area;
	}

	public String getProvince() {
		return Province;
	}

	public void setProvince(String province) {
		Province = province;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getRelatedCity() {
		return RelatedCity;
	}

	public void setRelatedCity(String relatedCity) {
		RelatedCity = relatedCity;
	}

	public String getDepartment() {
		return Department;
	}

	public void setDepartment(String department) {
		Department = department;
	}

	public String getRoomAttr() {
		return RoomAttr;
	}

	public void setRoomAttr(String roomAttr) {
		RoomAttr = roomAttr;
	}

	public String getRoomName() {
		return RoomName;
	}

	public void setRoomName(String roomName) {
		RoomName = roomName;
	}

	public String getHospitalCode() {
		return HospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		HospitalCode = hospitalCode;
	}

	public String getHospitalName() {
		return HospitalName;
	}

	public void setHospitalName(String hospitalName) {
		HospitalName = hospitalName;
	}

	public String getAnchorCity() {
		return AnchorCity;
	}

	public void setAnchorCity(String anchorCity) {
		AnchorCity = anchorCity;
	}

	public String getHelperStatus() {
		return HelperStatus;
	}

	public void setHelperStatus(String helperStatus) {
		HelperStatus = helperStatus;
	}

	public String getHelperStatusChangeTime() {
		return HelperStatusChangeTime;
	}

	public void setHelperStatusChangeTime(String helperStatusChangeTime) {
		HelperStatusChangeTime = helperStatusChangeTime;
	}

	public String getMonitorStatus() {
		return MonitorStatus;
	}

	public void setMonitorStatus(String monitorStatus) {
		MonitorStatus = monitorStatus;
	}

	public String getMonitorStatusChangeTime() {
		return MonitorStatusChangeTime;
	}

	public void setMonitorStatusChangeTime(String monitorStatusChangeTime) {
		MonitorStatusChangeTime = monitorStatusChangeTime;
	}

	public String getUpTime() {
		return UpTime;
	}

	public void setUpTime(String upTime) {
		UpTime = upTime;
	}

	public String getMonthAge() {
		return MonthAge;
	}

	public void setMonthAge(String monthAge) {
		MonthAge = monthAge;
	}

	public Integer getMonitorNum() {
		return MonitorNum;
	}

	public void setMonitorNum(Integer monitorNum) {
		MonitorNum = monitorNum;
	}

	public Integer getHelperNum() {
		return HelperNum;
	}

	public void setHelperNum(Integer helperNum) {
		HelperNum = helperNum;
	}

	public Integer getRobotNum() {
		return RobotNum;
	}

	public void setRobotNum(Integer robotNum) {
		RobotNum = robotNum;
	}

	public Integer getWhiteListNum() {
		return WhiteListNum;
	}

	public void setWhiteListNum(Integer whiteListNum) {
		WhiteListNum = whiteListNum;
	}

	public Integer getKom() {
		return Kom;
	}

	public void setKom(Integer kom) {
		Kom = kom;
	}

	public Integer getNewNumWork() {
		return NewNumWork;
	}

	public void setNewNumWork(Integer newNumWork) {
		NewNumWork = newNumWork;
	}

	public Integer getNewNumNonWork() {
		return NewNumNonWork;
	}

	public void setNewNumNonWork(Integer newNumNonWork) {
		NewNumNonWork = newNumNonWork;
	}

	public Integer getLeaveNum() {
		return LeaveNum;
	}

	public void setLeaveNum(Integer leaveNum) {
		LeaveNum = leaveNum;
	}

	public Integer getUserNum() {
		return UserNum;
	}

	public void setUserNum(Integer userNum) {
		UserNum = userNum;
	}

	public Integer getTotalNum() {
		return TotalNum;
	}

	public void setTotalNum(Integer totalNum) {
		TotalNum = totalNum;
	}

	public Integer getKickNum() {
		return KickNum;
	}

	public void setKickNum(Integer kickNum) {
		KickNum = kickNum;
	}
	
}
