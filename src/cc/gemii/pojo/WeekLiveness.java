package cc.gemii.pojo;

public class WeekLiveness {

	private String roomID;
	
	private String roomName;
	
	private float last;
	
	private float last2;

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public float getLast() {
		return last;
	}

	public void setLast(float last) {
		this.last = last;
	}

	public float getLast2() {
		return last2;
	}

	public void setLast2(float last2) {
		this.last2 = last2;
	}
	
}
