package cc.gemii.pojo;

public class MicroTaskCustom{

	private Integer id;
	
	private String activityTitle;
	
	private String activityContent;
	
	private String activityImage;
	
	private String imageTextTitle;
	
	private String imageTextCover;
	
	private String imageTextURL;
	
	private String imageTextRemark;
	
	private String createTime;
	
	private String lastUpdateTime;
	
	private String activityShowStart;
	
	private String activityShowEnd;
	
	private String pushStart;
	
	private String pushEnd;
	
	private String pushTime;
	
	private String enable;

	private String roomIDs;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getActivityTitle() {
		return activityTitle;
	}

	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}

	public String getImageTextTitle() {
		return imageTextTitle;
	}

	public void setImageTextTitle(String imageTextTitle) {
		this.imageTextTitle = imageTextTitle;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}


	public String getActivityShowStart() {
		return activityShowStart;
	}

	public void setActivityShowStart(String activityShowStart) {
		this.activityShowStart = activityShowStart;
	}

	public String getActivityShowEnd() {
		return activityShowEnd;
	}

	public void setActivityShowEnd(String activityShowEnd) {
		this.activityShowEnd = activityShowEnd;
	}

	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	public String getActivityContent() {
		return activityContent;
	}

	public void setActivityContent(String activityContent) {
		this.activityContent = activityContent;
	}

	public String getActivityImage() {
		return activityImage;
	}

	public void setActivityImage(String activityImage) {
		this.activityImage = activityImage;
	}

	public String getImageTextCover() {
		return imageTextCover;
	}

	public void setImageTextCover(String imageTextCover) {
		this.imageTextCover = imageTextCover;
	}

	public String getImageTextURL() {
		return imageTextURL;
	}

	public void setImageTextURL(String imageTextURL) {
		this.imageTextURL = imageTextURL;
	}

	public String getImageTextRemark() {
		return imageTextRemark;
	}

	public void setImageTextRemark(String imageTextRemark) {
		this.imageTextRemark = imageTextRemark;
	}

	public String getPushStart() {
		return pushStart;
	}

	public void setPushStart(String pushStart) {
		this.pushStart = pushStart;
	}

	public String getPushEnd() {
		return pushEnd;
	}

	public void setPushEnd(String pushEnd) {
		this.pushEnd = pushEnd;
	}

	public String getPushTime() {
		return pushTime;
	}

	public void setPushTime(String pushTime) {
		this.pushTime = pushTime;
	}

	public String getRoomIDs() {
		return roomIDs;
	}

	public void setRoomIDs(String roomIDs) {
		this.roomIDs = roomIDs;
	}

}
