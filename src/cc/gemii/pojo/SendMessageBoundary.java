package cc.gemii.pojo;

import java.util.Arrays;

import org.springframework.web.multipart.MultipartFile;

public class SendMessageBoundary {

	private String message;
	
	private Integer mID;
	
	private String[] RoomIDs;
	
	private MultipartFile file;
	
	private Integer type;

	private Integer resendType;
	
	private String fileName;
	
	private String atAll;//@成员：0 @all，1 @成员或者不艾特人
	
	private String memberIds;//群成员ID(多个用,号隔开,如果不用艾特则传空)
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getmID() {
		return mID;
	}

	public void setmID(Integer mID) {
		this.mID = mID;
	}

	public String[] getRoomIDs() {
		return RoomIDs;
	}

	public void setRoomIDs(String[] roomIDs) {
		RoomIDs = roomIDs;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getResendType() {
		return resendType;
	}

	public void setResendType(Integer resendType) {
		this.resendType = resendType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getAtAll() {
		return atAll;
	}

	public void setAtAll(String atAll) {
		this.atAll = atAll;
	}

	public String getMemberIds() {
		return memberIds;
	}

	public void setMemberIds(String memberIds) {
		this.memberIds = memberIds;
	}

	@Override
	public String toString() {
		return "SendMessageBoundary [message=" + message + ", mID=" + mID
				+ ", RoomIDs=" + Arrays.toString(RoomIDs) + ", file=" + file.getOriginalFilename()
				+ ", type=" + type + "]";
	}

}
