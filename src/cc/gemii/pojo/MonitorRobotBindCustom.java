package cc.gemii.pojo;

import cc.gemii.po.Monitorrobotbind;

public class MonitorRobotBindCustom extends Monitorrobotbind{

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
