package cc.gemii.pojo;

public class LoveBabyInsert {
	
	private String NcCode;
	
	private String storeName;
	
	private String province;
	
	private String city;
	
	private String roomName;
	
	private String Id;
	
	private String taskId;
	
	private String uRoomId;
	
	private String createTime;

	private String type;
	
	private String stareTime;
	
	private String endTime;
	
	private String area;
	
	private String owner;
	
	private String status;

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStareTime() {
		return stareTime;
	}

	public void setStareTime(String stareTime) {
		this.stareTime = stareTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getuRoomId() {
		return uRoomId;
	}

	public void setuRoomId(String uRoomId) {
		this.uRoomId = uRoomId;
	}


	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getNcCode() {
		return NcCode;
	}

	public void setNcCode(String ncCode) {
		NcCode = ncCode;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

}
