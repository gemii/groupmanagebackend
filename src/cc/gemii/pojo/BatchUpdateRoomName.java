package cc.gemii.pojo;

public class BatchUpdateRoomName {

	private String roomName;
	
	private String newRoomName;

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getNewRoomName() {
		return newRoomName;
	}

	public void setNewRoomName(String newRoomName) {
		this.newRoomName = newRoomName;
	}
	
}
