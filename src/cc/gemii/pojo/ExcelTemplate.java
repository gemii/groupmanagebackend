package cc.gemii.pojo;

/**
 * excel文件上传模版
 * 
 * @author gemii.yangyang
 *
 */
public class ExcelTemplate {

	private String roomId;

	private String innerId;

	private String roomName;

	private String nowRoomName;

	private String username;

	private String city;

	private String memberNums;
	
	private String monitorId;
	
	private Integer oldMonitorId;

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getInnerId() {
		return innerId;
	}

	public void setInnerId(String innerId) {
		this.innerId = innerId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getNowRoomName() {
		return nowRoomName;
	}

	public void setNowRoomName(String nowRoomName) {
		this.nowRoomName = nowRoomName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMemberNums() {
		return memberNums;
	}

	public void setMemberNums(String memberNums) {
		this.memberNums = memberNums;
	}

	public String getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(String monitorId) {
		this.monitorId = monitorId;
	}

	public Integer getOldMonitorId() {
		return oldMonitorId;
	}

	public void setOldMonitorId(Integer oldMonitorId) {
		this.oldMonitorId = oldMonitorId;
	}

}
