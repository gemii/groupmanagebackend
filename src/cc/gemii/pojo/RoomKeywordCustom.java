package cc.gemii.pojo;

import cc.gemii.po.Roomkeyword;

public class RoomKeywordCustom extends Roomkeyword{

	private Integer count;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	
}
