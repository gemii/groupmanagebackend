package cc.gemii.pojo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import cc.gemii.util.TimeUtils;

public class RoomMessageRecord implements Comparable<RoomMessageRecord>{

	@JsonProperty("Content")
	private String Content;
	
	@JsonProperty("MsgType")
	private Integer MsgType;
	
	@JsonProperty("AppMsgType")
	private Integer AppMsgType;

	@JsonProperty("UserDisplayName")
	private String UserDisplayName;
	
	@JsonProperty("MsgId")
	private String MsgId;
	
	@JsonProperty("CreateTime")
	private String CreateTime;
	
	@JsonProperty("RoomID")
	private String RoomID;
	
	@JsonProperty("MemberID")
	private String MemberID;
	
	@JsonProperty("UserNickName")
	private String UserNickName;

	@JsonProperty("MemberIcon")
	private String MemberIcon;
	
	@JsonProperty("isLegal")
	private String isLegal;
	
	@JsonProperty("IsMonitor")
	private String IsMonitor;
	
	private Integer AltMonitor;
	
	private Integer Keyword;
	
	private Integer Click; 
	
	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public Integer getMsgType() {
		return MsgType;
	}

	public void setMsgType(Integer msgType) {
		MsgType = msgType;
	}

	public Integer getAppMsgType() {
		return AppMsgType;
	}

	public void setAppMsgType(Integer appMsgType) {
		AppMsgType = appMsgType;
	}

	public String getUserDisplayName() {
		return UserDisplayName;
	}

	public void setUserDisplayName(String userDisplayName) {
		UserDisplayName = userDisplayName;
	}

	public String getMsgId() {
		return MsgId;
	}

	public void setMsgId(String msgId) {
		MsgId = msgId;
	}

	public String getCreateTime() {
		return CreateTime;
	}

	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}

	public String getRoomID() {
		return RoomID;
	}

	public void setRoomID(String roomID) {
		RoomID = roomID;
	}

	public String getMemberID() {
		return MemberID;
	}

	public void setMemberID(String memberID) {
		MemberID = memberID;
	}

	public String getUserNickName() {
		return UserNickName;
	}

	public void setUserNickName(String userNickName) {
		UserNickName = userNickName;
	}

	public Integer getAltMonitor() {
		return AltMonitor;
	}

	public void setAltMonitor(Integer altMonitor) {
		AltMonitor = altMonitor;
	}

	public Integer getKeyword() {
		return Keyword;
	}

	public void setKeyword(Integer keyword) {
		Keyword = keyword;
	}

	public Integer getClick() {
		return Click;
	}

	public void setClick(Integer click) {
		Click = click;
	}
	
	public String getMemberIcon() {
		return MemberIcon;
	}

	public void setMemberIcon(String memberIcon) {
		MemberIcon = memberIcon;
	}

	public String getIsLegal() {
		return isLegal;
	}

	public void setIsLegal(String isLegal) {
		this.isLegal = isLegal;
	}
	
	public String getIsMonitor() {
		return IsMonitor;
	}

	public void setIsMonitor(String isMonitor) {
		IsMonitor = isMonitor;
	}

	public Map<String, Object> toMap(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Content", Content);
		map.put("MsgType", MsgType);
		map.put("AppMsgType", AppMsgType);
		map.put("UserDisplayName", UserDisplayName);
		map.put("MsgId", MsgId);
		map.put("CreateTime", CreateTime);
		map.put("RoomID", RoomID);
		map.put("MemberID", MemberID);
		map.put("UserNickName", UserNickName);
		map.put("MemberIcon", MemberIcon);
		map.put("isLegal", isLegal);
		map.put("IsMonitor", IsMonitor);
		return map;
	}

	@Override
	public int compareTo(RoomMessageRecord o) {
		Date date_this = TimeUtils.getDate(this.getCreateTime(), "yyyy-MM-dd HH:mm:ss");
		Date date_o = TimeUtils.getDate(o.getCreateTime(), "yyyy-MM-dd HH:mm:ss");
		if(date_this.equals(date_o) || date_this.after(date_o))
			return 1;
		else
			return -1;
	}

}

