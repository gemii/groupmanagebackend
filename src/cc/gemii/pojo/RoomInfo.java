package cc.gemii.pojo;

public class RoomInfo {

	private String roomID;
	
	private String roomName;

	private String uroomId;
	
	private String memberNum;
	
	private String innerID;
	
	private String groupAttr;
	
	private String area;
	
	private String city;
	
	private String tag;
	
	private Integer count;
	
	private String timestamp;
	
	private Integer flag;
	
	private String department;
	
	private String province;
	
	private String type;
	
	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public String getInnerID() {
		return innerID;
	}

	public void setInnerID(String innerID) {
		this.innerID = innerID;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getGroupAttr() {
		return groupAttr;
	}

	public void setGroupAttr(String groupAttr) {
		this.groupAttr = groupAttr;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUroomId() {
		return uroomId;
	}

	public void setUroomId(String uroomId) {
		this.uroomId = uroomId;
	}

	public String getMemberNum() {
		return memberNum;
	}

	public void setMemberNum(String memberNum) {
		this.memberNum = memberNum;
	}

}
