package cc.gemii.pojo;

/**
 * ClassName:RoomMsgReportResp.java Function: Reason:
 * 
 * @author zili.jin
 * @version
 * @since
 * @Date Apr 12, 2017
 *
 * @see
 */
public class RoomMsgReportResp {

	private String RoomID;
	
	private String Area;
	
	private String Province;
	
	private String City;
	
	private String RelatedCity;
	
	private String Department;
	
	private String RoomAttr;
	
	private String RoomName;
	
	private String HospitalCode;
	
	private String HospitalName;
	
	private String AnchorCity;
	
	private String HelperStatus;
	
	private String HelperStatusChangeTime;
	
	private String MonitorStatus;
	
	private String MonitorStatusChangeTime;
	
	private String Uptime;
	
	private String MonthAge;
	
	private Integer TotalNum;
	
	private Integer UserNum;
	
	private Integer ActiveNum;
	
	private float ActiveRate;
	
	private Integer MsgAmnt;
	
	private float DailyMsgAmnt;
	
	private Integer MonitorMentioned;
	
	private Integer MonitorMsgAmnt;
	
	private float MonitorMsgRate;
	
	private Integer MonitorMentionAll;
	
	private Integer KomMentioned;
	
	private Integer KomMsgAmnt;
	
	private float KomMsgRate;
	
	private Integer UserMsgAmnt;
	
	private float UserMsgRate;

	public String getRoomID() {
		return RoomID;
	}

	public void setRoomID(String roomID) {
		RoomID = roomID;
	}

	public String getArea() {
		return Area;
	}

	public void setArea(String area) {
		Area = area;
	}

	public String getProvince() {
		return Province;
	}

	public void setProvince(String province) {
		Province = province;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getRelatedCity() {
		return RelatedCity;
	}

	public void setRelatedCity(String relatedCity) {
		RelatedCity = relatedCity;
	}

	public String getDepartment() {
		return Department;
	}

	public void setDepartment(String department) {
		Department = department;
	}

	public String getRoomAttr() {
		return RoomAttr;
	}

	public void setRoomAttr(String roomAttr) {
		RoomAttr = roomAttr;
	}

	public String getRoomName() {
		return RoomName;
	}

	public void setRoomName(String roomName) {
		RoomName = roomName;
	}

	public String getHospitalCode() {
		return HospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		HospitalCode = hospitalCode;
	}

	public String getHospitalName() {
		return HospitalName;
	}

	public void setHospitalName(String hospitalName) {
		HospitalName = hospitalName;
	}

	public String getAnchorCity() {
		return AnchorCity;
	}

	public void setAnchorCity(String anchorCity) {
		AnchorCity = anchorCity;
	}

	public String getHelperStatus() {
		return HelperStatus;
	}

	public void setHelperStatus(String helperStatus) {
		HelperStatus = helperStatus;
	}

	public String getHelperStatusChangeTime() {
		return HelperStatusChangeTime;
	}

	public void setHelperStatusChangeTime(String helperStatusChangeTime) {
		HelperStatusChangeTime = helperStatusChangeTime;
	}

	public String getMonitorStatus() {
		return MonitorStatus;
	}

	public void setMonitorStatus(String monitorStatus) {
		MonitorStatus = monitorStatus;
	}

	public String getMonitorStatusChangeTime() {
		return MonitorStatusChangeTime;
	}

	public void setMonitorStatusChangeTime(String monitorStatusChangeTime) {
		MonitorStatusChangeTime = monitorStatusChangeTime;
	}

	public String getUptime() {
		return Uptime;
	}

	public void setUptime(String uptime) {
		Uptime = uptime;
	}

	public String getMonthAge() {
		return MonthAge;
	}

	public void setMonthAge(String monthAge) {
		MonthAge = monthAge;
	}

	public Integer getTotalNum() {
		return TotalNum;
	}

	public void setTotalNum(Integer totalNum) {
		TotalNum = totalNum;
	}

	public Integer getUserNum() {
		return UserNum;
	}

	public void setUserNum(Integer userNum) {
		UserNum = userNum;
	}

	public Integer getActiveNum() {
		return ActiveNum;
	}

	public void setActiveNum(Integer activeNum) {
		ActiveNum = activeNum;
	}

	public float getActiveRate() {
		return ActiveRate;
	}

	public void setActiveRate(float activeRate) {
		ActiveRate = activeRate;
	}

	public Integer getMsgAmnt() {
		return MsgAmnt;
	}

	public void setMsgAmnt(Integer msgAmnt) {
		MsgAmnt = msgAmnt;
	}

	public float getDailyMsgAmnt() {
		return DailyMsgAmnt;
	}

	public void setDailyMsgAmnt(float dailyMsgAmnt) {
		DailyMsgAmnt = dailyMsgAmnt;
	}

	public Integer getMonitorMentioned() {
		return MonitorMentioned;
	}

	public void setMonitorMentioned(Integer monitorMentioned) {
		MonitorMentioned = monitorMentioned;
	}

	public Integer getMonitorMsgAmnt() {
		return MonitorMsgAmnt;
	}

	public void setMonitorMsgAmnt(Integer monitorMsgAmnt) {
		MonitorMsgAmnt = monitorMsgAmnt;
	}

	public float getMonitorMsgRate() {
		return MonitorMsgRate;
	}

	public void setMonitorMsgRate(float monitorMsgRate) {
		MonitorMsgRate = monitorMsgRate;
	}

	public Integer getMonitorMentionAll() {
		return MonitorMentionAll;
	}

	public void setMonitorMentionAll(Integer monitorMentionAll) {
		MonitorMentionAll = monitorMentionAll;
	}

	public Integer getKomMentioned() {
		return KomMentioned;
	}

	public void setKomMentioned(Integer komMentioned) {
		KomMentioned = komMentioned;
	}

	public Integer getKomMsgAmnt() {
		return KomMsgAmnt;
	}

	public void setKomMsgAmnt(Integer komMsgAmnt) {
		KomMsgAmnt = komMsgAmnt;
	}

	public float getKomMsgRate() {
		return KomMsgRate;
	}

	public void setKomMsgRate(float komMsgRate) {
		KomMsgRate = komMsgRate;
	}

	public Integer getUserMsgAmnt() {
		return UserMsgAmnt;
	}

	public void setUserMsgAmnt(Integer userMsgAmnt) {
		UserMsgAmnt = userMsgAmnt;
	}

	public float getUserMsgRate() {
		return UserMsgRate;
	}

	public void setUserMsgRate(float userMsgRate) {
		UserMsgRate = userMsgRate;
	}

}
