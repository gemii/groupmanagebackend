package cc.gemii.pojo;

public class BuildMatchGroup {
	private String dsId;
	private String hospitalName;
	private String city;
	private String edc;
	private String nowRoomName;
	private String type;
	private String hospitalCode;
	private String abbreviation1;
	private String abbreviation2;
	private String abbreviation3;
	private String abbreviation4;
	private String abbreviation5;
	private String abbreviation6;
	public String getDsId() {
		return dsId;
	}
	public void setDsId(String dsId) {
		this.dsId = dsId;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getEdc() {
		return edc;
	}
	public void setEdc(String edc) {
		this.edc = edc;
	}
	public String getNowRoomName() {
		return nowRoomName;
	}
	public void setNowRoomName(String nowRoomName) {
		this.nowRoomName = nowRoomName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getHospitalCode() {
		return hospitalCode;
	}
	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	public String getAbbreviation1() {
		return abbreviation1;
	}
	public void setAbbreviation1(String abbreviation1) {
		this.abbreviation1 = abbreviation1;
	}
	public String getAbbreviation2() {
		return abbreviation2;
	}
	public void setAbbreviation2(String abbreviation2) {
		this.abbreviation2 = abbreviation2;
	}
	public String getAbbreviation3() {
		return abbreviation3;
	}
	public void setAbbreviation3(String abbreviation3) {
		this.abbreviation3 = abbreviation3;
	}
	public String getAbbreviation4() {
		return abbreviation4;
	}
	public void setAbbreviation4(String abbreviation4) {
		this.abbreviation4 = abbreviation4;
	}
	public String getAbbreviation5() {
		return abbreviation5;
	}
	public void setAbbreviation5(String abbreviation5) {
		this.abbreviation5 = abbreviation5;
	}
	public String getAbbreviation6() {
		return abbreviation6;
	}
	public void setAbbreviation6(String abbreviation6) {
		this.abbreviation6 = abbreviation6;
	}

}
