package cc.gemii.pojo;

public class ChatRecordRes {

	private String area;
	
	private String province;
	
	private String city;
	
	private String department;
	
	private String GroupAttr;
	
	private String NowRoomName;
	
	private String RoomID;
	
	private String CreateTime;
	
	private String UserDisplayName;
	
	private String UserNickName;
	
	private String status;
	
	private Integer MsgType;
	
	private Integer AppMsgType;
	
	private String Content;

	private String RelatedCity;
	
	private String MemberIcon;
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getGroupAttr() {
		return GroupAttr;
	}

	public void setGroupAttr(String groupAttr) {
		GroupAttr = groupAttr;
	}

	public String getNowRoomName() {
		return NowRoomName;
	}

	public void setNowRoomName(String nowRoomName) {
		NowRoomName = nowRoomName;
	}

	public String getRoomID() {
		return RoomID;
	}

	public void setRoomID(String roomID) {
		RoomID = roomID;
	}

	public String getCreateTime() {
		return CreateTime;
	}

	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}

	public String getUserDisplayName() {
		return UserDisplayName;
	}

	public void setUserDisplayName(String userDisplayName) {
		UserDisplayName = userDisplayName;
	}

	public String getUserNickName() {
		return UserNickName;
	}

	public void setUserNickName(String userNickName) {
		UserNickName = userNickName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getMsgType() {
		return MsgType;
	}

	public void setMsgType(Integer msgType) {
		MsgType = msgType;
	}

	public Integer getAppMsgType() {
		return AppMsgType;
	}

	public void setAppMsgType(Integer appMsgType) {
		AppMsgType = appMsgType;
	}

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public String getRelatedCity() {
		return RelatedCity;
	}

	public void setRelatedCity(String relatedCity) {
		RelatedCity = relatedCity;
	}

	public String getMemberIcon() {
		return MemberIcon;
	}

	public void setMemberIcon(String memberIcon) {
		MemberIcon = memberIcon;
	}

}
