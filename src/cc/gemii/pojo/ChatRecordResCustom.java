package cc.gemii.pojo;

public class ChatRecordResCustom extends ChatRecordRes {

	private String InnerID;
	
	private String MsgType_Content_CN;
	
	private String AppMsgType_Content_CN;
	
	public String getInnerID() {
		return InnerID;
	}

	public void setInnerID(String innerID) {
		InnerID = innerID;
	}

	public String getMsgType_Content_CN() {
		return MsgType_Content_CN;
	}

	public void setMsgType_Content_CN(String msgType_Content_CN) {
		MsgType_Content_CN = msgType_Content_CN;
	}

	public String getAppMsgType_Content_CN() {
		return AppMsgType_Content_CN;
	}

	public void setAppMsgType_Content_CN(String appMsgType_Content_CN) {
		AppMsgType_Content_CN = appMsgType_Content_CN;
	}
}
