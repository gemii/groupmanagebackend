package cc.gemii.pojo;

import java.util.List;

public class UpdateParamEntity {
	
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	private String taskId;
	
	private List<LoveBabyInsert> data;

	public List<LoveBabyInsert> getData() {
		return data;
	}

	public void setData(List<LoveBabyInsert> data) {
		this.data = data;
	}

}
