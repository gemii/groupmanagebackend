package cc.gemii.pojo;

public class RoomInfoAPI {

	private String roomName;
	
	private String roomID;
	
	private String roomAttr;
	
	private String count;

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public String getRoomAttr() {
		return roomAttr;
	}

	public void setRoomAttr(String roomAttr) {
		this.roomAttr = roomAttr;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}
	
}
