package cc.gemii.pojo;

import java.io.Serializable;

public class HaoQiUserInfoVO implements Serializable {
	
	private String nickName;
	private String openId;
	private String matchGroup;

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getMatchGroup() {
		return matchGroup;
	}

	public void setMatchGroup(String matchGroup) {
		this.matchGroup = matchGroup;
	}
}
