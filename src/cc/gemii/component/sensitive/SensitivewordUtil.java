package cc.gemii.component.sensitive;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;


/**
 * @Description: 敏感词过滤
 */
@Component
public class SensitivewordUtil {
	
//	private static Map<String,Object> sensitiveWordMaps = new HashMap<String,Object>();
	
//	private static Map<String,Object> keyWordMaps = new HashMap<String,Object>();//关键字列表
	
//	private static Map<String,Object> deleteMemberKeyWordMaps = new HashMap<String,Object>();//踢人关键字
	
	//重新定义一个静态map缓存各种关键字
	private static Map<String,Map<String,Object>> keyWordMap = new HashMap<>();//关键字列表(包含关键字以及T人关键字)
	
	/**
	 * 敏感词匹配类型：
	 * 例如存在两个敏感词：中国，中国人
	 * 最小匹配规则能匹配到  中国，
	 * 最大匹配规则能匹配到  中国人
	 * */
	public static enum MatchType{
		/**
		 * 最小匹配类型
		 * */
		MIN_MATCHTYPE,
		
		/**
		 * 最大匹配类型
		 * */
		MAX_MATCHTYPE
	}
	
	
	/**
	 * 判断关键字是否初始化
	 * @param key
	 * @return
	 */
	public static boolean hasInitKey(String key){
		if(keyWordMap.get(key) == null || keyWordMap.isEmpty()){
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * 初始化key对应的关键字
	 */
	public static void initKey(String key,String words){
		synchronized(key){
			if(hasInitKey(key)){
				return;
			}
			Map<String,Object>  sensitiveWordMap = new SensitiveWordInit().initKeyWord(words);
			keyWordMap.put(key, sensitiveWordMap);
		}
	}
	
//	public static boolean hasInit(String key){
//		if(sensitiveWordMaps.get(key)==null){
//			return false;
//		}else{
//			return true;
//		}
//	}
//	
//	public static void initSensitiveWord(String key,String words){
//		synchronized(key){
//			if(hasInit(key)){
//				return;
//			}
//			Map<String,Object>  sensitiveWordMap = new SensitiveWordInit().initKeyWord(words);
//			sensitiveWordMaps.put(key, sensitiveWordMap);
//		}
//	}
	
//	/**
//	 * 
//	* @Title: hasInitKeyWord 
//	* @Description: 判断是否初始化了关键字 
//	* @param @param key
//	* @param @return    设定文件 
//	* @return boolean    返回类型 
//	* @throws
//	 */
//	public static boolean hasInitKeyWord(String key){
//		if(keyWordMaps.get(key)==null){
//			return false;
//		}else{
//			return true;
//		}
//	}
//	
//	/**
//	 * 
//	* @Title: initKeyWord 
//	* @Description: 初始化关键字列表
//	* @param @param key
//	* @param @param words    设定文件 
//	* @return void    返回类型 
//	* @throws
//	 */
//	public static void initKeyWord(String key,String words){
//		synchronized(key){
//			if(hasInitKeyWord(key)){
//				return;
//			}
//			Map<String,Object>  keyWordMap = new SensitiveWordInit().initKeyWord(words);
//			keyWordMaps.put(key, keyWordMap);
//		}
//	}
	
//	/**
//	 * 
//	* @Title: initDeleteKeyWord 
//	* @Description: TODO 判断踢人关键字有没有初始化
//	* @param @return    设定文件 
//	* @return boolean    返回类型 
//	* @throws
//	 */
//	public static boolean hasInitDeleteMemberKeyWord(String key){
//		if(deleteMemberKeyWordMaps.get(key) == null){
//			return false;
//		}else{
//			return true;
//		}
//	}
//	
//	/**
//	 * 
//	* @Title: initDeleteKeyWord 
//	* @Description: TODO 初始化踢人关键字
//	* @param @param key
//	* @param @param words    设定文件 
//	* @return void    返回类型 
//	* @throws
//	 */
//	public static void initDeleteMemberKeyWord(String key,String words){
//		synchronized(key){
//			if(hasInitDeleteMemberKeyWord(key)){
//				return;
//			}
//			Map<String,Object>  keyWordMap = new SensitiveWordInit().initKeyWord(words);
//			deleteMemberKeyWordMaps.put(key, keyWordMap);
//		}
//	}
	
//	/**
//	 * 将map置空
//	 * 针对于全局关键字
//	 * @param mapKey 
//	 */
//	public static void  moveKeyWordMapToEmpty(String key){
//		if(keyWordMap.containsKey(key)){
//			//将key对应的map置空
//			keyWordMap.remove(key);
//		}
//	}
	
	/**
	 * 将map中key删除
	 * 针对于所有关键字
	 * @param key
	 */
	public static void moveKeyWordMap(String key) {
		if(keyWordMap.containsKey(key)){
			keyWordMap.remove(key);
		}
	}
	
	
//	public static void deleteMemberKeyWordMapToEmpty(){
//		deleteMemberKeyWordMaps = new HashMap<String,Object>();
//	}
	
	/**
	 * 判断文字是否包含敏感字符
	 * @param txt  文字
	 * @param matchType  匹配规则&nbsp;1：最小匹配规则，2：最大匹配规则
	 * @return 若包含返回true，否则返回false
	 */
	public static boolean isContaintSensitiveWord(String mapKey,String txt,MatchType matchType){
		boolean flag = false;
		for(int i = 0 ; i < txt.length() ; i++){
			int matchFlag = checkSensitiveWord(mapKey,txt, i, matchType); //判断是否包含敏感字符
			if(matchFlag > 0){    //大于0存在，返回true
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * 获取文字中的敏感词
	 * @param txt 文字
	 * @param matchType 匹配规则&nbsp;1：最小匹配规则，2：最大匹配规则
	 */
	public static Set<String> getSensitiveWord(String mapKey,String txt , MatchType matchType){
		Set<String> sensitiveWordList = new HashSet<String>();
		
		for(int i = 0 ; i < txt.length() ; i++){
			int length = checkSensitiveWord(mapKey,txt, i, matchType);    //判断是否包含敏感字符
			if(length > 0){    //存在,加入list中
				sensitiveWordList.add(txt.substring(i, i+length));
				i = i + length - 1;    //减1的原因，是因为for会自增
			}
		}
		
		return sensitiveWordList;
	}
	
	/**
	 * 替换敏感字字符
	 * @param txt
	 * @param matchType
	 * @param replaceChar 替换字符，默认*
	 */
	public static String replaceSensitiveWord(String mapKey,String txt,MatchType matchType,String replaceChar){
		String resultTxt = txt;
		Set<String> set = getSensitiveWord(mapKey,txt, matchType);     //获取所有的敏感词
		Iterator<String> iterator = set.iterator();
		String word = null;
		String replaceString = null;
		while (iterator.hasNext()) {
			word = iterator.next();
			replaceString = getReplaceChars(replaceChar, word.length());
			resultTxt = resultTxt.replaceAll(word, replaceString);
		}
		
		return resultTxt;
	}
	
	/**
	 * 获取替换字符串
	 * @param replaceChar
	 * @param length
	 */
	private static String getReplaceChars(String replaceChar,int length){
		String resultReplace = replaceChar;
		for(int i = 1 ; i < length ; i++){
			resultReplace += replaceChar;
		}
		
		return resultReplace;
	}
	
	/**
	 * 检查文字中是否包含敏感字符，检查规则如下：<br>
	 * @param txt
	 * @param beginIndex
	 * @param matchType
	 * @return，如果存在，则返回敏感词字符的长度，不存在返回0
	 */
	private static int checkSensitiveWord(String mapKey,String txt,int beginIndex,MatchType matchType){
		boolean  flag = false;    //敏感词结束标识位：用于敏感词只有1位的情况
		int matchFlag = 0;     //匹配标识数默认为0
		Map<String,Object> nowMap = null;
		nowMap = keyWordMap.get(mapKey);
//		if("common_keyword".equals(mapKey)){
//			nowMap= (Map<String, Object>) keyWordMaps.get(mapKey);
//		}
//		if("delete_key_word".equals(mapKey)){
//			nowMap= (Map<String, Object>) deleteMemberKeyWordMaps.get(mapKey);
//		}
		
		if(nowMap == null){
			return -1;
		}
		for(int i = beginIndex; i < txt.length() ; i++){
			String word = String.valueOf(txt.charAt(i));
			nowMap = (Map<String,Object>) nowMap.get(word);     //获取指定key
			if(nowMap != null){     //存在，则判断是否为最后一个
				matchFlag++;     //找到相应key，匹配标识+1 
				if("1".equals(nowMap.get("isEnd"))){       //如果为最后一个匹配规则,结束循环，返回匹配标识数
					flag = true;       //结束标志位为true   
					if(MatchType.MIN_MATCHTYPE == matchType){    //最小规则，直接返回,最大规则还需继续查找
						break;
					}
				}
			}else{     //不存在，直接返回
				break;
			}
		}
		if(matchFlag < 2 || !flag){        //长度必须大于等于1，为词 
			matchFlag = 0;
		}
		return matchFlag;
	}

	
}
