package cc.gemii.component.sensitive;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * @Description: 初始化敏感词库，将敏感词加入到HashMap中，构建DFA算法模型
 */
public class SensitiveWordInit {
	
	private Logger logger = Logger.getLogger(this.getClass());
	/**
	 * @author chenming 
	 * @date 2014年4月20日 下午2:28:32
	 * @version 1.0
	 */
	public Map<String,Object> initKeyWord(String sensitiveWord){
		try {
				Set<String> keyWordSet = new HashSet<String>();
				String[] sensitiveWords= sensitiveWord.split(",");
				for (String str : sensitiveWords) {
					keyWordSet.add(str);
				}
				//读取敏感词库
				//将敏感词库加入到HashMap中
				return addSensitiveWordToHashMap(keyWordSet);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		return null;
	}

	/**
	 * 读取敏感词库，将敏感词放入HashSet中，构建一个DFA算法模型：<br>
	 * 中 = {
	 *      isEnd = 0
	 *      国 = {<br>
	 *      	 isEnd = 1
	 *           人 = {isEnd = 0
	 *                民 = {isEnd = 1}
	 *                }
	 *           男  = {
	 *           	   isEnd = 0
	 *           		人 = {
	 *           			 isEnd = 1
	 *           			}
	 *           	}
	 *           }
	 *      }
	 *  五 = {
	 *      isEnd = 0
	 *      星 = {
	 *      	isEnd = 0
	 *      	红 = {
	 *              isEnd = 0
	 *              旗 = {
	 *                   isEnd = 1
	 *                  }
	 *              }
	 *      	}
	 *      }
	 * @date 2014年4月20日 下午3:04:20
	 * @param keyWordSet  敏感词库
	 */
	private Map<String,Object> addSensitiveWordToHashMap(Set<String> keyWordSet) {
		Map<String,Object> sensitiveWordMap = new HashMap<String,Object>(keyWordSet.size());     //初始化敏感词容器，减少扩容操作
		String key = null;  
		Map<String,Object> nowMap = null;
		Map<String, Object> newWorMap = null;
		//迭代keyWordSet
		Iterator<String> iterator = keyWordSet.iterator();
		while(iterator.hasNext()){
			key = iterator.next();    //关键字
			nowMap = sensitiveWordMap;
			for(int i = 0 ; i < key.length() ; i++){
				String keyChar = String.valueOf(key.charAt(i));       //转换成char型
				Object wordMap = nowMap.get(keyChar);       //获取
				if(wordMap != null){        //如果存在该key，直接赋值
					nowMap = (Map<String,Object>) wordMap;
				}
				else{     //不存在则，则构建一个map，同时将isEnd设置为0，因为他不是最后一个
					newWorMap = new HashMap<String,Object>();
					newWorMap.put("isEnd", "0");     //不是最后一个
					nowMap.put(keyChar,newWorMap);
					nowMap = newWorMap;
				}
				if(i == key.length() - 1){
					nowMap.put("isEnd", "1");    //最后一个
				}
			}
		}
		return sensitiveWordMap;
	}
	
}
