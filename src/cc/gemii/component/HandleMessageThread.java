package cc.gemii.component;

import java.util.Map;

import org.apache.log4j.Logger;

import cc.gemii.service.MessageService;

public class HandleMessageThread extends Thread{

	private Logger logger = Logger.getLogger(this.getClass());
	
//	private Hook hook;
//	
//	public HandleMessageThread() {
//		ShutdownService shutdownService = SpringContextUtil.getBean(ShutdownService.class);
//		hook = shutdownService.CreateHook(this);
//	}
	
	private static boolean keepRuning = true;
	
	public void shutdown(){
		keepRuning = false;
		this.interrupt();
	}
	
	@Override
	public void run() {
		this.setName("HandleMessageThread");
		logger.info("start handleMessage thread");
		MessageService messageService = SpringContextUtil.getBean(MessageService.class);
		Map<String, Object> map = null;
		while(keepRuning){
			try {
				map = SpecialMessageQueue.take();
				if(map != null){
					messageService.insertSpecialMessage(map);
				}
			} catch(InterruptedException e){
				Thread.currentThread().interrupt();
			} catch (Exception e) {
				logger.error(map);
				logger.error(e.getMessage(),e);
			}
			
		}
		
		logger.info("handle message thread stop");
	}

}
