package cc.gemii.component;

import java.util.Set;

import javax.websocket.Endpoint;
import javax.websocket.server.ServerApplicationConfig;
import javax.websocket.server.ServerEndpointConfig;
/**
 * 
 * @author Jin
 *	在Tomcat启动的时候会加载此类(因为实现了ServerApplicationConfig接口)
 */
public class WebSocketConfig implements ServerApplicationConfig{

	@Override
	public Set<Class<?>> getAnnotatedEndpointClasses(Set<Class<?>> scan) {
		/*
		 * 这里会自动扫描带有@ServerEndpoint注解的类
		 * 返回它们的类名集合,这里可以做一些过滤,比如过滤测试用的带有@ServerEndpoint注解的类
		 * 最后一定要返回过滤后的set集合,相当于注册服务
		 * 最后一定要返回过滤后的set集合,相当于注册服务
		 * 最后一定要返回过滤后的set集合,相当于注册服务
		 */
		return scan;
	}

	@Override
	public Set<ServerEndpointConfig> getEndpointConfigs(
			Set<Class<? extends Endpoint>> arg0) {
		return null;
	}

}
