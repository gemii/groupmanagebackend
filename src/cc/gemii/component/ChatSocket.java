package cc.gemii.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import cc.gemii.util.ParamUtil;
import cc.gemii.util.RedisUtil;

/**
 * @author zili.jin
 */
@ServerEndpoint("/wechat")
public class ChatSocket {
	
	private static Logger logger = Logger.getLogger(ChatSocket.class);
	
	private static Map<String, ArrayList<Session>> sessionMap = new ConcurrentHashMap<String,ArrayList<Session>>();
	private static Map<Session, String> monitorMap = new ConcurrentHashMap<Session, String>();
	
	/**
	 * 每当有用户进入时,都会执行此函数
	 * @param session
	 */
	@OnOpen
	public void open(Session session) {
		logger.info("connnect open");
		try {
			String queryString = session.getQueryString();
			String[] querys = queryString.split("&");
			String[] queryTags = querys[0].split("=");
			String monitorId = querys[1].split("=")[1];
			//保存session
			if (!sessionMap.containsKey(monitorId)){
				sessionMap.put(monitorId, new ArrayList<Session>());
			}
			ArrayList<Session> sessionList =  sessionMap.get(monitorId);
			sessionList.add(session);
			sessionMap.put(monitorId,sessionList);
			monitorMap.put(session,monitorId);
			//订阅频道
			if(queryTags.length != 1){
				if(StringUtils.isNotBlank(queryTags[1])){
					RedisUtil.sendSubscribeMessage(Arrays.asList(queryTags[1].split(",")), "add");
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
	}

	/**
	 * 当有用户发送消息时,会执行此函数
	 * 
	 * @param session
	 *            为此用户开启的session
	 * @param msg
	 *            用户发送的消息文本
	 */
	@OnMessage
	public void receive(Session session, String msg) {
		//心跳包
		if(ParamUtil.WEBSOCKET_PING.equals(msg) && monitorMap.get(session) != null){
			broadcast(session, ParamUtil.WEBSOCKET_PONG);
		}
	}

	/**
	 * @param session
	 *        为此用户开启的session
	 */
	@OnClose
	public void close(Session session) {
		logger.info("connect broken");
		removeSession(session);
	}

	@OnError  
    public void error(Session session, Throwable error) {  
		logger.info("connect error");
		error.printStackTrace();
		removeSession(session); 
    }  
	
	private static void removeSession(Session session){
		String monitorId = monitorMap.get(session);
		if(StringUtils.isNotBlank(monitorId)){
			monitorMap.remove(session);
			ArrayList<Session> sessionList = sessionMap.get(monitorId);
			if(sessionList != null && sessionList.size() > 0){
				sessionList.remove(session);
			}
		}
	}
	/**
	 * @param session
	 *            该用户对应的session
	 * @param msg
	 *            向用户发送的数据 TODO
	 * @throws IOException 
	 */
	private static void broadcast (Session session, String msg) { 
		try {
			session.getAsyncRemote().sendText(msg);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			removeSession(session);
		}
	}

	/**
	 * @param json
	 *            接受到数据,转发给前端
	 */
	public static synchronized void sendMsg(String mID, String json) {
		ArrayList<Session> sessionList = sessionMap.get(mID);
		if(sessionList == null || sessionList.size() == 0){
			return;
		}
		for (Session session : sessionList){
			broadcast(session, json);
		}
	}
	
}
