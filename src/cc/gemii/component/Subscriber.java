package cc.gemii.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;

import cc.gemii.service.MessageService;
import cc.gemii.service.MonitorService;
import cc.gemii.util.ParamUtil;
import cc.gemii.util.RedisUtil;
import redis.clients.jedis.JedisPubSub;

@Component
public class Subscriber extends JedisPubSub{
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private MessageService messageService;
	@Autowired
	private MonitorService monitorService;
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;
	@Autowired
	private Properties propertyConfigurer;
	
	@Override
	public void subscribe(String... channels) {
		super.subscribe(channels);
	}

	@Override
	public void unsubscribe(String... channels) {
		super.unsubscribe(channels);
	}

	@Override
	public void onMessage(String channel, String message) {
		try {
			Map<String, Object> map = (Map<String, Object>) JSON.parse(message);
			String MsgType = String.valueOf(map.get("MsgType"));
			//消息类型: 添加监听频道
			switch (MsgType) {
				case ParamUtil.ADD_CHANNEL:
					//添加频道消息
					this.addChannel(map);
					return ;
				case ParamUtil.REMOVE_CHANNEL:
					//取消频道消息
					this.removeChannel(map);
					return ;
				case ParamUtil.SEND_MSG_RESPONSE:
					//发送消息反馈
					this.handleSendMsgResponse(map);
					return ;
				default:
					final String message_copy = message;
					handleMessage(message_copy);
					break;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
	}

	@Override
	public void onSubscribe(String channel, int subscribedChannels) {
		super.onSubscribe(channel, subscribedChannels);
	}

	@Override
	public void unsubscribe() {
		super.unsubscribe();
	}
	
	/**
	 * 
	 * 新添加用户添加频道,当已有用户添加过此频道那么此频道count+1
	 * 如果未有用户添加过此频道,此频道count=1
	 * count : redis中此频道监听记录数  IP channel count
	 * @param map
	 * TODO
	 */
	private void addChannel(Map<String, Object> map) {
		Map<String, Object> Msg = (Map<String, Object>) map.get("Msg");
		List<String> addChannels = (List<String>) Msg.get("Content");
		List<String> goHeavy = new ArrayList<String>();
		for (String addChannel : addChannels) {
			String count = RedisUtil.hget(RedisUtil.getRealIP(), addChannel);
			if(count == null){
				RedisUtil.hset(RedisUtil.getRealIP(), addChannel, 1 + "");
				goHeavy.add(addChannel);
			}else{
				RedisUtil.hincyby(RedisUtil.getRealIP(), addChannel, Long.valueOf(1));
			}
		}
		if(goHeavy.size() > 0){
			logger.info("subscribe redis, channel:" + StringUtils.join(addChannels, " ") + ", thread will be blocked.");
			this.subscribe(goHeavy.toArray(new String[goHeavy.size()]));
		}
	}
	
	/**
	 * 
	 * 用户退出系统,处理此用户所属频道,如果此频道count>1,那么count-1
	 * 如果此频道count=1,那么redis客户端取消订阅此频道
	 * @param map
	 * TODO
	 */
	private void removeChannel(Map<String, Object> map) {
		Map<String, Object> Msg = (Map<String, Object>) map.get("Msg");
		List<String> removeChannels = (List<String>) Msg.get("Content");
		List<String> excludeChannels = new ArrayList<String>();
		for (String removeChannel : removeChannels) {
			String count = RedisUtil.hget(RedisUtil.getRealIP(), removeChannel);
			if("1".equals(count) || Integer.parseInt(count) <= 0 ){
				excludeChannels.add(removeChannel);
				RedisUtil.hdel(RedisUtil.getRealIP(), removeChannel);
			}else{
				RedisUtil.hincyby(RedisUtil.getRealIP(), removeChannel,Long.valueOf(-1));
			}
		}
		
		if(excludeChannels.size() > 0){
			this.unsubscribe(excludeChannels.toArray(new String[excludeChannels.size()]));
		}
	}
	
	/**
	 * 处理单发和群发消息反馈
	 * @param map
	 * TODO
	 */
	private void handleSendMsgResponse(Map<String, Object> map) {
		List<Map<String, String>> returnMsgs = (List<Map<String, String>>) map.get("ReturnMsg");
		if(returnMsgs.size() > 0){
			ChatSocket.sendMsg(String.valueOf(monitorService.getMonitorByRoomID(returnMsgs.get(0).get("RoomID"))), JSON.toJSONString(map));
		}
	}
	
	/**
	 * 处理普通消息
	 * @param map
	 */
	private void handleMessage(final String message_copy) {
		taskExecutor.execute(new Runnable() {
			@Override
			public void run() {
				Map<String, Object> map = (Map<String, Object>) JSON.parse(message_copy);
				this.handleMessage(map);
			}
			
			private void handleMessage(Map<String, Object> map) {
				boolean flag = "open".equals(propertyConfigurer.getProperty(ParamUtil.HAOQI_KEYWORDS_SWITCH));
				//添加对好奇关键字的处理
				if(flag){
					messageService.handleHaoQiMessage(map);
				}
				try {
					map = messageService.handleMessage(map, true);
					if(map != null){
						ChatSocket.sendMsg(String.valueOf(map.get("mID")), JSON.toJSONString(map));
					}
				} catch (Exception e) {
					logger.error(e.getMessage(),e);
				}
			}
		});
//		//添加对好奇关键字的处理
//		messageService.handleHaoQiMessage(map);
//		try {
//			map = messageService.handleMessage(map, true);
//			if(map != null){
//				ChatSocket.sendMsg(String.valueOf(map.get("mID")), JSON.toJSONString(map));
//			}
//		} catch (Exception e) {
//			logger.error(e.getMessage(),e);
//		}
	}
	
}
