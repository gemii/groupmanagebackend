package cc.gemii.component;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

public class SpecialMessageQueue {

	private static Logger logger = Logger.getLogger(SpecialMessageQueue.class);

	private static BlockingQueue<Map<String, Object>> mq = new LinkedBlockingQueue<Map<String, Object>>();

	public static Boolean offer(Map<String, Object> node){
		return mq.offer(node);
	}
	
	public static Map<String, Object> take() throws Exception{
		return mq.take();
	}
	
	public static int size(){
		return mq.size();
	}
}
