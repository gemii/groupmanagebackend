package cc.gemii.component.wechat;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.fuckemoji.EmojiUtil;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.WechatUtil;
import net.sf.json.JSONObject;

@Component
public class SendTemplateMsg {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private Properties propertyConfigurer;
	
	public String sendHaoqTemplate(TemplateMsg templateMsg) throws UnsupportedEncodingException{
		
		String urlSwitch = propertyConfigurer.getProperty("wechat.sendHaoqTemplate.switch");
		if(!"open".equals(urlSwitch)){
			return "interface switch closed";
		}
		String openid = templateMsg.getOpenid();
		String url = propertyConfigurer.getProperty("wechat.sendHaoqTemplate.url");
		String token = propertyConfigurer.getProperty("wechat.sendHaoqTemplate.token");
		String timestamp = WechatUtil.getTimestamp();
		
		Formatter fm = new Formatter();
		String firstTemplate = propertyConfigurer.getProperty("wechat.sendHaoqTemplate.first");
		String first = String.valueOf(fm.format(firstTemplate, templateMsg.getFirst()));
		String keyword1Template = propertyConfigurer.getProperty("wechat.sendHaoqTemplate.keyword1");
		fm = new Formatter();
		String keyword1 = String.valueOf(fm.format(keyword1Template, templateMsg.getKeyword1()));
		String keyword2 = propertyConfigurer.getProperty("wechat.sendHaoqTemplate.keyword2");
		
		Map<String,String> params = new HashMap<String,String>();
		params.put("token", token);
		params.put("timestamp", timestamp);
		params.put("openid", openid);
		params.put("first", URLEncoder.encode(filterEmoji(first),"utf-8"));
		params.put("keyword1",URLEncoder.encode(filterEmoji(keyword1),"utf-8"));
		params.put("keyword2", URLEncoder.encode(filterEmoji(keyword2),"utf-8"));
		params.put("remark", "");
		
		String sign =  WechatUtil.getSignature(params);
		params.put("sign", sign);
		params.remove("token");
		
		logger.info("sendTemplateMsg-sendHaoqTemplate-url:"+url+"-param:"+JSONObject.fromObject(params).toString());
		String response = HttpClientUtil.doGet(url, params);
		logger.info("sendTemplateMsg-sendHaoqTemplate-result:"+response);
		
		return response;
	}
	
	private String filterEmoji(String htmlStr) { 
		String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式 
        String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式 
        String regEx_html="<[^>]+>"; //定义HTML标签的正则表达式 
         
        Pattern p_script=Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE); 
        Matcher m_script=p_script.matcher(htmlStr); 
        htmlStr=m_script.replaceAll(""); //过滤script标签 
         
        Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE); 
        Matcher m_style=p_style.matcher(htmlStr); 
        htmlStr=m_style.replaceAll(""); //过滤style标签 
         
        Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE); 
        Matcher m_html=p_html.matcher(htmlStr); 
        htmlStr=m_html.replaceAll(""); //过滤html标签 

        return htmlStr.trim(); //返回文本字符串 
}
}
