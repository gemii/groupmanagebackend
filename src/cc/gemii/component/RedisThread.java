package cc.gemii.component;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import cc.gemii.util.RedisUtil;
import redis.clients.jedis.Jedis;

public class RedisThread extends Thread {
    
	private Logger logger = Logger.getLogger(this.getClass());
	
	private static Subscriber subscriber = null;

    private String[] initChannels;
    
    public RedisThread(String[] initChannels){
    	subscriber = SpringContextUtil.getBean(Subscriber.class);
    	this.initChannels = initChannels;
    }
    
    public static void stopSubThread(){
    	subscriber.unsubscribe();
    }
    
    @Override
    public void run() {
    	
    	logger.info("subscribe redis, channel:" + StringUtils.join(initChannels, " ") + ", thread will be blocked.");
    	Jedis jedis = null;
        try {
        	jedis = RedisUtil.getResource();
        	jedis.subscribe(subscriber, this.initChannels);
        	logger.info("stop thread");
        } catch (Exception e) {
        	logger.error(e.getMessage(),e);
            logger.info(String.format("subscribe channel error, %s", e));
        } finally {
            RedisUtil.returnResource(jedis);
        }
    }

}