package cc.gemii.component.savepoint;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.util.HttpClientUtil;
import net.sf.json.JSONObject;

@Component
public class SavePointAdapter {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private Properties propertyConfigurer;
	
	public void savePoint(PointDTO pointDTO){
		
		String urlSwitch = propertyConfigurer.getProperty("helper.savepoint.switch");
		if(!"open".equals(urlSwitch)){
			return ;
		}
		
		String url = propertyConfigurer.getProperty("helper.savepoint.url");
		Map<String,String> params = new HashMap<String,String>();
		params.put("pageName",pointDTO.getPageName());
		params.put("pointName",pointDTO.getPointName());
		params.put("userKey",pointDTO.getUserKey());
		params.put("content",pointDTO.getContent());
		
		logger.info("sendTemplateMsg-sendHaoqTemplate-url:"+url+"-param:"+JSONObject.fromObject(params).toString());
		String response = HttpClientUtil.doGet(url, params);
		logger.info("sendTemplateMsg-sendHaoqTemplate-result:"+response);
	}
	
}
