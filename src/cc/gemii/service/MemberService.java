package cc.gemii.service;

import java.util.List;
import java.util.Map;

import cc.gemii.po.BlackWhiteListDO;
import cc.gemii.po.DeleteMemberHistory;
import cc.gemii.po.Wechatroommemberinfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.HaoQiUserInfoVO;

public interface MemberService {

	/**
	 * 
	 * @param roomID 群ID
	 * @param memberID 用户ID
	 * @param belong 1 gemii 2 wyeth
	 * @return 获取用户信息
	 * TODO
	 */
	CommonResult getMemberInfo(String roomID, String memberID, Integer belong);

	/**
	 * 
	 * @param belong 
	 * @return 标签列表
	 * TODO 取标签列表
	 */
	CommonResult getTagList(Integer belong);

	/**
	 * 
	 * @param roomID 群ID
	 * @param memberID 用户ID
	 * @param tags 标签数组
	 * @return
	 * TODO 更改用户标签
	 */
	CommonResult updateMemberTag(String roomID, String memberID, Integer[] tags);

	/**
	 * 根据群ID 查询该群中 好奇相关人员列表
	 * @param roolId 群Id
	 * @return 群成员列表
	 * */
	List<Wechatroommemberinfo> getHaoQiWechatMembers(String roomId);

	/**
	 * 删除群成员信息
	 * @param roomID 群ID
	 * @param memberID	成员ID
	 * @return
	 */
	CommonResult deleteRoomMemberInfo(String roomID, String memberID,String type);
	
	/**
	 * 
	* @Title: getHistoryData 
	* @Description: TODO 踢人历史记录
	* @param @param roomMember
	* @param @return    设定文件 
	* @return DeleteMemberHistory    返回类型 
	* @throws
	 */
	DeleteMemberHistory getHistoryData(Wechatroommemberinfo roomMember,String type);

	/**
	 * 
	* @Title: getMemberInfoByMap 
	* @Description: 获取入群类型
	* @param @param paramMap
	* @param @return    设定文件 
	* @return String    返回类型 
	* @throws
	 */
	List<Wechatroommemberinfo> getMemberInfoByMap(Map<String, String> paramMap);

	/**
	 * 
	* @Title: updateIsLegal 
	* @Description: TODO 设置白名单
	* @param @param roomID
	* @param @param memberID
	* @param @param isLegal
	* @param @return    设定文件 
	* @return CommonResult    返回类型 
	* @throws
	 */
	CommonResult updateIsLegal(String roomID, String memberID, String isLegal);

	/**
	 * 
	* @Title: findWhiteProId 
	* @Description: 查找白名单人员
	* @param @param map
	* @param @return    设定文件 
	* @return String    返回类型 
	* @throws
	 */
	String findWhiteProId(Map<String, String> map);

	/**
	 * 根据memberid去BlackWhiteList比对
	 * @param openid
	 * @return
	 */
	List<BlackWhiteListDO> selectWhiteByMemberId(Map<String, String> map);

}
