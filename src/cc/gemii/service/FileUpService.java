package cc.gemii.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import cc.gemii.pojo.BatchIntoGroup;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.LoveBabyInsert;
import cc.gemii.pojo.UpdateParamEntity;

public interface FileUpService {
	
	/**
	 * 爱婴岛excel插入数据
	 * @param file
	 * @return
	 */
	CommonResult insertFileInfo(MultipartFile file,BatchIntoGroup batchIntoGroup,String serNum);
	
	/**
	 * 修改群信息
	 * @param info
	 * @return
	 */
	CommonResult updateRoomInfo(UpdateParamEntity info);
	
	/**
	 * 添加医院自建群合作群网红群
	 * @param file
	 * @return
	 */
	CommonResult buildMatchGroup(MultipartFile file,String type);

	/**
	 * 批量修改群名
	 * @param file
	 * @return
	 */
	CommonResult batchUpdateRoomname(MultipartFile file);

	/**
	 * 更新所有群的群人数
	 * @param file
	 * @return
	 */
	CommonResult updateCurrentCountAll();
	

}
