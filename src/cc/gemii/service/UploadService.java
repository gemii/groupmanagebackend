package cc.gemii.service;

import org.springframework.web.multipart.MultipartFile;

import cc.gemii.pojo.CommonResult;

public interface UploadService {

	/**
	 * 
	 * @param base64String base64字符串
	 * @return
	 * TODO
	 */
	CommonResult uploadBase64(String base64String);

	CommonResult uploadFile(String dir, MultipartFile file);

	/**
	 * 微任务图片上传
	 * @param file
	 * @return
	 */
	CommonResult uploadMicroTaskFile(MultipartFile file);

}
