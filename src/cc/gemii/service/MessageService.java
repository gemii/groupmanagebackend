package cc.gemii.service;

import java.util.Map;

import cc.gemii.po.Wechatroommessagespecial;
import cc.gemii.pojo.CommonResult;

public interface MessageService {

	/**
	 * 
	 * @param roomID 群ID
	 * @param timestamp 时间戳
	 * @param type 类型  last向上翻页 next向下翻页 limit上下10条
	 * @return
	 * TODO 查看群历史聊天记录
	 */
	CommonResult getChatRecord(String roomID, String timestamp, String type);

	/**
	 * 
	 * @param mID 班长ID 当为关键词消息或@班长消息时 根据mID修改状态
	 * @param roomID 群ID
	 * @param type 类型 read 普通消息 keyword 关键词消息 admin @班长消息
	 * @return
	 * TODO 班长点击群,群消息由未读转为已读
	 */
	CommonResult modifyMessageStatus(Integer mID, String roomID, String type);

	/**
	 * 
	 * @param map 消息包装类
	 * @param isRedis 消息是否来自redis
	 * @return
	 * TODO 处理消息 是否包括关键字 @ 班长
	 */
	Map<String, Object> handleMessage(Map<String, Object> map, Boolean isRedis);

	/**
	 * 
	 * @param map 特殊消息map
	 * TODO 插入特殊消息
	 */
	void insertSpecialMessage(Map<String, Object> map);

	/**
	 * 
	 * @param read msgID包装类
	 * @return
	 * TODO 用户点击关键系或@管理员消息
	 */
	CommonResult readMessage(Wechatroommessagespecial read);

	/**
	 * 
	 * @param mID 班长ID
	 * @param timestamp 时间戳
	 * @param type 区分关键字消息或@班长消息
	 * @return
	 * TODO
	 */
	CommonResult getSpecialRecord(Integer mID, String timestamp, String type);

	void handleHaoQiMessage(Map<String, Object> map);

}
