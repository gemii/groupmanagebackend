package cc.gemii.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.GeneralPagingResult;
import cc.gemii.data.admin.ImportedMonitorRoomItem;
import cc.gemii.data.admin.MonitorRoomInfo;
import cc.gemii.mapper.MonitorMapper;
import cc.gemii.mapper.MonitorrobotbindMapper;
import cc.gemii.mapper.MonitorroomMapper;
import cc.gemii.mapper.MonitorworkrecordMapper;
import cc.gemii.mapper.SendapicallrecordMapper;
import cc.gemii.mapper.WechatroominfoMapper;
import cc.gemii.mapper.WechatroommemberinfoMapper;
import cc.gemii.mapper.custom.RobotMapper;
import cc.gemii.mapper.custom.RoomMapper;
import cc.gemii.mapper.custom.UserMapper;
import cc.gemii.po.Microtaskimagetext;
import cc.gemii.po.Monitor;
import cc.gemii.po.MonitorExample;
import cc.gemii.po.Monitorrobotbind;
import cc.gemii.po.Monitorroom;
import cc.gemii.po.MonitorroomExample;
import cc.gemii.po.Monitorworkrecord;
import cc.gemii.po.Sendapicallrecord;
import cc.gemii.po.Wechatroominfo;
import cc.gemii.po.Wechatroommemberinfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.ExcelTemplate;
import cc.gemii.pojo.MicroTaskCustom;
import cc.gemii.pojo.MonitorRobotBindCustom;
import cc.gemii.pojo.SendMessageBoundary;
import cc.gemii.service.MicroTaskService;
import cc.gemii.service.MonitorService;
import cc.gemii.util.AESUtil;
import cc.gemii.util.FileUtil;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.ImportExcel;
import cc.gemii.util.MD5Utils;
import cc.gemii.util.ParamUtil;
import cc.gemii.util.RedisUtil;
import cc.gemii.util.TimeUtils;

@Service
public class MonitorServiceImpl implements MonitorService{

	private Logger logger = Logger.getLogger(this.getClass());
	
	private final static String DEFAULT_PASSWORD = "qwerasdf";
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private MonitorMapper monitorMapper;
	
	@Autowired
	private RoomMapper roomMapper;
	
	@Autowired
	private MonitorworkrecordMapper workRecordMapper;
	
	@Autowired
	private MonitorrobotbindMapper monitorRobotMapper;
	
	@Autowired
	private RobotMapper robotMapper;
	
	@Autowired
	private SendapicallrecordMapper callRecordMapper;
	
	@Autowired
	private MonitorroomMapper monitorRoomMapper;
	
	@Autowired
	private Properties propertyConfigurer;
	
	@Autowired
	private WechatroommemberinfoMapper memberMapper;
	
	@Autowired
	private MicroTaskService microTaskService;
	@Autowired
	private WechatroominfoMapper wechatroominfoMapper;
	
	@Value("${AES_MONITOR_INFO}")
	private String AES_MONITOR_INFO;
	
	@Override
	public CommonResult login(String username, String password) {
		try {
			Map<String, String> param = new HashMap<String, String>();
			param.put("username", username);
			param.put("password", MD5Utils.md5Encode(password));
			Monitor monitor = userMapper.getMonitor(param);
			if(monitor == null){
				return CommonResult.build(-1, "账户名或密码错误");
			}
//			MonitorrobotbindExample bindExample = new MonitorrobotbindExample();
//			bindExample.createCriteria()
//				.andMonitoridEqualTo(monitor.getId())
//				.andTypeEqualTo("send");
			Map<String, Object> response = new HashMap<String, Object>();
			response.put("main", monitor);
			//response.put("sends", monitorRobotMapper.selectByExample(bindExample));
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("monitorId", monitor.getId());
			map.put("type", "send");
			response.put("sends", monitorRobotMapper.selectMonitorrobotbindByMap(map));
			return CommonResult.ok(AESUtil.encrypt2Str(JSON.toJSONString(response), AESUtil.PASSWORD));
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "错误");
		}
		
	}

	@Override
	public CommonResult getUserInfo(String data) {
		try {
			String json = AESUtil.decrypt2Str(data, AESUtil.PASSWORD);
			return CommonResult.ok((Map<String, Object>)JSON.parse(json));
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "error");
		}
	}
	@Override
	public CommonResult sendMessage(HttpServletRequest request, SendMessageBoundary sendMessage) {
		try {
			Integer type = sendMessage.getType();
			//发送机器人归类
			//Map<String, Object> tagMap = this.sendRobotClassify(sendMessage.getmID(), sendMessage.getRoomIDs());
			String result = "";
			switch (type) {
				case 1:
					//发送文本
					result = this.sendText(sendMessage.getmID(), sendMessage.getMessage(),//
							sendMessage.getAtAll(),sendMessage.getMemberIds(),sendMessage.getRoomIDs(),type);
					break ;
				case 2:
					//发送多媒体文件
					result = this.sendFile(sendMessage.getmID(), sendMessage.getFile(),//
							sendMessage.getAtAll(),sendMessage.getMemberIds(),sendMessage.getRoomIDs());
					break ;
				case 3:
					//根据url 发送
					String url = sendMessage.getMessage();
					String fileName = url.substring(url.lastIndexOf("/") + 1);
					result = this.sendFile(sendMessage.getmID(),  url, fileName,sendMessage.getAtAll(),//
							sendMessage.getMemberIds(),sendMessage.getRoomIDs());
					break ;
				case 4:
					//发送邀请卡片
					result=this.sendCard(sendMessage);
					break ;
				case 101:
					//重新发送
					result = this.resendLastRecord(sendMessage);
					break ;
				default:
					return CommonResult.build(-1, "unknown type");
			}
			
			//接口调用记录
			this.sendAPIRecord(request, sendMessage, result);
			return CommonResult.ok();
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "发送消息失败");
		}
	}

	private String sendCard(SendMessageBoundary sendMessage) throws Exception {
		return this.sendText(sendMessage.getmID(), sendMessage.getMessage(),//
				sendMessage.getAtAll(),sendMessage.getMemberIds(),sendMessage.getRoomIDs(),sendMessage.getType());
	}

	private void sendAPIRecord(HttpServletRequest request, SendMessageBoundary sendMessage, String result){
		Sendapicallrecord record = new Sendapicallrecord();
		record.setRemoteaddr(request.getRemoteAddr());
		record.setRemotehost(request.getRemoteHost());
		record.setRemoteport(request.getRemotePort() + "");
		record.setContent(result);
		record.setType(sendMessage.getType());
		record.setCreatetime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
		record.setMonitorid(sendMessage.getmID());
		record.setRoomid(StringUtils.join(sendMessage.getRoomIDs(), ","));
		callRecordMapper.insert(record);
	}
	
	@Override
	public CommonResult insertMonitorRecord(Integer mID, String action) {
		Monitorworkrecord record = new Monitorworkrecord();
		record.setAction(action);
		record.setMonitorid(mID);
		record.setCtime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
		workRecordMapper.insert(record);
		return CommonResult.ok();
	}

	@Override
	public CommonResult getMonitorInfo(Integer mID) {
		List<MonitorRobotBindCustom> sends = robotMapper.getSendRobotByMid(mID);
		List<Map<String, Object>> response = new ArrayList<Map<String,Object>>();
		
		Map<String, Object> each = null;
		Map<String, Object> param = null;
		for (MonitorRobotBindCustom bind : sends) {
			each = new HashMap<String, Object>();
			each.put("tag", bind.getRobottag());
			each.put("monitorName", bind.getMonitorname());
			
			param = new HashMap<String, Object>();
			param.put("robotTag", bind.getRobottag());
			param.put("mID", mID);
			each.put("managedRooms", roomMapper.getManagedRoom(param));
			if("online".equals(bind.getStatus())){
				each.put("status", "online");
				each.put("ownedRooms", roomMapper.getRoomByRobotTag(bind.getRobottag()));
			}else{
				each.put("status", "offline");
				each.put("ownedRooms", null);	
			}
			response.add(each);
		}
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult monitorLogin(String robotTag,HttpServletRequest request) {
		try {
			//发送请求
			String url = propertyConfigurer.getProperty("START_ROBOT_URL").replace("BOT", robotTag).replace("NEW", "true");
			//获取请求Ip
			String ip = getIpAddress(request);
			url = url+"&fip="+ip;
			logger.info("-------------------->url"+url);
			Map<String, String> responseMap = (Map<String, String>) JSON.parse(HttpClientUtil.doGet(url));
			//请求发送成功之后查询redis
			if(ParamUtil.START_ROBOT_SUCCESS.equals(String.valueOf(responseMap.get("code")))){
				for (int i = 0; i < 3; i++) {
					String urlJson = RedisUtil.hget(ParamUtil.ITEM_LOGIN, robotTag);
					if(urlJson != null){
						//删除redis记录
						RedisUtil.hdel(ParamUtil.ITEM_LOGIN, robotTag);
						Map<String, String> urlMap = (Map<String, String>) JSON.parse(urlJson);
						return CommonResult.ok(urlMap.get("url"));
					}
					Thread.sleep(2000);
				}
			}
			return CommonResult.build(-1, ParamUtil.NOT_FOUND_QRCODE);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(-1, ParamUtil.NOT_FOUND_QRCODE);
		}
	}
	
	@Override
	public Integer getMonitorByRoomID(String roomID) {
		return userMapper.getMonitorByRoomID(roomID);
	}
	
	@Override
	public String getMonitorNameByRoomID(String roomID) {
		return userMapper.getMonitorNameByRoomID(roomID);
	}

	@Override
	public CommonResult getSendRobotByRoomID(Integer mID, String roomID) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("mID", mID);
		param.put("roomID", roomID);
		return CommonResult.ok(robotMapper.getSendRobotByRoomID(param));
	}

	@Override
	public List<String> getChannelByMid(Integer mID) {
		String channalJson = RedisUtil.hget(RedisUtil.getRealIP() + "_" + ParamUtil.ITEM_SUBSCRIBE, String.valueOf(mID));
		if(channalJson == null){
			String channel = "";
			Integer channel_int = 0;
			Set<String> channels = new HashSet<String>();
			//监听班长频道
			List<MonitorRobotBindCustom> monitorRobots = robotMapper.getSendRobotByMid(mID);
			for (MonitorRobotBindCustom monitorRobot : monitorRobots) {
				channels.add(monitorRobot.getRobottag());
			}
			MonitorroomExample mExample = new MonitorroomExample();
			mExample.createCriteria()
				.andMonitoridEqualTo(mID);
			List<Monitorroom> rooms = monitorRoomMapper.selectByExample(mExample);
			for (Monitorroom room : rooms) {
				channel = robotMapper.getChannelByRoomID(room.getRoomid());
				if(channel == null)
					continue ;
				channel_int = Integer.parseInt(channel);
				if(channel_int % 2 == 0){
					channel = String.valueOf(channel_int - 1);
				}
				channels.add(channel);
			}
			//上传至redis  ITEM_SUBSCRIBE mID JSON(channels)
			if(channels.size() > 0){
				RedisUtil.hset(RedisUtil.getRealIP() + "_" + ParamUtil.ITEM_SUBSCRIBE, String.valueOf(mID), JSON.toJSONString(channels));
			}
			return new ArrayList<String>(channels);
		}else{
			return (List<String>) JSON.parse(channalJson);
		}
	}

	/*
	 * 将收到的roomIDs,以对应不同的机器人tag区分,
	 */
	private Map<String, Object> sendRobotClassify(Integer mID, List<String> roomIDs){
		Map<String, Object> tagMap = new HashMap<String, Object>();
		String tag = "";
		Monitorrobotbind bind = null;
		List<String> classifyRoomIDs = null;
		Map<String, Object> param = null;
		for (String roomID : roomIDs) {
			param = new HashMap<String, Object>();
			param.put("mID", mID);
			param.put("roomID", roomID);
			bind = robotMapper.getSendRobotByRoomID(param);
			if(bind == null)
				continue ;
			tag = bind.getRobottag();
			if(tagMap.containsKey(tag)){
				classifyRoomIDs = (List<String>) tagMap.get(tag);
				classifyRoomIDs.add(roomID);
				tagMap.put(tag, classifyRoomIDs);
			}else {
				classifyRoomIDs = new ArrayList<String>();
				classifyRoomIDs.add(roomID);
				tagMap.put(tag, classifyRoomIDs);
			}
		}
		return tagMap;
	}
	
	/**
	* @Title: 将拥有U_roomId群和老群roomID分开
	* @Description: 根据roomIds查找U_UserIds
	* @param @return    设定文件 
	* @return List<String>    返回类型 
	* @throws
	 */
	private Map<String,Object> getUUserIds(String[] roomIds,Integer mID){
		Map<String,Object> map = new HashMap<String,Object>();
		List<String> roomIdList = new ArrayList<String>();
		List<String> noURoomIds = new ArrayList<String>();//没有u_roomid的群
		if(null!=roomIds){
			roomIdList=Arrays.asList(roomIds);
		}
		List<String> uRoomIdList = new ArrayList<String>();
		Monitor monitor = monitorMapper.selectByPrimaryKey(mID);
		String roomId = "";
		for(int i=0; i<roomIdList.size(); i++){
			roomId = roomIdList.get(i);
			String u_roomId = roomMapper.findURoomIdByRoomId(roomId);
			if(null!=u_roomId){
				uRoomIdList.add(u_roomId);
			}else{
				uRoomIdList.add("");
				noURoomIds.add(roomId);
			}
		}
		map.put("roomIds", roomIdList);
		map.put("u_roomIds", uRoomIdList);
		map.put("monitor", monitor);
		map.put("noURoomIds", noURoomIds);
		return map;
	}
	
	private String sendText(Integer mID, String message,String atAll,String memberIds,String[] roomIds,Integer type) throws Exception{
		Microtaskimagetext mtText = null;
		if(type == 4){//表示消息类型是发送邀请卡片
			Integer taskId = null;
			if(null!=message){
				taskId = Integer.valueOf(message);
			}
			CommonResult commonResult = microTaskService.getImageTextByTaskID(taskId);
			try{
				mtText= (Microtaskimagetext) commonResult.getData();
			}catch(Exception e){
				mtText = null;
			}
		}
		Map<String,Object> map = getUUserIds(roomIds,mID);
		map.put("mtText", mtText);
		List<String> uRoomIdList = (List<String>) map.get("u_roomIds");
		String u_roomId = "";
		for(int i=0; i<uRoomIdList.size(); i++){
			u_roomId = uRoomIdList.get(i);
			if(("1".equals(atAll)&&null!=memberIds&&!"".equals(memberIds))||
					("0".equals(atAll))||(null!=u_roomId&&!"".equals(u_roomId))){//@群成员
				map.put("u_roomId", u_roomId);
				map.put("roomIdIndex", i);
				if(type==4){
					this.sendToRobot(3, message, "",atAll,memberIds,map);
				}else{
					this.sendToRobot(1, message, "",atAll,memberIds,map);
				}
			}
		}
		List<String> noURoomIds  = (List<String>) map.get("noURoomIds");
		if(null!=noURoomIds&&noURoomIds.size()>0){
			//发送机器人归类
			Map<String, Object> tagMap = this.sendRobotClassify(mID, noURoomIds);
			this.sendToRobot(tagMap, 1, message, "",map);
		}
		return message;
	}
	
	private String sendFile(Integer mID, MultipartFile file,String atAll,String memberIds,String[] roomIds) throws Exception {
		String ext = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") );
		String filename = mID + "_" + System.currentTimeMillis() + ext;
		String url = FileUtil.uploadFile(ParamUtil.MONITOR_SEND_DIR, filename, file.getBytes());
		Map<String,Object> map = getUUserIds(roomIds,mID);
		List<String> uRoomIdList = (List<String>) map.get("u_roomIds");
		String u_roomId = "";
		//发送
		for(int i=0; i<uRoomIdList.size(); i++){
			u_roomId = uRoomIdList.get(i);
			if(("1".equals(atAll)&&null!=memberIds&&!"".equals(memberIds))||
					("0".equals(atAll))||(null!=u_roomId&&!"".equals(u_roomId))){//@群成员
				map.put("u_roomId", u_roomId);
				map.put("roomIdIndex", i);
				this.sendToRobot(2, url, "",atAll,memberIds,map);
			}
		}
		List<String> noURoomIds  = (List<String>) map.get("noURoomIds");
		if(null!=noURoomIds&&noURoomIds.size()>0){
			//发送机器人归类
			Map<String, Object> tagMap = this.sendRobotClassify(mID, noURoomIds);
			this.sendToRobot(tagMap, 2, url, file.getOriginalFilename(),map);
		}
		return url;
	}
	
	private String sendFile(Integer mID, String url, String fileName,String atAll,String memberIds,String[] roomIds) throws Exception{
		Map<String,Object> map = getUUserIds(roomIds,mID);
		String u_roomId = "";
		List<String> uRoomIdList = (List<String>) map.get("u_roomIds");
		for(int i=0; i<uRoomIdList.size(); i++){
			u_roomId = uRoomIdList.get(i);
			if(("1".equals(atAll)&&null!=memberIds&&!"".equals(memberIds))||
					("0".equals(atAll)||(null!=u_roomId&&!"".equals(u_roomId)))){//@群成员
				map.put("u_roomId", u_roomId);
				map.put("roomIdIndex", i);
				this.sendToRobot(2, url, "",atAll,memberIds,map);
			}
		}
		List<String> noURoomIds  = (List<String>) map.get("noURoomIds");
		if(null!=noURoomIds&&noURoomIds.size()>0){
			//发送机器人归类
			Map<String, Object> tagMap = this.sendRobotClassify(mID, noURoomIds);
			this.sendToRobot(tagMap, 2, url, fileName,map);
		}
		return url;
	}
	
	private void sendToRobot(Map<String, Object> tagMap, Integer MsgType, String content, String fileName,Map<String,Object> map) throws Exception{
		Set<Entry<String, Object>> set = tagMap.entrySet();
		Microtaskimagetext mtText = (Microtaskimagetext) map.get("mtText");
		for (Entry<String, Object> entry : set) {
			Map<String, Object> sendMap = new HashMap<String, Object>();
			sendMap.put("MsgType", MsgType);
			Map<String, Object> Msg = new HashMap<String, Object>();
			Msg.put("Uin", robotMapper.getRobotIDByRobotTag(entry.getKey())); //122661399
			Msg.put("RoomIDs", (List<String>)entry.getValue());
			if(null!=mtText){
				Msg.put("Content", mtText.getImagetexturl());
			}else{
				Msg.put("Content", content); 
			}
			Msg.put("FileName", fileName);
			sendMap.put("Msg", Msg);
			logger.info("old sendMessage method----->Content:"+Msg.get("Content"));
			RedisUtil.publishMessage(entry.getKey() + "_", JSON.toJSONString(sendMap));
		}
	}
	
	/**
	 * 发送消息（@ 群成员）
	 * @param tagMap	消息发送机器人
	 * @param MsgType	消息类型
	 * @param content	内容
	 * @param fileName	文件名
	 * @param atAll	at成员
	 * @param memberIds 群成员ID
	 * @throws Exception
	 */
	private void sendToRobot(Integer MsgType, String content, String fileName,String atAll,
			String memberIds,Map<String,Object> map) throws Exception{
		Map<String, Object> sendMap = new HashMap<String, Object>();
		sendMap.put("MsgType", MsgType);
		Map<String, Object> Msg = new HashMap<String, Object>();
		Monitor monitor = (Monitor) map.get("monitor");
		List<String> roomIds = (List<String>) map.get("roomIds");
		int index = (Integer)map.get("roomIdIndex");//roomId对应U_roomId的索引
		String[] memIds = null;
		if(StringUtils.isNotEmpty(memberIds)){
			memIds = memberIds.split(",");
		}
		List<String> uUserIds = new ArrayList<String>();
		String roomId = roomIds.get(index);
		if(null!=memIds){
			for(String memberId:memIds){
				Map<String,String> param = new HashMap<String,String>();
				param.put("roomId", roomId);
				param.put("memberId", memberId);
				Wechatroommemberinfo roomMember = memberMapper.findUUserIdByMemberId(param);
				String u_userId = null;
				if(null!=roomMember){
					u_userId = roomMember.getU_userId();
				}
				if(null!=u_userId){
					uUserIds.add(u_userId);
				}
			}
		}
		Microtaskimagetext mtText = (Microtaskimagetext) map.get("mtText");//发送微任务邀请卡片
		Msg.put("u_roomId", map.get("u_roomId"));
		Msg.put("RoomID",roomId );
		if(null!=mtText){
			String title = mtText.getImagetexttitle();
			String remark = mtText.getImagetextremark();
			String url = mtText.getImagetexturl();
			Msg.put("Content", title+"#$#"+remark+"#$#"+url);
		}else{
			Msg.put("Content", content); 
		}
		Msg.put("FileName", fileName);
		Msg.put("UserNickName", monitor.getUsername());
		Msg.put("MonitorSend", "monitor");
		if(null == atAll){
			Msg.put("atAll", "1");
		}else{
			Msg.put("atAll", atAll);
		}
		Msg.put("memberIds", uUserIds);
		sendMap.put("Msg", Msg);
		logger.info("new sendMessage method "+"[u_roomId:"
				+map.get("u_roomId")+",monitorName"
				+monitor.getUsername()+",Content:"
				+Msg.get("Content")+"]");
		RedisUtil.publishMessage("20170630_", JSON.toJSONString(sendMap));
	}
	
	private String resendLastRecord(SendMessageBoundary sendMessage) throws Exception {
		switch (sendMessage.getResendType()) {
			case 1:
				//重新发送文本
				this.sendText(sendMessage.getmID(),  sendMessage.getMessage(),//
						sendMessage.getAtAll(),sendMessage.getMemberIds(),sendMessage.getRoomIDs(),sendMessage.getResendType());
				break ;
			case 2:
				//重新发送文件
				this.sendFile(sendMessage.getmID(), sendMessage.getMessage(), sendMessage.getFileName(),//
						sendMessage.getAtAll(),sendMessage.getMemberIds(),sendMessage.getRoomIDs());
				break ;
			default:
				break;
		}
		return sendMessage.getMessage();
	}
	
	@Override
	public String[] getInitChannel() {
		List<String> all_channels = robotMapper.getAllChannels();
		Set<String> initChannels = new HashSet<String>();
		int channel_int = 0;
		for (String channel : all_channels) {
			channel_int = Integer.parseInt(channel);
			if(channel_int % 2 == 0){
				channel = String.valueOf(channel_int - 1);
			}
			initChannels.add(channel);
		}
		initChannels.add("p20170630_");//监听@群成员失败信息
		initChannels.add("p20170701_");//页面刷新消息
		//添加记录频道  为本服务器公网ip
		initChannels.add(RedisUtil.getRealIP());
		return initChannels.toArray(new String[]{});
	}

	@Override
	public CommonResult addSendRobot(String monitorName, Integer mID,HttpServletRequest request) {
		try {
			String nextTag = RedisUtil.get(ParamUtil.ADD_ROBOT_TAG);
			if(nextTag == null){
				nextTag = robotMapper.getMaxTag();
				if(null==nextTag){
					nextTag = "0";
				}
				RedisUtil.set(ParamUtil.ADD_ROBOT_TAG, nextTag);
			}
			nextTag = RedisUtil.incr(ParamUtil.ADD_ROBOT_TAG) + "";
			
			//插入机器人规则
			robotMapper.insertSendRobotRole(nextTag);
			
			String url = propertyConfigurer.getProperty("START_ROBOT_URL").replace("BOT", nextTag).replace("NEW", "true");
			//获取请求Ip
			String ip = getIpAddress(request);
			url = url+"&fip="+ip;
			logger.info("-------------------->url"+url);
			String responseJson = HttpClientUtil.doGet(url);
			Map<String, String> responseMap = (Map<String, String>) JSON.parse(responseJson);
			//请求发送成功之后查询redis
			if(ParamUtil.START_ROBOT_SUCCESS.equals(responseMap.get("code"))){
				for (int i = 0; i < 3; i++) {
					String urlJson = RedisUtil.hget(ParamUtil.ITEM_LOGIN, nextTag);
					if(urlJson != null){
						Map<String, String> urlMap = (Map<String, String>) JSON.parse(urlJson);
						//删除redis记录
						RedisUtil.hdel(ParamUtil.ITEM_LOGIN, nextTag);
						//插入用户机器人关联记录
						Map<String, Object> param = new HashMap<String, Object>();
						param.put("mID", mID);
						param.put("robotTag", nextTag);
						param.put("monitorName", monitorName);
						param.put("created", TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
						userMapper.insertSendRobot(param);
						
						return CommonResult.ok(urlMap.get("url"));
					}
					Thread.sleep(2000);
				}
			}
			return CommonResult.ok(responseMap);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(-1, "服务器错误");
		}
		
	}

	public static String getIpAddress(HttpServletRequest request) {  
        String ip = request.getHeader("x-forwarded-for");  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
        }  
        return ip;  
    } 
	
	@Override
	public CommonResult getMicroTask(Integer mID, String roomID, Integer belong) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("mID", mID);
		param.put("belong", belong);
		param.put("timestamp", TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
		if(!"".equals(roomID) && roomID != null){
			param.put("roomID", roomID);
		}
		List<Map<String, Object>> response = new ArrayList<Map<String,Object>>();
		List<MicroTaskCustom> tasks = userMapper.getMicroTask(param);
		Map<String, Object> each = null;
		Map<String, Object> taskRoomInfo_param = null;
		
		for (MicroTaskCustom task : tasks) {
			each = new HashMap<String, Object>();
			taskRoomInfo_param = new HashMap<String, Object>();
			taskRoomInfo_param.put("mID", mID);
			taskRoomInfo_param.put("taskID", task.getId());
			each.put("main", task);
			each.put("rooms", userMapper.getRoomInfoByTaskID(taskRoomInfo_param));
			response.add(each);
		}
		return CommonResult.ok(response);
	}

	public CommonResult updateRoomOwner(MultipartFile file) {
		Map<String,Object> resultMap = new HashMap<String,Object>();
		List<Monitor> monitorList = null;
		Set<String> searchmSet = new HashSet<String>();
		List<Wechatroominfo> roomList = null;
		List<String> notExistsmList = new ArrayList<String>();//不存在的班长列表
		List<ExcelTemplate> notExistsRoomList = new ArrayList<ExcelTemplate>();//不存在的群列表
		List<ExcelTemplate> updateDataList = new ArrayList<ExcelTemplate>();//更新群关系数据列表
		List<ExcelTemplate> insertDataList = new ArrayList<ExcelTemplate>();//新增的群关系数据列表
		//获取excel中的数据
		List<ExcelTemplate> list = null;
		try {
			//list = FileUtil.uploadExcel(file);
			Map<String,String> map = getExcelTemplateRelationShipMap();
			ImportExcel importExcel = new ImportExcel(map, file.getInputStream());
			list = importExcel.getExcelDataList(ExcelTemplate.class);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(-1, "读取文件失败！");
		}
		if(null == list || list.size() == 0){
			resultMap.put("result", "excel文件中数据为空！");
		}
		
		int size = list.size();
		
		ExcelTemplate eTemplate = null;
		for(int i=0; i<size; i++){
			eTemplate = list.get(i);
			String username = null;
			if(null!=eTemplate){
				username = eTemplate.getUsername();
				//查询是否有不存在的群
				roomList = roomMapper.getRoomByExcelTemplate(eTemplate);
				if(null == roomList || roomList.size() == 0 || roomList.size()>1){//群数据为空，或数据异常
					notExistsRoomList.add(eTemplate);
					//群不存在则去除当前数据
					list.remove(i);
					size--;
				}
			}
			if(null!=username&&!"".equals(username)){//减少数据库查询
				searchmSet.add(username);
			}
		}
		//查询是否有不存在的班长
		MonitorExample monitorExample = new MonitorExample();
		if(searchmSet.size()>0){
			Iterator<String> it = searchmSet.iterator();
			while(it.hasNext()){
				String username = it.next();
				monitorExample.createCriteria()//
				.andUsernameEqualTo(username.trim());
				monitorList = monitorMapper.selectByExample(monitorExample);
				if(null == monitorList || monitorList.size() == 0){
					notExistsmList.add(username);
				}
			}
		}
		
		//Integer monitorTag = monitorMapper.findMonitorMaxTag();
		//插入班长数据
		Monitor record = null;
		System.out.println("===============班长数据插入begin======================");
		if(notExistsmList.size()>0){
			for(int i=0; i<notExistsmList.size();i++){
				record = getMonitor(notExistsmList.get(i));
				monitorMapper.insertSelective(record);
				System.out.println("----->插入第"+i+"个班长:"+record.getUsername());
			}
		}
		System.out.println("插入班长数据总数为："+notExistsmList.size());
		System.out.println("===============班长数据插入end======================");
		
		
		System.out.println("===============更新群归属数据begin==================");
		//更新群归属关系
		for(int i=0; i<list.size(); i++){
			eTemplate = list.get(i);
			Monitorroom monitorroom = null;
			if(null!=eTemplate){
				//根据群ID判断群关系数据是否存在
				List<Monitorroom> monitorRooms=monitorRoomMapper.findMonitorRoomByExcelTemplate(eTemplate);
				
				MonitorExample mExample = new MonitorExample();
				mExample.createCriteria().andUsernameEqualTo(eTemplate.getUsername());
				List<Monitor> monitors = monitorMapper.selectByExample(mExample);
				
				if(null!=monitorRooms&&monitorRooms.size()>0){//存在更新班长ID
					monitorRoomMapper.updateMonitorRoomByExcelTemplate(eTemplate);
					eTemplate.setOldMonitorId(monitorRooms.get(0).getMonitorid());
					eTemplate.setMonitorId(monitors.get(0).getId()+"");
					updateDataList.add(eTemplate);
					System.out.println("-------->当前第"+i+"条，更新群:"+eTemplate.getRoomName()+",班长Id由："+eTemplate.getOldMonitorId()+"变为"
							+eTemplate.getMonitorId());
				}else{//不存在插入群归属数据
					monitorroom = getMonitorroom(eTemplate,monitors.get(0));
					monitorRoomMapper.insertSelective(monitorroom);
					insertDataList.add(eTemplate);
					System.out.println("------>插入新的群归属数据群ID为"+monitorroom.getRoomid()+",班长Id为："+monitorroom.getMonitorid());
				}
			}
		}
		System.out.println("===============更新群归属数据总条数================");
		System.out.println("===============更新群归属数据end==================");
		
		//resultMap.put("result", "更新群归属关系成功！");
		resultMap.put("不存在的群，或数据异常的群", notExistsRoomList);
		resultMap.put("不存在的群，或数据异常的群数", notExistsRoomList.size());
		resultMap.put("新增的班长", notExistsmList);
		resultMap.put("新增的班长数", notExistsmList.size());
		resultMap.put("更新班长群关系数据", updateDataList);
		resultMap.put("更新班长群关系条数", updateDataList.size());
		resultMap.put("新增班长群关系数据", insertDataList);
		resultMap.put("新增班长群关系数据条数", insertDataList.size());
		return CommonResult.ok(resultMap);
	}

	private Map<String, String> getExcelTemplateRelationShipMap() {
		Map<String, String> map = new HashMap<String, String>();
		//map.put("群ID", "roomId");
		map.put("群名", "roomName");
		map.put("班长账号", "username");
		//map.put("city", "city");
		//map.put("memberNums", "memberNums");
		return map;
	}

	private Monitorroom getMonitorroom(ExcelTemplate eTemplate,Monitor monitor) {
		String roomId = eTemplate.getRoomId();//获取模版里面的roomId
		if(roomId == null || "".equals(roomId)){//如果模版roomId没有，就根据群名
			roomId =	roomMapper.findRoomByRoomName(eTemplate.getRoomName());
		}
		Monitorroom monitorroom = new Monitorroom();
		monitorroom.setRoomid(roomId);
		monitorroom.setMonitorid(monitor.getId());
		monitorroom.setFlag(1);
		return monitorroom;
	}

	private Monitor getMonitor(String username) {
		Monitor monitor = new Monitor();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		monitor.setUsername(username);
		monitor.setPassword(MD5Utils.md5Encode(DEFAULT_PASSWORD));
		monitor.setCreatetime(df.format(new Date()));
		monitor.setBelong(2);
		monitor.setCustomerType("ayd");//当前班长属于爱婴岛
		return monitor;
	}

	@Override
	public GeneralContentResult<Map<String, Object>> importMonitorRoom(List<ImportedMonitorRoomItem> importedMonitorRoomItems,Integer belong) {
		/*
		 * 1,通过群相关属性，确定群
		 * 2,通过班长相关属性，确定班长
		 * 3，通过群和班长确定monitorroom表中的记录，更新或修改
		 * */
		GeneralContentResult<Map<String, Object>> result = new GeneralContentResult<Map<String, Object>>();
		Map<String, Object> data = new HashMap<String,Object>();
		List<ImportedMonitorRoomItem> errorRoomList = new ArrayList<ImportedMonitorRoomItem>();
		List<ImportedMonitorRoomItem> errorMonitorList = new ArrayList<ImportedMonitorRoomItem>();
		List<Monitor> newMonitorList = new ArrayList<Monitor>();
		Integer successUpdate = 0;
		Integer successAdd = 0;
		Integer error = 0;

		Map<String,Monitor> monitorCatch = new HashMap<String,Monitor>();
		Integer newmonitorCount = 1;
		for (ImportedMonitorRoomItem importedMonitorRoomItem : importedMonitorRoomItems) {
			//获取群
			List<Wechatroominfo> wechatroominfoList = this.getRoomInfos(importedMonitorRoomItem);
			if(wechatroominfoList == null || wechatroominfoList.size() == 0 || wechatroominfoList.size() > 1){
				errorRoomList.add(importedMonitorRoomItem);	
				continue;
			}
			Wechatroominfo wechatroominfo = wechatroominfoList.get(0);
			//获取班长，缓存
			String catchKey = importedMonitorRoomItem.getMonitorId()+importedMonitorRoomItem.getName()+importedMonitorRoomItem.getUserName();
			Monitor monitor = monitorCatch.get(catchKey);
			if(monitor == null){
				List<Monitor> getMonitors = this.getMonitors(importedMonitorRoomItem,belong);
				if(getMonitors == null || getMonitors.size() == 0 ){
					monitor = new Monitor();
					DateFormat df1 = new SimpleDateFormat("yyMMddHHmm");
					String userName = "monitor"+df1.format(new Date());
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					monitor.setUsername(userName+"_"+newmonitorCount);
					monitor.setPassword(MD5Utils.md5Encode(DEFAULT_PASSWORD));
					monitor.setCreatetime(df.format(new Date()));
					monitor.setBelong(belong);
					monitor.setName(importedMonitorRoomItem.getName());
					monitorMapper.insertSelective(monitor);
					newMonitorList.add(monitor);
					newmonitorCount++;
				}else{
					if(getMonitors.size() > 1){
						errorMonitorList.add(importedMonitorRoomItem);	
						continue;
					}
					monitor = getMonitors.get(0);
				}
				monitorCatch.put(catchKey, monitor);
			}
			//关联关系变更
			if(monitor != null && wechatroominfo != null){
				List<Monitorroom>  monitorrooms = monitorRoomMapper.selectByRoomId(wechatroominfo.getRoomid());
				if(monitorrooms == null || monitorrooms.size() == 0){
					Monitorroom monitorroom = new Monitorroom();
					monitorroom.setRoomid(wechatroominfo.getRoomid());
					monitorroom.setMonitorid(monitor.getId());
					monitorRoomMapper.insertSelective(monitorroom);
					successAdd++;
				}else{
					Monitorroom monitorroom = monitorrooms.get(0);
					monitorroom.setMonitorid(monitor.getId());
					monitorRoomMapper.updateByPrimaryKey(monitorroom);
					successUpdate++;
				}
			}else{
				error++;
			}
		}
		result.setResultCode(ResultCode.OPERATION_SUCCESS);
		data.put("errorRoomList", errorRoomList);
		data.put("errorMonitorList", errorMonitorList);
		data.put("newMonitorList", newMonitorList);
		data.put("successUpdate", successUpdate);
		data.put("successAdd", successAdd);
		data.put("error", error);
		result.setResultContent(data);
		return result;
	}
	
	private List<Monitor> getMonitors(ImportedMonitorRoomItem importedMonitorRoomItems,Integer belong){
		Map<String,String> reqMap = new HashMap<String,String>();
		reqMap.put("id", importedMonitorRoomItems.getMonitorId());
		reqMap.put("UserName", importedMonitorRoomItems.getUserName());
		reqMap.put("Name", importedMonitorRoomItems.getName());
		reqMap.put("Belong",belong.toString());
		return monitorMapper.selectMonitor(reqMap);
	}
	
	private List<Wechatroominfo> getRoomInfos(ImportedMonitorRoomItem importedMonitorRoomItems){
		Map<String,String> reqMap = new HashMap<String,String>();
		reqMap.put("RoomID", importedMonitorRoomItems.getRoomId());
		reqMap.put("InnerID", importedMonitorRoomItems.getInnerId());
		reqMap.put("RoomName", importedMonitorRoomItems.getRoomName());
		return wechatroominfoMapper.selectRoomInfo(reqMap);
	}

	@Override
	public GeneralPagingResult<MonitorRoomInfo> searchMonitorRoom(MonitorRoomInfo monitorRoomInfo, Integer page,Integer size) {
		GeneralPagingResult<MonitorRoomInfo> result = new GeneralPagingResult<MonitorRoomInfo>();
		PageHelper.startPage(page, size);
		Page<MonitorRoomInfo> pageData = monitorMapper.searchMonitorRoom(monitorRoomInfo);
		PageInfo<MonitorRoomInfo> pageInfo = pageData.toPageInfo();
		result.setResultCode(ResultCode.OPERATION_SUCCESS);
		result.setDetailDescription("操作成功");
		result.setPageInfo(pageInfo);
		return result;
	}

	@Override
	public void deleteMonitorRoom(String monitorRoomId) {
		monitorMapper.deleteMonitorRoom(monitorRoomId);
	}
	
}
