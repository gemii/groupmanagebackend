package cc.gemii.service.impl;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import sun.misc.BASE64Decoder;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.UploadService;
import cc.gemii.util.FileUtil;
import cc.gemii.util.ParamUtil;
import cc.gemii.util.UUIDUtils;

@Service
public class UploadServiceImpl implements UploadService{

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Override
	public CommonResult uploadBase64(String base64String) {
		try {
			String ext = base64String.substring(base64String.indexOf("/") + 1, base64String.indexOf(";"));
			byte[] bytes = new BASE64Decoder().decodeBuffer(base64String.substring(base64String.indexOf(",") + 1));
			return CommonResult.ok(FileUtil.uploadFile(ParamUtil.MICRO_TASK_UPLOAD_DIR, UUIDUtils.getRandomCapital(16) + "." + ext, bytes));
		} catch (StringIndexOutOfBoundsException e){
			logger.error(e.getMessage(),e);
			return CommonResult.build(-1, "base64格式错误");
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "服务器错误");
		}
	}

	@Override
	public CommonResult uploadFile(String dir, MultipartFile file) {
		try {
			String fileName = file.getOriginalFilename();
			String ext = fileName.substring(fileName.lastIndexOf("."));
			byte[] bytes = file.getBytes();
			return CommonResult.ok(FileUtil.uploadFile(dir, UUIDUtils.getRandomCapital(16) + "." + ext, bytes));
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "服务器错误");
		}
	}

	public CommonResult uploadMicroTaskFile(MultipartFile file) {
		try {
			String ext = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") );
			String fileName = System.currentTimeMillis()+ext ;
			return CommonResult.ok(FileUtil.uploadFile(ParamUtil.MICRO_TASK_UPLOAD_DIR, fileName, file.getBytes()));
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "服务器错误");
		}
	}
}
