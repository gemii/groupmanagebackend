package cc.gemii.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.component.sensitive.SensitivewordUtil;
import cc.gemii.mapper.RoomkeywordMapper;
import cc.gemii.mapper.custom.KeywordMapper;
import cc.gemii.mapper.custom.RoomMapper;
import cc.gemii.po.Monitor;
import cc.gemii.po.Roomkeyword;
import cc.gemii.po.RoomkeywordExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.KeywordService;
import cc.gemii.service.MonitorService;
@Service
public class KeywordServiceImpl implements KeywordService {

	@Autowired
	private RoomkeywordMapper roomKeywordMapper;
	
	@Autowired
	private RoomMapper roomMapper;
	
	@Autowired
	private KeywordMapper keywordMapper;
	
	@Autowired
	private MonitorService monitorService;
	
	@Override
	public CommonResult getGlobalKeyword(Integer belong) {
		RoomkeywordExample example = new RoomkeywordExample();
		example.createCriteria()
			.andTypeEqualTo(1)
			.andBelongEqualTo(belong)
			.andStatusEqualTo("开启");
		return CommonResult.ok(roomKeywordMapper.selectByExample(example));
	}

	@Override
	public CommonResult getKeywordByMid(String mID) {
		return CommonResult.ok(roomMapper.getKeywordByMid(mID));
	}

	@Override
	public CommonResult deleteKeyword(String mID, String keyword) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("mID", mID);
		map.put("keyword", keyword);
		keywordMapper.deleteKeyword(map);
		List<String> roomids = keywordMapper.selectRoomidsByKeyWord(map);
		initAddKeyWord(roomids);
		return CommonResult.ok();
	}

	@Override
	public CommonResult insertKeyword(String mID,String[] roomIDs, String keyword,String status) {
		Map<String, String> param = new HashMap<String, String>();
		param.put("mID", mID);
		param.put("keyword", keyword);
		List<Roomkeyword> roomkeywords = keywordMapper.isExists(param);
		if(roomkeywords.size() > 0){
			return CommonResult.build(-1, "关键字已存在");
		}
		String belong=keywordMapper.selectBelong(mID);
		if(roomIDs.length > 0){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("roomIDs", roomIDs);
			map.put("keyword", keyword);
			map.put("status", status);
			map.put("belong", belong);
			keywordMapper.insertKeyword(map);
			List<String> roomids = Arrays.asList(roomIDs);
			initAddKeyWord(roomids);
		}
		return CommonResult.ok();
	}

	@Override
	public CommonResult modifyStatus(String keyword, String mID) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("keyword", keyword);
		map.put("mID", mID);
		keywordMapper.modifyStatus(map);
		List<String> roomids = keywordMapper.selectRoomidsByKeyWord(map);
		//初始化关键字
		initAddKeyWord(roomids);
		return CommonResult.ok();
	}

	@Override
	public CommonResult updateKeyword(String mID, String keyword,
			String[] roomIDs) {
		RoomkeywordExample example = new RoomkeywordExample();
		example.createCriteria()
			.andKeywordEqualTo(keyword);
		List<Roomkeyword> keywords = roomKeywordMapper.selectByExample(example);
		this.deleteKeyword(mID, keyword);
		if(keywords.get(0).getStatus().equals("开启")){
			this.insertKeyword(mID,roomIDs, keyword,"开启");
		}else{
			this.insertKeyword(mID,roomIDs, keyword,"关闭");
		}
		return CommonResult.ok();
	}

	@Override
	public List<String> getKeywordByRoomID(String roomID) {
		return keywordMapper.getKeywordByRidG(roomID);
	}

	public CommonResult initKeyWordMap(String owner) {
		String mapKey = owner + "_keyword";
		String mapDelKey = owner + "_delkeyword";
		SensitivewordUtil.moveKeyWordMap(mapKey);
		SensitivewordUtil.moveKeyWordMap(mapDelKey);
		return CommonResult.ok();
	}

	@Override
	public List<String> getKeyWordByMap(Map<String, String> param) {
		return keywordMapper.getKeyWordByMap(param);
	}

	/**
	 * @param roomids 
	 * 
	* @Title: initAddKeyWord 
	* @Description: TODO 初始化新增， 更改状态的关键字
	* @param     设定文件 
	* @return void    返回类型 
	* @throws
	 */
	private void initAddKeyWord(List<String> roomids){
		for (String roomid : roomids) {
			String key = roomid + "_customkeyword";
			SensitivewordUtil.moveKeyWordMap(key);
		}
	}

	@Override
	public List<String> selectKeyWordByKey(String belong) {
		return keywordMapper.selectKeyWordByKey(belong);
	}

	@Override
	public List<String> selectCustomKeyWordByKey(String roomid) {
		return keywordMapper.selectCustomKeyWordByRoomid(roomid);
	}
	
	@Override
	public List<String> selectDelKeyWordByKey(String belong) {
		return keywordMapper.selectDelKeyWordByKey(belong);
	}
}
