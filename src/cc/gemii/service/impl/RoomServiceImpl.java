package cc.gemii.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.MonitorroomMapper;
import cc.gemii.mapper.RoomkeywordMapper;
import cc.gemii.mapper.RoomtagbindMapper;
import cc.gemii.mapper.WechatroommemberinfoMapper;
import cc.gemii.mapper.custom.KeywordMapper;
import cc.gemii.mapper.custom.RoomMapper;
import cc.gemii.mapper.custom.TagMapper;
import cc.gemii.po.Monitorroom;
import cc.gemii.po.MonitorroomExample;
import cc.gemii.po.RoomtagbindExample;
import cc.gemii.po.WechatroommemberinfoExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;
import cc.gemii.pojo.RoomInfo;
import cc.gemii.service.RoomService;
import cc.gemii.util.TimeUtils;
@Service
public class RoomServiceImpl implements RoomService {

	@Autowired
	private RoomMapper roomMapper;
	
	@Autowired
	private WechatroommemberinfoMapper memberMapper;
	
	@Autowired
	private RoomkeywordMapper roomKeywordMapper;
	
	@Autowired
	private KeywordMapper keywordMapper;
	
	@Autowired
	private TagMapper tagMapper;
	
	@Autowired
	private RoomtagbindMapper roomTagMapper;
	
	@Autowired
	private MonitorroomMapper monitorRoomMapper;
	
	@Override
	public CommonResult getRoomByMid(Integer id) {
		Map<String, Object> param = new HashMap<String, Object>();
		//显示前一天零点之后的未读消息数
		String timestamp_high = TimeUtils.getTime("yyyy-MM-dd HH:mm:ss");
		String timestamp_low = TimeUtils.getXDayStart(-1);
		param.put("timestamp_low", timestamp_low);
		param.put("timestamp_high", timestamp_high);
		param.put("mID", id);
		List<RoomInfo> rooms = roomMapper.getRoomByMid(param);
		rooms.addAll(roomMapper.getKeywordMessageUnread(param));
		rooms.addAll(roomMapper.getAdminMessageUnread(param));
		return CommonResult.ok(rooms);
	}

	@Override
	public CommonResult getMemberByRid(String roomID) {
		WechatroommemberinfoExample example = new WechatroommemberinfoExample();
		example.createCriteria()
			.andRoomidEqualTo(roomID)
			.andLeaveGroupTimeIsNull();
		return CommonResult.ok(memberMapper.selectByExample(example));
	}

	@Override
	public CommonResult getRoomByKeyword(Integer mID, String keyword) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("mID", mID);
		map.put("keyword", keyword);
		
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("rooms", roomMapper.getRoomByKeyword(map));
		response.put("keyword", keywordMapper.getKeyword(map));
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult getRoomKeyword(Integer mID) {
		MonitorroomExample mExample = new MonitorroomExample();
		mExample.createCriteria()
			.andMonitoridEqualTo(mID);
		List<Monitorroom> roomIDs = monitorRoomMapper.selectByExample(mExample);
		List<Map<String, Object>> response = new ArrayList<Map<String,Object>>();
		Map<String, Object> map = null;
		for (Monitorroom room : roomIDs) {
			map = new HashMap<String, Object>();
			map.put("roomID", room.getRoomid());
			map.put("keywords", keywordMapper.getKeywordByRoomID(room.getRoomid()));
            map.put("tags", tagMapper.getRoomTagByRoomID(room.getRoomid()));
			response.add(map);
		}
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult insertRoomTag(String roomID, String[] tags) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("roomID", roomID);
		param.put("tags", tags);
		tagMapper.insertRoomTag(param);
		return CommonResult.ok();
	}

	@Override
	public CommonResult deleteRoomTag(String roomID, String tag) {
		RoomtagbindExample example = new RoomtagbindExample();
		example.createCriteria()
			.andRoomidEqualTo(roomID)
			.andTagnameEqualTo(tag)
			.andTypeEqualTo(2);
		roomTagMapper.deleteByExample(example);
		return CommonResult.ok();
	}

	@Override
	public CommonResult getRoomTagByRoomID(String roomID) {
		return CommonResult.ok(tagMapper.getRoomTagByRoomID(roomID));
	}

	@Override
	public CommonResult markRoom(Integer mID, String roomID, String action) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("mID", mID);
		param.put("roomID", roomID);
		param.put("action", action);
		roomMapper.markRoom(param);
		return CommonResult.ok();
	}

	@Override
	public CommonResult getManagedRoom(Integer mID) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("mID", mID);
		return CommonResult.ok(roomMapper.getManagedRoom(param));
	}

	@Override
	public CommonResult getCSRRoom(Pagination page,String belong) {
		Map<String, Object> param = null;
		String owner = null;
		switch (belong) {
		case "4":
			owner = "aiyingdao";
			break;
		case "5":
			owner = "meisujiaer";
			break;
		default:
			owner = null;
			break;
		}
		if(null!=page.getPage()&&null!=page.getPageSize()){
			param = new HashMap<String,Object>();
			param.put("start", (page.getPage() - 1) * page.getPageSize());
			param.put("end", page.getPageSize());
			param.put("owner", owner);
			return CommonResult.ok(roomMapper.getCSRRoomByPage(param));
		}
		return CommonResult.ok(roomMapper.getCSRRoom(owner));
	}

	@Override
	public CommonResult validateRoomIDs(String roomIDs) {
		List<String> roomIDList = new ArrayList<String>(Arrays.asList(roomIDs.split(",")));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("roomIDs", roomIDList);
		List<String> existsRoomIDs = roomMapper.validateRoomIDs(param);
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("total", roomIDList.size());
		response.put("success", existsRoomIDs.size());
		roomIDList.removeAll(existsRoomIDs);
		response.put("failed", roomIDList.size());
		response.put("failedRooms", roomIDList);
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult lowestLiveness(Integer mID, String percent) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("mID", mID);
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DAY_OF_MONTH, -1);
		param.put("timestamp", TimeUtils.getTime(c.getTime(), "yyyyMMdd"));
		param.put("percent", percent);
		return CommonResult.ok(roomMapper.lowestLiveness(param));
	}

	@Override
	public CommonResult livenessList(Integer mID) {
		String pattern = "yyyyMMdd";
		String lastSunday = TimeUtils.getCountWeekSunday(-1, pattern);
		String last2Sunday = TimeUtils.getCountWeekSunday(-2, pattern);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("last_sunday", lastSunday);
		param.put("last2_sunday", last2Sunday);
		param.put("mID", mID);
		return CommonResult.ok(roomMapper.livenessList(param));
	}
	
	@Override
	public RoomInfo getRoomInfoByRoomId(String roomId){
		return roomMapper.selectWechatRoomByRoomId(roomId);
	}

	@Override
	public CommonResult getMemberByMap(Map<String, Object> map) {
		String roomID = (String) map.get("roomId");
		String nickName = (String) map.get("userName");
		WechatroommemberinfoExample example = new WechatroommemberinfoExample();
		example.createCriteria()
			.andRoomidEqualTo(roomID)
			.andNicknameEqualTo(nickName)
			.andLeaveGroupTimeIsNull();
		return CommonResult.ok(memberMapper.selectByExample(example));
	}

	@Override
	public List<RoomInfo> findRoomStatisticsInfo() {
		List<RoomInfo> roomList = roomMapper.findRoomStatisticsInfo();
		return roomList;
	}

	@Override
	public String findURoomIdByRoomId(String roomID) {
		return roomMapper.findURoomIdByRoomId(roomID);
	}

	@Override
	public String selectOwnerByRoomId(String roomID) {
		return roomMapper.selectOwnerByRoomId(roomID);
	}
}
