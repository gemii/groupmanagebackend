package cc.gemii.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cc.gemii.mapper.MicrotaskMapper;
import cc.gemii.mapper.MicrotaskimagetextMapper;
import cc.gemii.mapper.MicrotaskroomidMapper;
import cc.gemii.mapper.custom.MicroTaskMapper;
import cc.gemii.po.Microtask;
import cc.gemii.po.MicrotaskWithBLOBs;
import cc.gemii.po.Microtaskimagetext;
import cc.gemii.po.MicrotaskimagetextExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.MicroTaskCustom;
import cc.gemii.pojo.Pagination;
import cc.gemii.pojo.TaskUser;
import cc.gemii.service.MicroTaskService;
import cc.gemii.util.ComputePages;
import cc.gemii.util.MD5Utils;
import cc.gemii.util.TimeUtils;

@Service
public class MicroTaskServiceImpl implements MicroTaskService{

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private MicrotaskMapper microTaskMapper;
	
	@Autowired
	private MicrotaskimagetextMapper imageTextMapper;
	
	@Autowired
	private MicrotaskroomidMapper roomIDsMapper;
	
	@Autowired
	private MicroTaskMapper microTaskCustomMapper;
	
	@Override
	public CommonResult login(String userName, String password, Integer type) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userName", userName);
		param.put("password", MD5Utils.md5Encode(password));
		param.put("type", type);
		TaskUser user = microTaskCustomMapper.login(param);
		if(user == null){
			return CommonResult.build(-1, "账户名或密码错误");
		}
		return CommonResult.ok(user);
	}
	
	@Override
	public CommonResult createOrUpdated(MicrotaskWithBLOBs microTask, Microtaskimagetext imageText, String[] roomIDs, Integer isImageText) {
		if(microTask.getId() == null){  //创建
			return this.createTask(microTask, imageText, roomIDs, isImageText);
		}else{                          //更新
			return this.updateTask(microTask, imageText, roomIDs, isImageText);
		}
	}

	private CommonResult createTask(MicrotaskWithBLOBs microTask, Microtaskimagetext imageText, String[] roomIDs, Integer isImageText){
		try {
			//创建微任务
			microTask.setCreatetime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
			microTask.setLastupdatetime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
			//创建微任务-默认未删除
			microTask.setIsdelete(0);
			//创建微任务-默认启用
			microTask.setEnable(1);
			microTaskMapper.insert(microTask);
			
			//创建图文信息
			if(1 == isImageText){
				imageText.setMicrotaskid(microTask.getId());
				imageTextMapper.insert(imageText);
			}
			
			//创建微任务对应群
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("taskID", microTask.getId());
			param.put("roomIDs", roomIDs);
			microTaskCustomMapper.insertMicroTaskRoomID(param);
			
			return CommonResult.ok();
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "服务器错误");
		}
	}
	
	private CommonResult updateTask(MicrotaskWithBLOBs microTask, Microtaskimagetext imageText, String[] roomIDs, Integer isImageText){
		try {
			//更新微任务
			microTask.setLastupdatetime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
			microTaskMapper.updateByPrimaryKeySelective(microTask);

			if(1 == isImageText){
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("microTaskID", microTask.getId());
				param.put("imageText", imageText);
				microTaskCustomMapper.updateImageText(param);
			}
			
			//删除原来对应群
			microTaskCustomMapper.deleteMicroTaskRoomID(microTask.getId());
			//创建微任务对应群
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("taskID", microTask.getId());
			param.put("roomIDs", roomIDs);
			microTaskCustomMapper.insertMicroTaskRoomID(param);
			return CommonResult.ok();
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "服务器错误");
		}
	}
	
	@Override
	public CommonResult getCityByArea(String area) {
		return CommonResult.ok(microTaskCustomMapper.getCityByArea(area));
	}

	@Override
	public CommonResult getMicroTaskList(String search, Integer belong, Pagination page) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("search", "%%");
		param.put("belong", belong);
		param.put("start", (page.getPage() - 1) * page.getPageSize());
		param.put("end", page.getPageSize());
		logger.info(search);
		if("启用".equals(search)){
			param.put("enable", 1);
		}else if("停用".equals(search)){
			param.put("enable", 0);
		}else if("过期".equals(search)){
			param.put("enable", 3);
		}else{
			param.put("search", "%"+ search +"%");
		}
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("main", microTaskCustomMapper.getMicroTaskList(param));
		response.put("totalPage", ComputePages.computePages(microTaskCustomMapper.getMicroTaskListCount(param), page.getPageSize()));
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult delete(Integer taskID) {
		microTaskCustomMapper.delete(taskID);
		return CommonResult.ok();
	}

	@Override
	public CommonResult getDetailByID(Integer taskID) {
		MicroTaskCustom taskCustom = microTaskCustomMapper.getDetailByID(taskID);
		List<String> roomIDs = microTaskCustomMapper.getRoomIDsByTaskID(taskID);
		taskCustom.setRoomIDs(StringUtils.join(roomIDs, ","));
		return CommonResult.ok(taskCustom);
	}

	@Override
	public CommonResult updateTaskEnable(Integer taskID) {
		microTaskCustomMapper.updateTaskEnable(taskID);
		return CommonResult.ok();
	}

	@Override
	public void noticeMonitor() {
		
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public CommonResult cpMicroTask(Integer taskID) {
		//copy 任务
		Integer newTaskID = cpMicroTaskDetail(taskID);
		//copy 图文
		cpImageText(taskID, newTaskID);
		//copy 覆盖群
		cpMicroTaskRoomIDs(taskID, newTaskID);
		return CommonResult.ok();
	}

	private Integer cpMicroTaskDetail(Integer taskID){
		Microtask cpTask = microTaskMapper.selectByPrimaryKey(taskID);
		String nowTimeString = TimeUtils.getTime("yyyy-MM-dd HH:mm:ss");
		Microtask newTask = new Microtask();
		newTask.setActivitycontent(cpTask.getActivitycontent());
		newTask.setActivityimage(cpTask.getActivityimage());
		newTask.setActivitytitle(cpTask.getActivitytitle());
		newTask.setCreatetime(nowTimeString);
		newTask.setEnable(cpTask.getEnable());
		newTask.setIsdelete(cpTask.getIsdelete());
		newTask.setLastupdatetime(nowTimeString);
		newTask.setPushend(cpTask.getPushend());
		newTask.setPushstart(cpTask.getPushstart());
		newTask.setPushtime(cpTask.getPushtime());
		newTask.setBelong(cpTask.getBelong());
		if(3 == cpTask.getEnable()){
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.DATE, 1);
			newTask.setActivityshowstart(nowTimeString);
			newTask.setActivityshowend(TimeUtils.getTime(c.getTime(), "yyyy-MM-dd HH:mm:ss"));
			newTask.setEnable(1);
		}else {
			newTask.setActivityshowstart(cpTask.getActivityshowstart());
			newTask.setActivityshowend(cpTask.getActivityshowend());
			newTask.setEnable(1);
		}
		microTaskMapper.insert(newTask);
		return newTask.getId();
	}
	
	private void cpImageText(Integer taskID, Integer newTaskID){
		MicrotaskimagetextExample imageTextExample = new MicrotaskimagetextExample();
		imageTextExample.createCriteria()
			.andMicrotaskidEqualTo(taskID);
		List<Microtaskimagetext> imageText = imageTextMapper.selectByExample(imageTextExample);
		if(imageText.size() > 0){
			Microtaskimagetext cpImageText = imageText.get(0);
			Microtaskimagetext newImageText = new Microtaskimagetext();
			newImageText.setMicrotaskid(newTaskID);
			newImageText.setImagetextcover(cpImageText.getImagetextcover());
			newImageText.setImagetextremark(cpImageText.getImagetextremark());
			newImageText.setImagetexttitle(cpImageText.getImagetexttitle());
			newImageText.setImagetexturl(cpImageText.getImagetexturl());
			imageTextMapper.insert(newImageText);
		}
	}
	
	private void cpMicroTaskRoomIDs(Integer taskID, Integer newTaskID) {
		List<String> roomIDs = microTaskCustomMapper.getRoomIDsByTaskID(taskID);
		if(roomIDs.size() > 0){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("taskID", newTaskID);
			param.put("roomIDs", roomIDs);
			microTaskCustomMapper.insertMicroTaskRoomID(param);
		}
	}

	@Override
	public CommonResult getImageTextByTaskID(Integer taskID) {
		MicrotaskimagetextExample imageTexteExample = new MicrotaskimagetextExample();
		imageTexteExample.createCriteria()
			.andMicrotaskidEqualTo(taskID);
		List<Microtaskimagetext> imageTexts = imageTextMapper.selectByExample(imageTexteExample);
		if(imageTexts.size() > 0){
			return CommonResult.ok(imageTexts.get(0));
		}
		return CommonResult.build(-1, "此任务无图文消息");
	}
}
