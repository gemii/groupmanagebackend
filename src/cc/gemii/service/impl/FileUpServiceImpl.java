package cc.gemii.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.ibatis.annotations.Param;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;

import cc.gemii.mapper.FileUpMapper;
import cc.gemii.po.Wechatroominfo;
import cc.gemii.pojo.BatchIntoGroup;
import cc.gemii.pojo.BatchUpdateRoomName;
import cc.gemii.pojo.BuildMatchGroup;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.LoveBabyInsert;
import cc.gemii.pojo.UpdateParamEntity;
import cc.gemii.service.FileUpService;
import cc.gemii.util.FileUpUtil;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.ImportExcel;
import cc.gemii.util.TimeUtils;
import cc.gemii.util.sql.DataSourceContextHolder;
import cc.gemii.util.sql.DataSourceType;

@Service
public class FileUpServiceImpl implements FileUpService {

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private Properties propertyConfigurer;

	@Autowired
	private FileUpMapper fileUpMapper;

	@Override
	public CommonResult insertFileInfo(MultipartFile file, BatchIntoGroup batchIntoGroup, String serNum) {
		Map<String, Object> resultMap = new HashMap<>();
		Map<String, String> receive = new HashMap<>();
		// 获取excel中的数据
		List<LoveBabyInsert> listwyeth = null;
		List<LoveBabyInsert> listgemii = null;
		LoveBabyInsert loveBabyInsert = new LoveBabyInsert();
		if ("1".equals(batchIntoGroup.getWeCall())) {
			receive.put("NC编码", "NcCode");
			receive.put("所辖门店", "storeName");
		}
		receive.put("省份", "province");
		receive.put("城市", "city");
		receive.put("群名", "roomName");
		receive.put("ID", "Id");
		receive.put("TYPE", "type");
		receive.put("群归属", "owner");
		receive.put("群状态", "status");
		if ("2".equals(batchIntoGroup.getWeCall()) || "3".equals(batchIntoGroup.getWeCall())) {
			receive.put("预产期起", "stareTime");
			receive.put("预产期末", "endTime");
			receive.put("地区", "area");
		}
		try {
			ImportExcel imExcel = new ImportExcel(receive, file.getInputStream());
			listwyeth = imExcel.getExcelDataList(LoveBabyInsert.class);
			listgemii = listwyeth;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return CommonResult.build(-1, "读取文件失败！");
		}

		if (null == listgemii || listgemii.size() == 0) {
			return CommonResult.build(-1, "excel文件中数据为空！");
		}
		// 传入主题和简介规则
		if ((batchIntoGroup.getTheme().length() > 6 && batchIntoGroup.getTheme().length() < 15)
				&& (batchIntoGroup.getIntroduction().length() > 10
						&& batchIntoGroup.getIntroduction().length() < 200)) {
			// 调用python返回taskId
			Map<String, String> param = new HashMap<>();
			param.put("theme", batchIntoGroup.getTheme());
			param.put("introduce", batchIntoGroup.getIntroduction());
			param.put("price", "0.0");
			param.put("limit_member_count", batchIntoGroup.getCount());
			param.put("type", batchIntoGroup.getType());
			param.put("count", String.valueOf(listgemii.size()));
			param.put("serNum", serNum);
			param.put("bot_code", batchIntoGroup.getCode());
			String resultInfo = HttpClientUtil.doPost(propertyConfigurer.getProperty("BUILD_GROUP_URL"), param);
			if ("".equals(resultInfo) || null == resultInfo) {
				return CommonResult.build(-2, "建群失败");
			}
			Map<String, Object> receiveInfo = JSON.parseObject(resultInfo);
			if ((int) receiveInfo.get("code") == 1) {
				return CommonResult.build(-2, "建群失败");
			}
			Map<String, Object> getData = (Map<String, Object>) receiveInfo.get("data");
			fileUpMapper.insertBatchInfo(getData.get("task_id").toString(), getData.get("qr_code").toString(),
					getData.get("verify_code").toString());

			int desInfo = insertInfo(listgemii, String.valueOf(getData.get("task_id")), batchIntoGroup.getWeCall());
			DataSourceContextHolder.setDbType(DataSourceType.SOURCE_WYETH);
			int desInfo1 = insertInfo(listwyeth, String.valueOf(getData.get("task_id")), batchIntoGroup.getWeCall());
			DataSourceContextHolder.setDbType(DataSourceType.SOURCE_GEMII);
			resultMap.put("成功导入数据的数量gemii", listgemii.size());
			resultMap.put("成功导入数据的数量wyeth", listwyeth.size());
			resultMap.put("重复数据的数量，未导入gemii", desInfo);
			resultMap.put("重复数据的数量，未导入wyeth", desInfo1);
			return CommonResult.ok(resultMap);
		}
		return CommonResult.build(-1, "主题长度在6-15之间，简介长度在10-200之间，所填信息不符合要求");

	}

	private int insertInfo(List<LoveBabyInsert> list, String uuId, String type) {
		int failur = 0;

		for (int i = 0; i < list.size(); i++) {
			int count = fileUpMapper.selectIsRepeat(list.get(i).getRoomName());
			list.get(i).setTaskId(uuId);
			if (count > 0) {
				list.remove(i);
				failur++;
			}
		}
		if (list.size() > 0) {
			if ("1".equals(type)) {
				fileUpMapper.insertLoveBabyInfo(list);
			} 
			if("2".equals(type)){
				fileUpMapper.insertMsInfo(list);
			}
			if("3".equals(type)){
				fileUpMapper.insertLyInfo(list);
			}
		}

		return failur;

	}

	@Override
	public CommonResult updateRoomInfo(UpdateParamEntity info) {
		Map<String, Object> param = new HashMap<>();
		Map<String, String> pythonParam = new HashMap<>();
		List<LoveBabyInsert> loveBabyInserts = fileUpMapper.selectLoveBabyInfo(info.getTaskId());
		if (loveBabyInserts.size() == 0) {
			return CommonResult.build(-1, "无该批次群信息");
		}
		param.put("inComeNum", info.getData().size());
		param.put("failureNum", (loveBabyInserts.size() - info.getData().size()));
		param.put("uMoreLocal", (info.getData().size() - loveBabyInserts.size()));
		for (int j = 0; j < info.getData().size(); j++) {
			DataSourceContextHolder.setDbType(DataSourceType.SOURCE_WYETH);
			fileUpMapper.updateInfo(loveBabyInserts.get(j).getId(), info.getData().get(j).getuRoomId(),info.getData().get(j).getCreateTime());
			DataSourceContextHolder.setDbType(DataSourceType.SOURCE_GEMII);
			fileUpMapper.updateInfo(loveBabyInserts.get(j).getId(), info.getData().get(j).getuRoomId(),info.getData().get(j).getCreateTime());
			pythonParam.put("task_id", info.getTaskId());
			pythonParam.put("chat_room_id", info.getData().get(j).getuRoomId());
			pythonParam.put("chat_room_name", loveBabyInserts.get(j).getRoomName());
			pythonParam.put("create_time", info.getData().get(j).getCreateTime());
			// 调用python那边修改群名称的接口
			String resultInfo = HttpClientUtil.doPost(propertyConfigurer.getProperty("UPDATE_GROUP_URL"), pythonParam);
			Map<String, Object> receiveInfo = JSON.parseObject(resultInfo);
			logger.info(receiveInfo.get("msg"));

		}

		return CommonResult.ok(param);
	}

	@Override
	public CommonResult buildMatchGroup(MultipartFile file, String type) {
		Map<String, Object> resultMap = new HashMap<>();
		Map<String, String> receive = new HashMap<>();
		// 获取excel中的数据
		List<BuildMatchGroup> list = null;
		BuildMatchGroup buildMatchGroup = new BuildMatchGroup();
		
		receive.put("DS", "dsId");
		receive.put("所在城市", "city");
		receive.put("孕龄", "edc");
		receive.put("当前群名", "nowRoomName");
		receive.put("医院名称", "hospitalName");
		receive.put("群类型", "type");
		receive.put("机构代码", "hospitalCode");
		try {
			ImportExcel imExcel = new ImportExcel(receive, file.getInputStream());
			list = imExcel.getExcelDataList(BuildMatchGroup.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return CommonResult.build(-1, "读取文件失败！");
		}

		if (null == list || list.size() == 0) {
			return CommonResult.build(-1, "excel文件中数据为空！");
		}
		DataSourceContextHolder.setDbType(DataSourceType.SOURCE_WYETH);
//		fileUpMapper.insertRoomInfo(list);
		updateRoomInfo(list,type);
		DataSourceContextHolder.setDbType(DataSourceType.SOURCE_GEMII);
//		fileUpMapper.insertRoomInfo(list);
		updateRoomInfo(list,type);
		return CommonResult.ok();
	}

	private void updateRoomInfo(List<BuildMatchGroup> list,String type) {
		String starTime = "";
		String endTime = "";
		for (int i = 0; i < list.size(); i++) {
			String str = list.get(i).getEdc();
			String star = str.substring(0, str.indexOf("-"));
			String end = str.substring(str.indexOf("-") + 1);
			starTime = TimeUtils.getTime(TimeUtils.getDate(star, "yyyyMMdd"), "yyyy-MM-dd");
			endTime = TimeUtils.getTime(TimeUtils.getDate(end, "yyyyMMdd"), "yyyy-MM-dd");
			if("2".equals(type)){
				if(list.get(i).getHospitalCode() != null && !"".equals(list.get(i).getHospitalCode())){
					fileUpMapper.insertKeyWord(list.get(i).getDsId(), list.get(i).getCity(), list.get(i).getHospitalCode(),
							starTime, endTime);
				}
			}
			if(list.get(i).getHospitalName() != null && !"".equals(list.get(i).getHospitalName())){
				fileUpMapper.insertKeyWord(list.get(i).getDsId(), list.get(i).getCity(), list.get(i).getHospitalName(),
						starTime, endTime);
			}

		}
	}

	@Override
	public CommonResult batchUpdateRoomname(MultipartFile file) {
		
		Map<String, String> receive = new HashMap<>();
		receive.put("旧群名", "roomName");
		receive.put("新群名", "newRoomName");
		try {
			ImportExcel imExcel = new ImportExcel(receive, file.getInputStream());
			List<BatchUpdateRoomName> excelDataList = imExcel.getExcelDataList(BatchUpdateRoomName.class);
			Map<String, String> pythonParam = new HashMap();
			for (BatchUpdateRoomName batchUpdateRoomName : excelDataList) {
				String roomName = batchUpdateRoomName.getRoomName();
				String newRoomName = batchUpdateRoomName.getNewRoomName();
				List<Wechatroominfo> info = fileUpMapper.selectRoomInfoByRoomName(roomName);
				if(null != info && info.size() > 0){
					
					logger.info("U_ROOMID" + info.get(0).getRoomid());
					DataSourceContextHolder.setDbType(DataSourceType.SOURCE_WYETH);
					fileUpMapper.updateRoomName(newRoomName,info.get(0).getRoomid());
					DataSourceContextHolder.setDbType(DataSourceType.SOURCE_GEMII);
					fileUpMapper.updateRoomName(newRoomName,info.get(0).getRoomid());
					
					pythonParam.put("task_id", info.get(0).getTaskid());
					pythonParam.put("chat_room_id", info.get(0).getRoomid());
					pythonParam.put("chat_room_name", newRoomName);
					pythonParam.put("create_time", info.get(0).getCreatedate());
					// 调用python那边修改群名称的接口
					String resultInfo = HttpClientUtil.doPost(propertyConfigurer.getProperty("UPDATE_GROUP_URL"), pythonParam);
					Map<String, Object> receiveInfo = JSON.parseObject(resultInfo);
					logger.info(receiveInfo.get("msg"));
				}else{
					logger.info("根据当前群名无法在数据库中查询到数据" + roomName);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return CommonResult.build(-1, "读取文件失败！");
		}
		return CommonResult.ok();
	}

	@Override
	public CommonResult updateCurrentCountAll() {
		DataSourceContextHolder.setDbType(DataSourceType.SOURCE_WYETH);
		List<Wechatroominfo> list = fileUpMapper.selectCurrentCountAll();
		if(null != list && list.size() > 0){
			for (Wechatroominfo wechatroominfo : list) {
				fileUpMapper.updateCurrentCountAll(wechatroominfo.getRoomid(),wechatroominfo.getCurrentcount().toString());
			}
			logger.info("bot库中的数据共" + list.size() + "条");
		}
		DataSourceContextHolder.setDbType(DataSourceType.SOURCE_GEMII);
		List<Wechatroominfo> list1 = fileUpMapper.selectCurrentCountAll();
		if(null != list1 && list1.size() > 0){
			for (Wechatroominfo wechatroominfo : list1) {
				fileUpMapper.updateCurrentCountAll(wechatroominfo.getRoomid(),wechatroominfo.getCurrentcount().toString());
			}
			logger.info("gemii库中的数据共" + list1.size() + "条");
			return CommonResult.ok();
		}
		return CommonResult.build(-1, "查询失败！");
	}

}
