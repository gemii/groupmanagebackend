package cc.gemii.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.custom.APIMapper;
import cc.gemii.pojo.ChatRecordRes;
import cc.gemii.pojo.ChatRecordResCustom;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.RoomInfo;
import cc.gemii.pojo.RoomMemReportResp;
import cc.gemii.pojo.RoomMsgReportResp;
import cc.gemii.service.APIService;
import cc.gemii.util.ExportExcel;
import cc.gemii.util.MD5Utils;
import cc.gemii.util.TimeUtils;
import cc.gemii.util.UUIDUtils;

@Service
public class APIServiceImpl implements APIService{

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private APIMapper apiMapper;
	
	private float getTimeGapByHour(String start, String end, String type){
		Long start_long = TimeUtils.getDate(start, type).getTime();
		Long end_long = TimeUtils.getDate(end, type).getTime();
		return (float) ((end_long - start_long) / 1000 / 60.0 / 60.0);
	}

	@Override
	public CommonResult getRoomInfo(String[] roomIDs) {
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("roomIDs", roomIDs);
			return CommonResult.ok(apiMapper.getRoomInfo(param));
		} catch (Exception e) {
			return CommonResult.build(500, "server error");
		}
	}

	@Override
	public Map<String, Object> getChatRecordExcel(String[] roomIDs, String start, String end,
			String path) {
		String type = "yyyy-MM-dd HH:mm:ss";
		if(!TimeUtils.validateFormat(start, type) && !TimeUtils.validateFormat(end, type))
			return null;
		//导出excel准备
		ExportExcel<List<ArrayList<String[]>>> export = new ExportExcel<List<ArrayList<String[]>>>();
		String fileName = UUIDUtils.getUUID() + ".xlsx";
		String[] sheetnames = {TimeUtils.timestamp2date(start, "yyyy-MM-dd HH:mm:ss", "yyyyMMddHHmmss") + "~" + TimeUtils.timestamp2date(end, "yyyy-MM-dd HH:mm:ss", "yyyyMMddHHmmss")};
		List<ArrayList<String[]>> list = new ArrayList<ArrayList<String[]>>();
		
		//参数包装
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("start", start);
		param.put("end", end);
		param.put("roomIDs", roomIDs);
		
		try {
			List<ChatRecordResCustom> records  = apiMapper.getChatRecordExcel(param);
			
			ArrayList<String[]> strings = new ArrayList<String[]>();
			String[] record_title = new String[]{"大区","省","城市","部门","群属性","群名称","群ID","时间","微信昵称","群内昵称","状态","类型1","类型2","消息"};
			strings.add(record_title);
			String[] record_each = null;
			for (ChatRecordResCustom record : records) {
				record_each = new String[]{record.getArea(), record.getProvince(), record.getCity(),
						record.getDepartment(), record.getGroupAttr(), record.getNowRoomName(),
						record.getInnerID(), record.getCreateTime(), MD5Utils.md5Encode(record.getUserNickName()),
						MD5Utils.md5Encode(record.getUserDisplayName()), record.getStatus(), record.getMsgType_Content_CN(),
						record.getAppMsgType_Content_CN(), record.getContent()};
				strings.add(record_each);
			}
			list.add(strings);
			export.write_Excel(path + fileName, list, sheetnames);
			
			Map<String, Object> response = new HashMap<String, Object>();
			response.put("fileName", fileName);
			response.put("file", new File(path + fileName));
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return null;
		}
		
	}
	
	@Override
	public CommonResult getChatRecord(String start, String end) {
		String type = "yyyy-MM-dd HH:mm:ss";
		if(TimeUtils.validateFormat(start, type) && TimeUtils.validateFormat(end, type)){
			if(getTimeGapByHour(start, end, type) > 6)
				return CommonResult.build(-2, "间隔不能大于6小时");
			
			List<RoomInfo> rooms = apiMapper.getRooms();
			Map<String, RoomInfo> rooms_map = new HashMap<String, RoomInfo>();
			for (RoomInfo room : rooms) {
				rooms_map.put(room.getRoomID(), room);
			}
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("start", start);
			param.put("end", end);
			List<ChatRecordRes> records = apiMapper.getChatRecord(param);
			Iterator<ChatRecordRes> it = records.iterator();
			RoomInfo room = null;
			ChatRecordRes record = null;
			while(it.hasNext()){
				record = it.next();
				room = rooms_map.get(record.getRoomID());
				if(room == null){
					it.remove();
					continue ;
				}
				record.setArea(room.getArea());
				record.setCity(room.getCity());
				record.setDepartment(room.getDepartment());
				record.setGroupAttr(room.getGroupAttr());
				record.setNowRoomName(room.getRoomName());
				record.setProvince(room.getProvince());
				record.setRoomID(room.getInnerID());
			}
			return CommonResult.ok(records);
		}else{
			return CommonResult.build(-1, "时间格式错误");
		}
	}

	@Override
	public CommonResult getRoomMemReport(String date) {
		if(!TimeUtils.validateFormat(date, "yyyyMMdd")){
			return CommonResult.build(-1, "invalid time format");
		}
		List<RoomMemReportResp> datas = apiMapper.getRoomMemReport(date);
		return CommonResult.ok(datas);
	}

	@Override
	public CommonResult getRoomMsgReport(String date) {
		if(!TimeUtils.validateFormat(date, "yyyyMMdd")){
			return CommonResult.build(-1, "invalid time format");
		}
		List<RoomMsgReportResp> datas = apiMapper.getRoomMsgReport(date);
		return CommonResult.ok(datas);
	}
	
}
