package cc.gemii.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import cc.gemii.component.SpecialMessageQueue;
import cc.gemii.component.savepoint.PointDTO;
import cc.gemii.component.savepoint.SavePointAdapter;
import cc.gemii.component.sensitive.SensitivewordUtil;
import cc.gemii.component.sensitive.SensitivewordUtil.MatchType;
import cc.gemii.component.wechat.SendTemplateMsg;
import cc.gemii.component.wechat.TemplateMsg;
import cc.gemii.fuckemoji.EmojiUtil;
import cc.gemii.mapper.custom.MessageMapper;
import cc.gemii.po.BlackWhiteListDO;
import cc.gemii.po.Wechatroommemberinfo;
import cc.gemii.po.Wechatroommessagespecial;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.RoomInfo;
import cc.gemii.pojo.RoomMessageRecord;
import cc.gemii.service.KeywordService;
import cc.gemii.service.MemberService;
import cc.gemii.service.MessageService;
import cc.gemii.service.MonitorService;
import cc.gemii.service.RoomService;
import cc.gemii.util.ParamUtil;
import cc.gemii.util.RedisUtil;
import cc.gemii.util.TimeUtils;
import cc.gemii.util.sql.DataSourceContextHolder;
import cc.gemii.util.sql.DataSourceType;
import net.sf.json.JSONObject;

@Service
public class MessageServiceImpl implements MessageService{
	
	/**
	 * owner belong映射
	 */
	private static Map<String, String> bownerBelongMap = new HashMap<String, String>();
	static {
		bownerBelongMap.put("gemii","1");
		bownerBelongMap.put("wyeth","2");
//		bownerBelongMap.put("",3);
		bownerBelongMap.put("aiyingdao","4");
		bownerBelongMap.put("meisujiaer","5");
//		bownerBelongMap.put("",6);
		bownerBelongMap.put("yibei","7");
	}
	
	private static final String HAOQI_SENSITIVEWORD_KEY = "haiqi_keyword";
	
	private static final String FUNCTION_CODE = "WHITE_CANNOT_DELETE";
	
	private Logger logger = Logger.getLogger(this.getClass());

	
	@Autowired
	private MessageMapper messageMapper;
	@Autowired
	private KeywordService keywordService;
	@Autowired
	private MonitorService monitorService;
	@Autowired
	private Properties propertyConfigurer;
	@Autowired
	private MemberService memberService;
	@Autowired
	private RoomService roomService;
	@Autowired
	private SendTemplateMsg sendTemplateMsg;
	@Autowired
	private SavePointAdapter savePointAdapter;
	
	@Override
	public CommonResult getChatRecord(String roomID, String timestamp, String type) {
		Map<String, String> param = new HashMap<String, String>();
		param.put("roomID", roomID);
		param.put("timestamp", timestamp);
		List<RoomMessageRecord> messages = null;
		switch (type) {
			// 上一页
			case ParamUtil.MESSAGE_LAST:
				param.put("point", TimeUtils.getXDayStart(timestamp, -3));
				messages = messageMapper.getChatRecordLast(param);
				break;
			// 下一页
			case ParamUtil.MESSAGE_NEXT:
				param.put("point", TimeUtils.getXDayEnd(timestamp, 3));
				messages = messageMapper.getChatRecordNext(param);
				break;
			// 分割
			case ParamUtil.MESSAGE_LIMIT:
				param.put("timestamp_low", TimeUtils.getXDayStart(timestamp, -3));
				param.put("timestamp_high", TimeUtils.getDayEnd(timestamp));
				messages = messageMapper.getChatRecordLimit(param);
				break;
			default:
				return CommonResult.build(-1, "unkown type");
		}
		
		if(ParamUtil.MESSAGE_LAST.equals(type)){
			String timestamp_low = "";
			String timestamp_high = "";
			List<RoomMessageRecord> supplement = null;
			while(messages.size() < ParamUtil.MESSAGE_PAGESIZE){
				//查询结果不足20条 需要向前一天搜索剩余条数
				timestamp_high = TimeUtils.getXDayStart(timestamp, -3);
				timestamp_low = TimeUtils.getXDayEnd(timestamp_high, -1);
				
				param.put("timestamp_low", timestamp_low);
				param.put("timestamp_high", timestamp_high);
				param.put("count", String.valueOf(ParamUtil.MESSAGE_PAGESIZE - messages.size()));
				supplement = messageMapper.getChatRecordLast(param);
				if(supplement.size() == 0){
					break ;
				}
				messages.addAll(supplement);
				timestamp = timestamp_low;
			}
		}
		
		Collections.sort(messages);
		List<Map<String, Object>> response = new ArrayList<Map<String,Object>>();
		for (RoomMessageRecord record : messages) {
			String isLegal = getIsLegal(record);
			record.setIsLegal(isLegal);
			response.add(this.handleMessage(record.toMap(), false));
		}
		return CommonResult.ok(response);
	}

	private String getIsLegal(RoomMessageRecord record) {
		//TODO 此处根据roomid,memberid查询群成员，查询不到给予isLegal值为1
		String isLegal = "1";
		String roomId = record.getRoomID();
		String memberId =  record.getMemberID();
		Wechatroommemberinfo memberInfo  = null;
		List<Wechatroommemberinfo> memberList = null;
		Map<String,String> paramMap = new HashMap<String,String>();
		paramMap.put("roomId", roomId);
		paramMap.put("memberId", memberId);
		//获取isLegal
		if(null!=roomId&&null!=memberId&&!"".equals(roomId)&&!"".equals(memberId)){
			memberList =memberService.getMemberInfoByMap(paramMap);
			if(null!=memberList&&memberList.size()>0){
				memberInfo= memberList.get(0);
			}
		}
		if(null!=memberInfo){
			isLegal = memberInfo.getIsLegal();
		}
		return isLegal;
	}

	@Override
	public CommonResult modifyMessageStatus(Integer mID, String roomID, String type) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("mID", mID);
		param.put("roomID", roomID);
		param.put("timestamp_low", TimeUtils.getXDayStart(-1));
		param.put("timestamp_high", TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
		param.put("type", type);
		if("read".equals(type)){
			//普通群消息 改为已读
			messageMapper.modifyMessageStatus(param);
		}else{
			//特殊消息(关键字消息,@班长消息) 改为已读
			messageMapper.modifySpecialMessageStatus(param);
		}
		return CommonResult.ok();
	}

	@Override
	public Map<String, Object> handleMessage(Map<String, Object> map, Boolean isRedis) {
		String roomID = String.valueOf(map.get("RoomID"));
		Integer monitorID = monitorService.getMonitorByRoomID(roomID);
		if(monitorID == null){
			return null;
		}
		map.put("mID", monitorID);
		map.put("keyword", "");
		String content = String.valueOf(map.get("Content"));
		boolean flag = false;
		Set<String> deleteKeyWords  =null;
		Integer msgType = (Integer) map.get("MsgType");
		String owner = getOwnerByRoomId(roomID);
		//根据owner给予一个全局关键字key
		String mapKey = owner + "_keyword";
		//给予一个自定义关键字key
		String customMapKey =  roomID + "_customkeyword";
		//判断其所对应的关键字map是否初始化
		if(!SensitivewordUtil.hasInitKey(mapKey) || !SensitivewordUtil.hasInitKey(customMapKey)){
			//初始化全局关键字
			initKeyWordsMap(mapKey);
			//初始化自定义关键字
			initKeyWordsMap(customMapKey);
		}
		//根据消息内容以及所属用户去匹配全局关键字
		Set<String>  keyWords = SensitivewordUtil.getSensitiveWord(mapKey, content,
				MatchType.MIN_MATCHTYPE);
		//根据消息内容匹配自定义关键字
		Set<String>  customKeyWords = SensitivewordUtil.getSensitiveWord(customMapKey, content,
				MatchType.MIN_MATCHTYPE);
		if(msgType == 1){//文本格式的做匹配
			String mapDelKey = owner + "_delkeyword";
			if(!SensitivewordUtil.hasInitKey(mapDelKey)){
				//初始化T人关键字
				initKeyWordsMap(mapDelKey);
			}
			deleteKeyWords = SensitivewordUtil.getSensitiveWord(mapDelKey, content,
					MatchType.MIN_MATCHTYPE);
		}
		//如果是班长或者白名单发的消息给予tag为false
		boolean tag = isMonitorOrWhile(String.valueOf(map.get("MemberID")),roomID);
		//logger.info("match keyword MemberID:"+String.valueOf(map.get("MemberID"))+",roomID:"+roomID+",tag:"+tag);
		//普通用户全局关键字消息
		if(null!=keyWords&&keyWords.size()>0&&tag){
			flag = true;
			map.put("keyword", "1");
		}
		//普通用户自定义关键字消息
		if(null != customKeyWords && customKeyWords.size() > 0 && tag){
			flag = true;
			map.put("keyword", "1");
		}
		//关键字匹配踢人
		String switchType = propertyConfigurer.getProperty("AUTO_DELETE_MEMBER");
		String memberID = (String) map.get("MemberID");
		if(null!=deleteKeyWords&&deleteKeyWords.size()>0
				&&"open".equals(switchType)){
			if(isRedis){
				memberService.deleteRoomMemberInfo(roomID, memberID,"2");
			}
			flag = true;
			map.put("keyword", "1");
		}
		//固定话术踢人
		deleteAltPerson(content,roomID,memberID,isRedis);
		if(content.contains("@班长")){
			flag = true;
			map.put("altMonitor", "1");
		}else if(content.contains("@栗子班长")){
			flag = true;
			map.put("altMonitor", "1");
		}else if(content.contains("@美妈帮主")){
			flag = true;
			map.put("altMonitor", "1");
		}else{
			map.put("altMonitor", "0");
		}
		//如果消息是特殊消息b并且是从redis中获取,则放入队列
		if(flag && isRedis){
			SpecialMessageQueue.offer(map);
		}
		return map;
	}


	/**
	 * 根据roomid获取owner，并在redis做对应缓存
	 * @param roomID
	 * @return
	 */
	private String getOwnerByRoomId(String roomID) {
		String owner = RedisUtil.get(roomID);
		if(null == owner || "".equals(owner)){
			owner = roomService.selectOwnerByRoomId(roomID);//(可能存在owner为空的情况)
			if(null == owner || "".equals(owner)){
				owner = "wyeth";
			}
			RedisUtil.set(roomID, owner);
		}
		return owner;
	}

	/**
	* @Title: deleteAltPerson 
	* @Description: 固定话术踢人
	* @param @param content（@班长 @某某某 妈妈违反了群规）
	* @param @param memberID
	* @param @param isRedis    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	private void deleteAltPerson(String content,String roomID, String memberID, Boolean isRedis) {
		//判断消息是否来自redis
		if(isRedis){
			String userNickName = null;
			content = content.replace(" ", "");//去除消息里所有的空格
			String[] userNickNames =null;
			if(content.contains("妈妈违反了群规")){
				userNickName= content.substring(0, content.indexOf("妈妈违反了群规")).trim();
				userNickNames = userNickName.split("@");
			}
			String owner = RedisUtil.get(roomID);
			String regex = "^@\\S*@\\S*妈妈违反了群规$";
			boolean flag = isWhitePersonPro(roomID,memberID);
			//匹配内容
			boolean matched = content.matches(regex);
			if(matched){
				if(flag){
					logger.info("固定话术T人开始执行" + content);
					Map<String,String> map = new HashMap<String,String>();
					map.put("roomId", roomID);
					String nickName = userNickNames[2];
					nickName = nickName.replace("\u2005", "");//微信@某某 时会自动添加一个不可见字符"\u2005"
					map.put("userNickName", nickName);
					List<Wechatroommemberinfo> wemberList  = null;
					if(null!=roomID&&!"".equals(roomID)
							&&null!=nickName&&!"".equals(nickName)){
						wemberList= memberService.getMemberInfoByMap(map);
					}
					Wechatroommemberinfo wi = null;
					String canNotFindMemberContent = canNotFindMemberContent(owner);
					String cannotDeleteMemberContent = cannotDeleteMemberContent(owner);
//					String canNotFindMember="这个昵称小班找不到呢~";
//					String cannotDeleteMember = "哎呀，班长混乱了……";
					//踢出内容中被alt的用户
					if(null!=wemberList){
						if(wemberList.size() == 0){
							sendToRobot(canNotFindMemberContent,roomID);
						}
						if(wemberList.size()>0){
							wi  = wemberList.get(0);
						}
						if(wemberList.size()==1){
							HashMap<String,String> param = new HashMap<>();
							//查看被T的用户memberid是否在白名单不能T的名单里面
							//在jbb环境，此时应该切换库，切换到bot库
							String memberid = wemberList.get(0).getMemberid();
							//根据此memberid去名单里面去比对
							param.put("memberid", memberid);
							param.put("functionCode", FUNCTION_CODE);
							//切换到惠氏或者bot库去查询不能被T的名单
							DataSourceContextHolder.setDbType(DataSourceType.SOURCE_WYETH);
							List<BlackWhiteListDO> selectWhiteByMemberId = memberService.selectWhiteByMemberId(param);
							DataSourceContextHolder.setDbType(DataSourceType.SOURCE_GEMII);
							//此人在不能T的名单里面
							if(null != selectWhiteByMemberId && selectWhiteByMemberId.size() > 0){
								sendToRobot(cannotDeleteMemberContent,roomID);//班长话术不能T此人
							}else{
								logger.info("固定话术开始T人,此人微信名为：" + wi.getNickname());
								memberService.deleteRoomMemberInfo(roomID, wi.getMemberid(),"3");
							}
						}
						if(wemberList.size()>1){
							sendToRobot(cannotDeleteMemberContent,roomID);
						}
					}else{
						sendToRobot(canNotFindMemberContent,roomID);
					}
				}else{
					//走redis发送消息
					//根据roomid判断其属于哪个群，发送不同的话术
					String message = null;
					//惠氏
					if(null == owner || "".equals(owner) || "wyeth".equals(owner)){
						message = propertyConfigurer.getProperty("wechat.sendToRobot.wyethMessage");
					}else{
						//TODO gemii话术  
						if(owner.equals("gemii")){
							message = propertyConfigurer.getProperty("wechat.sendToRobot.gemiiMessage");
						}
						if(owner.equals("aiyingdao")){
							message = propertyConfigurer.getProperty("wechat.sendToRobot.aiyingdaoMessage");
						}
						if(owner.equals("meisujiaer")){
							message = propertyConfigurer.getProperty("wechat.sendToRobot.meisujiaerMessage");
						}
					}
					sendToRobot(message,roomID);
				}
			}else{
				//sendToRobot("请妈妈们输入正确的踢人格式，感谢配合～",roomID);
			}
		}
	}

	/**
	 * 根据owner发送找不到此用户话术
	 * @param owner
	 * @return
	 */
	private String canNotFindMemberContent(String owner) {
		String canNotFindForWyeth = "这个昵称小班找不到呢~";
		String canNotFindForGemii = "这个昵称小班找不到呢~";//TODO 正确回复话术
		String canNotFindForLoveBaby = "这个昵称小班找不到呢~";
		String canNotFindForFriso = "这个昵称小帮主找不到呢~";
		if(null == owner || "".equals(owner) || "wyeth".equals(owner)){
			return canNotFindForWyeth;
		}else{
			//TODO gemii话术  
			if(owner.equals("gemii")){
				return canNotFindForGemii;
			}
			if(owner.equals("aiyingdao")){
				return canNotFindForLoveBaby;
			}
			if(owner.equals("meisujiaer")){
				return canNotFindForFriso;
			}
		}
		return canNotFindForWyeth;
	}

	/**
	 * 根据owner发送找到人数多于一个或此人不能T时发送话术
	 * @param owner
	 * @return
	 */
	private String cannotDeleteMemberContent(String owner) {
		String canNotDeleteForWyeth = "哎呀，小班混乱了....";
		String canNotDeleteForGemii = "哎呀，小班混乱了....";//TODO 正确回复话术
		String canNotDeleteForLoveBaby = "哎呀，小班混乱了....";
		String canNotDeleteForFriso = "哎呀，小帮主混乱了....";
		if(null == owner || "".equals(owner) || "wyeth".equals(owner)){
			return canNotDeleteForWyeth;
		}else{
			//TODO gemii话术  
			if(owner.equals("gemii")){
				return canNotDeleteForGemii;
			}
			if(owner.equals("aiyingdao")){
				return canNotDeleteForLoveBaby;
			}
			if(owner.equals("meisujiaer")){
				return canNotDeleteForFriso;
			}
		}
		return canNotDeleteForWyeth;
	}
	
	/**
	 * 
	* @Title: sendToRobot 
	* @Description: 发送消息
	* @param @param message    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	private void sendToRobot(String message,String roomID) {
		Map<String, Object> sendMap = new HashMap<String, Object>();
		sendMap.put("MsgType", "1");
		Map<String, Object> Msg = new HashMap<String, Object>();
		String mName = monitorService.getMonitorNameByRoomID(roomID);
		String uRoomId = roomService.findURoomIdByRoomId(roomID);
		Msg.put("u_roomId", uRoomId);
		Msg.put("RoomID",roomID );
		Msg.put("Content", message); 
		Msg.put("FileName", "");
		Msg.put("UserNickName", mName);
		Msg.put("MonitorSend", "monitor");//TODO 
		Msg.put("atAll", "1");
		Msg.put("memberIds", "");
		sendMap.put("Msg", Msg);
		RedisUtil.publishMessage("20170630_", JSON.toJSONString(sendMap));
	}

	/**
	 * 
	* @Title: isWhitePersonPro 
	* @Description: 判断当前固定话术是否来自白名单官方人员
	* @param @param roomID
	* @param @param memberID
	* @param @return    设定文件 
	* @return boolean    返回类型 
	* @throws
	 */
	private boolean isWhitePersonPro(String roomID, String memberID) {
		String owner = getOwnerByRoomId(roomID);
		Map<String,String> map = new HashMap<String,String>();
		map.put("owner", owner);
		map.put("memberId", memberID);
		map.put("functioncode", "WHITE_PERSON_PRO");
		//切换数据库查询此用户是否白名单
		DataSourceContextHolder.setDbType(DataSourceType.SOURCE_WYETH);
		String id = memberService.findWhiteProId(map);
		DataSourceContextHolder.setDbType(DataSourceType.SOURCE_GEMII);
		if(null!=id&&!"".equals(id)){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 
	* @Title: isMonitorOrWhile 
	* @Description: 关键字消息：判断不是白名单，并且不是班长发的消息
	* @param @param memberId
	* @param @return    设定文件 
	* @return boolean    返回类型 
	* @throws
	 */
	private boolean isMonitorOrWhile(String memberId,String roomID) {//TODO memberid换成新的字段确认此消息是否为班长
		boolean flag = false;
		Map<String,String> param = null;
		Wechatroommemberinfo memberInfo = null;
		List<Wechatroommemberinfo> memberList = null;
		if(null != memberId&&!"".equals(memberId)&&
				!"monitor".equals(memberId)){//当前消息不是班长发的
			param = new HashMap<String,String>();
			param.put("roomId", roomID);
			param.put("memberId", memberId);
			memberList = memberService.getMemberInfoByMap(param);
			if(null!=memberList&&memberList.size()>0){
				memberInfo = memberList.get(0);
			}
			if(null!=memberInfo){
				String isLegal = memberInfo.getIsLegal();
				if("2".equals(isLegal)){//白名单群成员发的消息
					return flag;
				}else{
					flag = true;
					return flag;
				}
			}else{
				flag = true;
				return flag;
			}
		}
		return flag;//班长发的消息
	}

	/**
	 * 初始化key对应的全局关键字
	 * @param mapKey
	 * @param map
	 */
	private void initKeyWordsMap(String mapKey) {
		List<String> keyWords = null;
		//全局关键字
		if(mapKey.contains("_keyword")){
			String owner = mapKey.replace("_keyword", "");
			String belong = bownerBelongMap.get(owner);
			keyWords = keywordService.selectKeyWordByKey(belong);
		}
		//自定义关键字
		if(mapKey.contains("_customkeyword")){
			String roomid = mapKey.replace("_customkeyword", "");
			keyWords = keywordService.selectCustomKeyWordByKey(roomid);
		}
		//T人关键字
		if(mapKey.contains("_delkeyword")){
			String owner = mapKey.replace("_delkeyword", "");
			String belong = bownerBelongMap.get(owner);
			keyWords = keywordService.selectDelKeyWordByKey(belong);
		}
		//初始化对应关键字
		if(null != keyWords && keyWords.size() > 0){
			StringBuffer sb  = new StringBuffer();
			for (String keyword : keyWords) {
				sb.append(keyword+",");
			}
			String words = sb.toString();
			words = words.substring(0,words.length()-1);
			SensitivewordUtil.initKey(mapKey, words);
		}
	}
	
	@Override
	public void insertSpecialMessage(Map<String, Object> map) {
		Wechatroommessagespecial specialMessage = new Wechatroommessagespecial();
		specialMessage.setAltmonitor(Object2Int(map.get("altMonitor")));
		specialMessage.setKeyword(Object2Int(map.get("keyword")));
		specialMessage.setAppmsgtype((Integer)map.get("AppMsgType"));
		specialMessage.setContent(EmojiUtil.handleEmoji(String.valueOf(map.get("Content"))));
		specialMessage.setCreatetime(TimeUtils.getDate(String.valueOf(map.get("CreateTime")), "yyyy-MM-dd HH:mm:ss"));
		specialMessage.setMemberid(String.valueOf(map.get("MemberID")));
		specialMessage.setMsgid(String.valueOf(map.get("MsgId")));
		specialMessage.setMsgtype((Integer)map.get("MsgType"));
		specialMessage.setRoomid(String.valueOf(map.get("RoomID")));
		specialMessage.setStatus(0);
		specialMessage.setUserdisplayname(EmojiUtil.handleEmoji(String.valueOf(map.get("UserDisplayName"))));
		specialMessage.setUsernickname(EmojiUtil.handleEmoji(String.valueOf(map.get("UserNickName"))));
		specialMessage.setClick(0);
		specialMessage.setMemberIcon((String)map.get("MemberIcon"));
		messageMapper.insertSpecialMessage(specialMessage);
	}
	
	private int Object2Int(Object o) {
		try {
			return "".equals(String.valueOf(o)) ? 0 : Integer.parseInt(String.valueOf(o));
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	@Override
	public CommonResult readMessage(Wechatroommessagespecial read) {
		messageMapper.updateSepcialMessageClick(read.getMsgid());
		return CommonResult.ok();
	}

	@Override
	public CommonResult getSpecialRecord(Integer mID, String timestamp, String type) {
		List<RoomMessageRecord> messages = new ArrayList<RoomMessageRecord>();
		List<RoomMessageRecord> query = null;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("mID", mID);
		param.put("type", type);
		String timestamp_low = "";
		while(messages.size() < ParamUtil.MESSAGE_PAGESIZE){
			param.put("timestamp", timestamp);
			param.put("count", ParamUtil.MESSAGE_PAGESIZE - messages.size());
			timestamp_low = TimeUtils.getXDayStart(timestamp, -3);
			param.put("timestamp_low", timestamp_low);
			query = messageMapper.getSpecialMessageLast(param);
			if(query.size() == 0)
				break;
			messages.addAll(query);
			timestamp = timestamp_low;
		}
		Collections.sort(messages);
		//添加islegal
		for (RoomMessageRecord record : messages) {
			String isLegal = getIsLegal(record);
			record.setIsLegal(isLegal);
		}
		return CommonResult.ok(messages);
	}

	@Override
	public void handleHaoQiMessage(Map<String, Object> map) {
		if(map==null){
			return;
		}
		// 为了不影响之前的逻辑，加try-cacheHAOQI_SENSITIVEWORD_KEY
		try {
			if (!SensitivewordUtil.hasInitKey(HAOQI_SENSITIVEWORD_KEY)) {
				String keyWords = propertyConfigurer.getProperty("haoqi_keywords");
				SensitivewordUtil.initKey(HAOQI_SENSITIVEWORD_KEY, keyWords);
			}
			// 提问内容
			String content = (String) map.get("Content");
			Set<String> sensitiveWords = SensitivewordUtil.getSensitiveWord(HAOQI_SENSITIVEWORD_KEY, content,
					MatchType.MIN_MATCHTYPE);
			if (sensitiveWords != null && sensitiveWords.size() > 0) {
				String keyWords =StringUtils.join(sensitiveWords, ",");
				String roomId = (String) map.get("RoomID");
				RoomInfo roomInfo = roomService.getRoomInfoByRoomId(roomId);
				if (roomInfo == null) {
					return;
				}
				String roomName = roomInfo.getRoomName();
				// 提问人群昵称
				String userDisplayName = (String) map.get("UserDisplayName");
				if(StringUtils.isBlank(userDisplayName)){
					userDisplayName = (String) map.get("UserNickName");
				}
				List<Wechatroommemberinfo> wechatMembers = memberService.getHaoQiWechatMembers(roomId);
				if (wechatMembers == null || wechatMembers.size() == 0) {
					return;
				}
				//随机获取一个记录
				Wechatroommemberinfo wechatMember = wechatMembers.get(new Random().nextInt(wechatMembers.size()));
				String haoQiUerNickname = wechatMember.getNickname();
				String haoQiUerOpenId = wechatMember.getOpenId();
				//组装模版消息与发送
				TemplateMsg templateMsg = new TemplateMsg();
				templateMsg.setFirst(new String[] { haoQiUerNickname });
				templateMsg.setKeyword1(new String[] { roomName, userDisplayName, content, keyWords });
				templateMsg.setOpenid(haoQiUerOpenId);
				String sendResult = sendTemplateMsg.sendHaoqTemplate(templateMsg);
				//埋点
				PointDTO pointDTO = new PointDTO();
				try {
					pointDTO.setPageName("关键字匹配");
					pointDTO.setPointName("匹配到关键字");
					pointDTO.setUserKey(haoQiUerOpenId);
					pointDTO.setContent(content);
					savePointAdapter.savePoint(pointDTO);
				} catch (Exception e) {
					logger.error(JSONObject.fromObject(pointDTO).toString());
					logger.error(e.getMessage(),e);
				}
			}
		} catch (Exception e) {
			logger.error("好奇关键词匹配-error",e);
		}
	}
	
}
