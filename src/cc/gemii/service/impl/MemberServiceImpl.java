package cc.gemii.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import cc.gemii.mapper.HaoQiUserInfoMapper;
import cc.gemii.mapper.MembertagMapper;
import cc.gemii.mapper.MembertagbindMapper;
import cc.gemii.mapper.WechatroommemberinfoMapper;
import cc.gemii.mapper.custom.MemberMapper;
import cc.gemii.mapper.custom.RoomMapper;
import cc.gemii.po.BlackWhiteListDO;
import cc.gemii.po.DeleteMemberHistory;
import cc.gemii.po.MembertagExample;
import cc.gemii.po.Monitor;
import cc.gemii.po.Wechatroommemberinfo;
import cc.gemii.po.WechatroommemberinfoExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.HaoQiUserInfoVO;
import cc.gemii.service.MemberService;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.RedisUtil;
import cc.gemii.util.sql.DataSourceContextHolder;
import cc.gemii.util.sql.DataSourceType;

@Service
public class MemberServiceImpl implements MemberService{

	@Autowired
	private WechatroommemberinfoMapper memberMapper;
	
	@Autowired
	private MemberMapper memberCustomMapper;
	
	@Autowired
	private MembertagMapper tagMapper;
	
	@Autowired
	private MembertagbindMapper memberTagMapper;
	
	@Autowired
	private HaoQiUserInfoMapper haoQiUserInfoMapper;
	@Autowired
	private WechatroommemberinfoMapper wechatroommemberinfoMapper;
	
	@Autowired
	private Properties propertyConfigurer;
	
	@Autowired
	private RoomMapper roomMapper;
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Override
	public CommonResult getMemberInfo(String roomID, String memberID, Integer belong) {
		WechatroommemberinfoExample mExample = new WechatroommemberinfoExample();
		mExample.createCriteria()
			.andRoomidEqualTo(roomID)
			.andMemberidEqualTo(memberID);
		List<Wechatroommemberinfo> members = memberMapper.selectByExample(mExample);
		if(members.size() > 0){
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("roomID", roomID);
			param.put("memberID", memberID);
			param.put("belong", belong);
			Map<String, Object> response = new HashMap<String, Object>();
			//用户信息
			response.put("main", members.get(0));
			//查找用户全局标签
			response.put("globalTags", memberCustomMapper.getGlobalMemberTags(param));
			//查找用户自定义标签
			response.put("diyTags", memberCustomMapper.getDIYMemberTags(param));
			return CommonResult.ok(response);
		}else{
			return CommonResult.build(-1, "未查询到此用户");
		}
		
	}

	@Override
	public CommonResult getTagList(Integer belong) {
		MembertagExample tExample = new MembertagExample();
		tExample.setOrderByClause("Type");
		tExample.createCriteria()
			.andBelongEqualTo(belong);
		return CommonResult.ok(tagMapper.selectByExample(tExample));
	}

	@Override
	public CommonResult updateMemberTag(String roomID, String memberID,
			Integer[] tags) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tags", tags);
		param.put("roomID", roomID);
		param.put("memberID", memberID);
		memberCustomMapper.deleteMemberTagBind(param);
		if(tags.length > 0){
			memberCustomMapper.insertMemberTagBind(param);
		}
		return CommonResult.ok();
	}

	
	@Override
	public List<Wechatroommemberinfo> getHaoQiWechatMembers(String roomId) {
		//切换数据源，到惠氏库查询 好奇会员信息，查询完成后再切回数据源
		DataSourceContextHolder. setDbType(DataSourceType.SOURCE_WYETH);
		List<HaoQiUserInfoVO> haoQiUserInfos = haoQiUserInfoMapper.selectHaoQiUserInfoByRoomId(roomId);
		DataSourceContextHolder. setDbType(DataSourceType.SOURCE_GEMII);
		if(haoQiUserInfos == null || haoQiUserInfos.size() == 0){
			return null;
		}
		Map<String,String> haoQiUserInfoMap = new HashMap<String,String>();
		for (HaoQiUserInfoVO haoQiUserInfoVO : haoQiUserInfos) {
			haoQiUserInfoMap.put(haoQiUserInfoVO.getNickName(), haoQiUserInfoVO.getOpenId());
		}
		List<Wechatroommemberinfo> wechatMembers = wechatroommemberinfoMapper.selectExistUserByNickName(roomId, haoQiUserInfoMap.keySet());
		if(wechatMembers == null || wechatMembers.size() == 0){
			return null;
		}
		//对匹配到的好奇用户，openId赋值
		for (Wechatroommemberinfo wechatroommemberinfo : wechatMembers) {
			wechatroommemberinfo.setOpenId(haoQiUserInfoMap.get(wechatroommemberinfo.getNickname()));
		}
		return wechatMembers;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CommonResult deleteRoomMemberInfo(String roomID, String memberID,String type) {
		String url = propertyConfigurer.getProperty("DELETE_MEMBER_URL");
		//获取U创roomid
		String u_roomId = roomMapper.findURoomIdByRoomId(roomID);
		//查询U_userId
		Map<String,String> map = new HashMap<String,String>();
		map.put("roomId", roomID);
		map.put("memberId", memberID);
		Wechatroommemberinfo roomMember = memberMapper.findUUserIdByMemberId(map);
		String u_userId = null;
		DeleteMemberHistory history =null;
		String monitorName = "";
		if(null!=roomMember){
			u_userId = roomMember.getU_userId();
			history = getHistoryData(roomMember,type);
			monitorName = history.getMonitorName();
		}
		//剔除群成员
		Map<String,String> param = new HashMap<String,String>();
		param.put("u_roomId", u_roomId);
		param.put("u_userId", u_userId);
		param.put("roomId", roomID);
		param.put("monitorName", monitorName);
		if(null!=u_roomId&&null!=u_userId){
			Map<String,Object> resultMap = (Map<String, Object>) JSON.parse(HttpClientUtil.doGet(url,param));
			String result = (String) resultMap.get("nResult");
			if(null!=result&&"1".equals(result)){
				WechatroommemberinfoExample example = new WechatroommemberinfoExample();
				example.createCriteria()
				.andRoomidEqualTo(roomID)
				.andMemberidEqualTo(memberID);
				memberMapper.deleteRoomMemberInfo(example);
				memberMapper.insertDeleteMemberHistory(history);
				logger.info("delete memberinfo ["+"type:"+type+",roomID:"+roomID+",memberID:"+memberID+",result:"+result+"]");
				return CommonResult.ok();
			}else{
				return CommonResult.build(404,(String)resultMap.get("vcResult"));
			}
		}else{
			return CommonResult.build(404,"不支持删除老群或手动拉入群的群成员！");
		}
	}

	public DeleteMemberHistory getHistoryData(Wechatroommemberinfo roomMember,String type) {
		DeleteMemberHistory history = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Monitor monitor = roomMapper.findMonitorByRoomId(roomMember.getRoomid());
		String id = UUID.randomUUID().toString().replace("-","");
		if(null!=roomMember){
			history = new DeleteMemberHistory();
			history.setHistoryId(id);
			history.setMemberId(roomMember.getMemberid());
			history.setRoomId(roomMember.getRoomid());
			history.setU_userId(roomMember.getU_userId());
			history.setUserDisplayName(roomMember.getDisplayname());
			history.setUserNickName(roomMember.getNickname());
			history.setOpenId(roomMember.getOpenId());
			history.setCreateTime(df.format(new Date()));
			history.setMonitorName(monitor.getUsername());
			history.setType(type);
		}
		return history;
	}

	public List<Wechatroommemberinfo> getMemberInfoByMap(Map<String, String> paramMap) {
		return memberMapper.getMemberInfoByMap(paramMap);
	}

	@SuppressWarnings("all")
	public CommonResult updateIsLegal(String roomID, String memberID, String isLegal) {
		Map<String,String> param = new HashMap<String,String>();
		Map<String,String> data = new HashMap<String,String>();
		param.put("roomId", roomID);
		param.put("memberId", memberID);
		String uRoomId = roomMapper.findURoomIdByRoomId(roomID);
		Wechatroommemberinfo memberInfo = memberMapper.findUUserIdByMemberId(param);
		String uMemberId = "";
		if(null!=memberInfo){
			uMemberId = memberInfo.getMemberid();
		}
		//插入数据
		data.put("uRoomId", uRoomId);
		data.put("uMemberId", uMemberId);
		data.put("flag", "");
		data.put("type","1");//1 select
		//传给python作查询
		String resultByPython = HttpClientUtil.doPost(this.propertyConfigurer.getProperty("WHITE_LIST_DO"), data);
		Map<String,Object> mapDate = (Map) JSONObject.parse(resultByPython);
		Map mapId = (Map) mapDate.get("data");
		String whileListId = (String) mapId.get("White_ID");
		if("2".equals(isLegal)){//设置白名单
			data.put("flag", "1");
		}
		if("0".equals(isLegal)){//取消白名单
			data.put("flag", "0");
		}
		if(null!=whileListId&&!"".equals(whileListId)){
			data.put("type", "2");
			//传给python作更新
			String resultUpdate = HttpClientUtil.doPost(this.propertyConfigurer.getProperty("WHITE_LIST_DO"), data);
			Map<String,Object> map = (Map) JSONObject.parse(resultUpdate);
			if(String.valueOf(map.get("code")).equals("0")){
				logger.info("python更新白名单成功!");
			}else{
				logger.info("python更新白名单失败!此用户u_MemberId : " +  uMemberId);
			}
		}else{
			data.put("type", "3");
			String resultInsert = HttpClientUtil.doPost(this.propertyConfigurer.getProperty("WHITE_LIST_DO"), data);
			Map<String,Object> map = (Map) JSONObject.parse(resultInsert);
			if(String.valueOf(map.get("code")).equals("0")){
				logger.info("python新增白名单成功!");
			}else{
				logger.info("python新增白名单失败!此用户u_MemberId : " +  uMemberId);
			}
		}
		//更新群成员数据
		Wechatroommemberinfo record = new Wechatroommemberinfo();
		record.setRoomid(roomID);
		record.setMemberid(memberID);
		record.setIsLegal(isLegal);
		memberMapper.updateByPrimaryKeySelective(record);
		
		//删除redis缓存数据
		String key = ":1:wechatroommemberinfo_data:"+roomID+":"+uMemberId;
		long result = RedisUtil.del(key);
		return CommonResult.ok();
	}

	@Override
	public String findWhiteProId(Map<String, String> map) {
		return memberMapper.findWhiteProId(map);
	}

	@Override
	public List<BlackWhiteListDO> selectWhiteByMemberId(Map<String, String> map) {
		return memberMapper.selectWhiteByMemberId(map);
	}
}
