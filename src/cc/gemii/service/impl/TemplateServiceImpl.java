package cc.gemii.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.KnowledgeanswerMapper;
import cc.gemii.mapper.KnowledgequestionMapper;
import cc.gemii.mapper.custom.UserMapper;
import cc.gemii.po.Knowledgeanswer;
import cc.gemii.po.Knowledgequestion;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.QuestionTemplate;
import cc.gemii.service.TemplateService;

@Service
public class TemplateServiceImpl implements TemplateService{

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private KnowledgequestionMapper questionMapper;
	
	@Autowired
	private KnowledgeanswerMapper answerMapper;
	
	@Override
	public CommonResult searchTemplate(Integer belong, String content) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("content", "%" + content + "%");
		param.put("belong", belong);
		//查询相应belong或者belong为null的数据，belong为null代表公共部分
		List<QuestionTemplate> searchTemplate = userMapper.searchTemplate(param);
		return CommonResult.ok(searchTemplate);
	}
	
	@Override
	public CommonResult addTemplate(QuestionTemplate template) {
		Knowledgequestion question = new Knowledgequestion();
		question.setQuestion(template.getQuestion());
		question.setBelong(template.getBelong());
		questionMapper.insert(question);
		Knowledgeanswer answer = new Knowledgeanswer();
		answer.setContent(template.getAnswer());
		answer.setQuestionid(question.getId());
		answerMapper.insert(answer);
		return CommonResult.ok();
	}
	
}
