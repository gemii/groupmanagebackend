package cc.gemii.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import cc.gemii.mapper.WechatroominfoMapper;
import cc.gemii.po.Wechatroominfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.ExcelInfoUpdateService;
import cc.gemii.util.ImportExcel;
import cc.gemii.util.sql.DataSourceContextHolder;
import cc.gemii.util.sql.DataSourceType;

@Service
public class ExcelInfoUpdateServiceImpl implements ExcelInfoUpdateService{

	private Logger logger = Logger.getLogger(this.getClass());
	
	private static Map<String, String> resultCodeMap = new HashMap<String, String>();
	static {
		resultCodeMap.put("wyethpulling", "1");
	}
	
	@Autowired
	private WechatroominfoMapper weChatroominfoMapper;
	
	@Override
	public CommonResult excelInfoUpdate(MultipartFile file) throws Exception {
		Map<String, String> map = new HashMap<>();
		map.put("群ID", "roomid");
		map.put("InnerId", "innerid");
		map.put("群名称", "roomname");
		map.put("群归属", "owner");
		map.put("群状态", "status");
		try {
			//获取excel中的数据
			ImportExcel importExcel = new ImportExcel(map,file.getInputStream());
			List<Wechatroominfo> excelDataList = importExcel.getExcelDataList(Wechatroominfo.class);
			//创建一个用于收集错误信息的list(三个条件都无法匹配到)
			List<Integer> list = new ArrayList<>();
			//创建一个用于收集错误信息的list(两表数据不同步)
			List<Integer> list2 = new ArrayList<>();
			//创建一个用于收集错误信息的list(owner或status不满足规则)
			List<Integer> list3 = new ArrayList<>();
			
			Map<Object, Object> map1 = new HashMap<>();
			int i = 2;
			for (Wechatroominfo wechatroominfo : excelDataList) {
				
				boolean b = true;
				b = checkNameRight(wechatroominfo,b);
				
				//根据三个条件查询
				Wechatroominfo selectRoom =  null;
				if( selectRoom == null  && (null != wechatroominfo.getOwner() || null !=  wechatroominfo.getStatus())){
					selectRoom = weChatroominfoMapper.selectRoomByRoomId(wechatroominfo.getRoomid());
				}
				if( selectRoom == null){
					selectRoom = weChatroominfoMapper.selectRoomByInnerId(wechatroominfo.getInnerid());
				}
				if( selectRoom == null){
					selectRoom =  weChatroominfoMapper.selectRoomByRoomName(wechatroominfo.getRoomname());
				}
				if( selectRoom == null || b == false){
					logger.info("根据以下条件无法匹配到数据:"+"RoomID:"+wechatroominfo.getRoomid()+"--"+"InnerID:"+wechatroominfo.getInnerid()+"--"+"RoomName:"+wechatroominfo.getRoomname());
					list.add(i);
				}
				if(selectRoom != null && b == true){
					//两个数据库中数据不同步的情况
					DataSourceContextHolder.setDbType(DataSourceType.SOURCE_WYETH);
					Wechatroominfo selectRoomByRoomId = weChatroominfoMapper.selectRoomByRoomId(selectRoom.getRoomid());
					if(!selectRoom.equals(selectRoomByRoomId)){
						list2.add(i);
					}else{
						DataSourceContextHolder.setDbType(DataSourceType.SOURCE_GEMII);
						//根据条件给grouptag赋值
						setGroupTag(wechatroominfo);
						updateRoomInfo(selectRoom, wechatroominfo);
					}
				}
				if(!b){
					list3.add(i);
					if(list2.contains(i)){
						for(int j = 0;j<list2.size();j++){
							if(list2.get(j) == i){
								list2.remove(j);
							}
						}
					}
					if(list.contains(i)){
						for(int j = 0;j<list.size();j++){
							if(list.get(j) == i){
								list.remove(j);
							}
						}
					}
				}
				i++;
			}
			if((null != list && list.size() > 0) || (null != list2 && list2.size() > 0) || (null != list3 && list3.size() > 0) ){
				return resultMap(list,list2,list3,map1);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(-1, "读取文件失败！");
		}

		return CommonResult.ok("数据修改成功");
	}
	
	
	/**
	 * 根据错误List返还错误信息
	 */
	private CommonResult resultMap(List<Integer> list, List<Integer> list2, List<Integer> list3,
			Map<Object, Object> map1) {
		StringBuffer sb = new StringBuffer();
		for (Integer no : list) {
			sb.append(no+",");
		}
		for(Integer no : list2){
			sb.append(no+",");
		}
		for(Integer no : list3){
			sb.append(no+",");
		}
		sb.deleteCharAt(sb.length()-1);
		map1.put("noSelect", list);
		map1.put("noEquals", list2);
		map1.put("noMatch", list3);
		return CommonResult.build(-1, "更新部分成功，以下行数更新失败:"+sb,map1);
	}

	/**
	 * 根据owner,status给grouptag赋值
	 */
	private void setGroupTag(Wechatroominfo wechatroominfo) {
		//同时满足归属为惠氏，状态拉人中
		String key = wechatroominfo.getOwner()+wechatroominfo.getStatus();
		Set<String> keySet = resultCodeMap.keySet();
		for (String str : keySet) {
			if(str.equals(key)){
				wechatroominfo.setGrouptag(resultCodeMap.get(str));
			}else{
				wechatroominfo.setGrouptag(null);
			}
		}
	}

	/**
	 * 校验owner,status是否满足条件
	 */
	private boolean checkNameRight(Wechatroominfo wechatroominfo, boolean b) {
		if(null != wechatroominfo.getOwner()){
			String owner = wechatroominfo.getOwner();
			if(!owner.equals("gemii") && !owner.equals("wyeth") && !owner.equals("haoyuer") && !owner.equals("aiyingdao") && !owner.equals("meisujiaer")){
				logger.info("不满足条件的名称(群归属):"+owner);
				return b = false;
			}
		}
		
		if(null != wechatroominfo.getStatus()){
			String status = wechatroominfo.getStatus();
			if(!status.equals("unknown") && !status.equals("nousing") && !status.equals("pulling") && !status.equals("nopulling")){
				logger.info("不满足条件的名称(群状态):"+status);
				return b = false;
			}
		}
		return b;
	}

	/**
	 * 去两个库更新数据
	 */
	private void updateRoomInfo(Wechatroominfo roomInfoUpdate, Wechatroominfo wechatroominfo) {
		roomInfoUpdate.setOwner(wechatroominfo.getOwner());
		roomInfoUpdate.setStatus(wechatroominfo.getStatus());
		roomInfoUpdate.setGrouptag(wechatroominfo.getGrouptag());
		weChatroominfoMapper.update(roomInfoUpdate);
		DataSourceContextHolder.setDbType(DataSourceType.SOURCE_WYETH);
		weChatroominfoMapper.update(roomInfoUpdate);
		DataSourceContextHolder.setDbType(DataSourceType.SOURCE_GEMII);
	}


}
