package cc.gemii.service;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;
import cc.gemii.pojo.RoomInfo;

public interface RoomService {

	/**
	 * 
	 * @param mID 
	 * @return
	 * TODO 通过班长ID获取群列表
	 */
	CommonResult getRoomByMid(Integer mID);

	/**
	 * 
	 * @param roomID 群ID
	 * @return
	 * TODO 通过群ID取群成员
	 */
	CommonResult getMemberByRid(String roomID);

	/**
	 * 
	 * @param mID 班长ID
	 * @param keyword 关键字
	 * @return
	 * TODO 根据关键字获得对应的群
	 */
	CommonResult getRoomByKeyword(Integer mID, String keyword);

	/**
	 * 
	 * @param mID 班长ID
	 * @return
	 * TODO 根据班长ID获得各个群对应的关键字
	 */
	CommonResult getRoomKeyword(Integer mID);

	/**
	 * 
	 * @param roomID 群ID
	 * @param tags 标签内容 数组
	 * @return
	 * TODO
	 */
	CommonResult insertRoomTag(String roomID, String[] tags);

	/**
	 * 
	 * @param roomID 群ID
	 * @param tag 标签
	 * @return
	 * TODO
	 */
	CommonResult deleteRoomTag(String roomID, String tag);

	/**
	 * 
	 * @param roomID 群ID
	 * @return
	 * TODO 根据群ID查询tag
	 */
	CommonResult getRoomTagByRoomID(String roomID);

	/**
	 * 
	 * @param mID 班长ID
	 * @param roomID 群ID
	 * @param action 动作  mark 标记 cancel 取消 
	 * TODO 班长标记群
	 */
	CommonResult markRoom(Integer mID, String roomID, String action);

	/**
	 * 
	 * @param mID 班长ID
	 * @return
	 * TODO 根据班长ID获取管理的群
	 */
	CommonResult getManagedRoom(Integer mID);

	/**
	 * 
	 * @param belong 
	 * @return
	 * TODO 获取csr 群信息
	 */
	CommonResult getCSRRoom(Pagination page, String belong);
	
	/**
	 * 
	 * @param roomIDs 群ID字符串  ","分隔
	 * @return
	 * TODO 验证群ID是否正确或是否存在于csr群中
	 */
	CommonResult validateRoomIDs(String roomIDs);

	/**
	 * 
	 * @param mID 班长ID
	 * @param percent 百分比
	 * @return
	 * TODO 获取该班长最低活跃度的群，个数为count
	 */
	CommonResult lowestLiveness(Integer mID, String percent);

	/**
	 * 
	 * @param mID 班长ID
	 * @return
	 * TODO 群活跃度列表
	 */
	CommonResult livenessList(Integer mID);

	/**
	 * 根据群Id，查询群信息
	 * @param roomId 群Id
	 * @return 群信息
	 * */
	RoomInfo getRoomInfoByRoomId(String roomId);
	
	/**
	 * 根据群Id，NickName，查询成员信息
	 * @param roomId 群Id
	 * @return 群信息
	 * */
	CommonResult getMemberByMap(Map<String, Object> map);

	/**
	 * 
	* @Title: findRoomStatisticsInfo 
	* @Description: TODO 爱婴岛群成员统计 
	* @param @return    设定文件 
	* @return List<RoomInfo>    返回类型 
	* @throws
	 */
	List<RoomInfo> findRoomStatisticsInfo();

	String findURoomIdByRoomId(String roomID);

	/**
	 * 根据roomID判断当前群归属
	 * @param roomID
	 * @return
	 */
	String selectOwnerByRoomId(String roomID);
}
