package cc.gemii.service;

import cc.gemii.po.MicrotaskWithBLOBs;
import cc.gemii.po.Microtaskimagetext;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Pagination;

public interface MicroTaskService {

	/**
	 * 
	 * @param microTask 微任务包装类
	 * @param imageText 微任务图文
	 * @param roomIDs 微任务对应群列表
	 * @param isImageText 是否包含图文消息
	 * @param response
	 * @return
	 * TODO 创建微任务
	 */
	CommonResult createOrUpdated(MicrotaskWithBLOBs microTask, Microtaskimagetext imageText, String[] roomIDs, Integer isImageText);

	/**
	 * 
	 * @param area 
	 * @return
	 * TODO 根据区域获取包含城市
	 */
	CommonResult getCityByArea(String area);

	/**
	 * @param search 查找字符串
	 * @param belong  1 gemii 2 wyeth
	 * @param page 分页包装类
	 * @return
	 * TODO 获取微任务列表
	 */
	CommonResult getMicroTaskList(String search, Integer belong, Pagination page);

	/**
	 * 
	 * @param taskID 任务ID
	 * @return
	 * TODO 删除任务
	 */
	CommonResult delete(Integer taskID);

	/**
	 * 
	 * @param taskID 任务ID
	 * @return
	 * TODO 根据任务ID获取此任务详细信息
	 */
	CommonResult getDetailByID(Integer taskID);

	/**
	 * 
	 * @param taskID 任务ID
	 * @return
	 * TODO 启用 停用切换
	 */
	CommonResult updateTaskEnable(Integer taskID);

	/**
	 * 
	 * TODO 有新任务时通知班长
	 */
	void noticeMonitor();

	/**
	 * 
	 * @param taskID 任务ID
	 * @return
	 * TODO 拷贝任务
	 */
	CommonResult cpMicroTask(Integer taskID);

	/**
	 * 
	 * @param userName 用户名
	 * @param password 密码
	 * @param type 
	 * @return 登录
	 * TODO
	 */
	CommonResult login(String userName, String password, Integer type);

	/**
	 * 
	 * @param taskID 任务ID
	 * @return
	 * TODO 获取任务图文信息
	 */
	CommonResult getImageTextByTaskID(Integer taskID);

}
