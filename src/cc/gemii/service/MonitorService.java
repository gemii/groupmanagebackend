package cc.gemii.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartFile;

import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.GeneralPagingResult;
import cc.gemii.data.admin.ImportedMonitorRoomItem;
import cc.gemii.data.admin.MonitorRoomInfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.SendMessageBoundary;


public interface MonitorService {
	
	/**
	 * 
	 * @param username 班长账号
	 * @param password 密码
	 * @return
	 * TODO 登录
	 */
	CommonResult login(String username, String password);

	/**
	 * 
	 * @param data 加密字符串
	 * @return
	 * TODO 获取用户信息
	 */
	CommonResult getUserInfo(String data);

	/**
	 * 
	 * @param request 
	 * @param 班长发送消息边界类
	 * @return
	 * TODO
	 */
	CommonResult sendMessage(HttpServletRequest request, SendMessageBoundary sendMessage);

	

	/**
	 * 
	 * @param mID 班长ID
	 * @param action 动作
	 * @return
	 * TODO 班长 上班 下班 午休
	 */
	CommonResult insertMonitorRecord(Integer mID, String action);

	/**
	 * 
	 * @param mID 班长ID
	 * @return
	 * TODO 获取班长信息
	 */
	CommonResult getMonitorInfo(Integer mID);

	/**
	 * 
	 * @param robotTag 机器人tag
	 * @return
	 * TODO 班长登录
	 */
	CommonResult monitorLogin(String robotTag,HttpServletRequest request);

	/**
	 * 
	 * @param roomID 群ID
	 * @return
	 * TODO 根据群ID匹配班长
	 */
	Integer getMonitorByRoomID(String roomID);

	/**
	 * 
	 * @param mID 
	 * @param roomID 群ID
	 * @return
	 * TODO 根据群ID查询发言机器人
	 */
	CommonResult getSendRobotByRoomID(Integer mID, String roomID);

	/**
	 * 
	 * @param mID 班长ID
	 * @return
	 * TODO 根据班长ID获取要监听的频道
	 */
	List<String> getChannelByMid(Integer mID);

	/**
	 * 
	 * @return
	 * TODO 初始化监听频道
	 */
	String[] getInitChannel();

	/**
	 * 
	 * @param monitorName 班长机器人名称
	 * @param mID 用户ID
	 * @return
	 * TODO 用户增加班长机器人名称
	 */
	CommonResult addSendRobot(String monitorName, Integer mID,HttpServletRequest request);

	/**
	 * 
	 * @param mID
	 * @param roomID
	 * @param belong 
	 * @return
	 * TODO
	 */
	CommonResult getMicroTask(Integer mID, String roomID, Integer belong);

	/**
	 * 根据导入的Excel更新群归属关系
	 * @param file
	 * @return
	 */
	CommonResult updateRoomOwner(MultipartFile file);

	String getMonitorNameByRoomID(String roomID);
	
	GeneralContentResult<Map<String,Object>> importMonitorRoom(List<ImportedMonitorRoomItem> importedMonitorRoomItems,Integer belong);

	/**
	 * 根据条件查询群以及对应的班长
	 * @param monitorRoomInfo
	 * @param page
	 * @param size
	 * @return
	 */
	GeneralPagingResult<MonitorRoomInfo> searchMonitorRoom(MonitorRoomInfo monitorRoomInfo, Integer page,Integer size);

	/**
	 * 删除班长与群的关联关系(单条记录)
	 * @param monitorRoomId
	 */
	void deleteMonitorRoom(String monitorRoomId);
}
