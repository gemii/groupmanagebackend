package cc.gemii.service;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.QuestionTemplate;

public interface TemplateService {
	/**
	 * 
	 * @param belong 1 gemii 2 wyeth
	 * @param content 查询内容
	 * @return
	 * TODO 用户查询模板消息
	 */
	CommonResult searchTemplate(Integer belong, String content);
	
	/**
	 * 
	 * @param template 模板包装类
	 * @return
	 * TODO 用户添加模板消息
	 */
	CommonResult addTemplate(QuestionTemplate template);


}
