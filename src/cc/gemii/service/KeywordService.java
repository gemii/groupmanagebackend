package cc.gemii.service;

import java.util.List;
import java.util.Map;

import cc.gemii.po.Monitor;
import cc.gemii.pojo.CommonResult;

public interface KeywordService {

	/**
	 * 
	 * @param belong 客户类型
	 * @return
	 * TODO 获取全局关键字
	 */
	CommonResult getGlobalKeyword(Integer belong);

	/**
	 * 
	 * @param mID 班长ID
	 * @return
	 * TODO 根据班长ID获取群关键字
	 */
	CommonResult getKeywordByMid(String mID);

	/**
	 * 
	 * @param mID 班长ID
	 * @param keyword 要删除的关键字
	 * @return
	 * TODO 班长删除关键字
	 */
	CommonResult deleteKeyword(String mID, String keyword);

	/**
	 * 
	 * @param mID 班长ID
	 * @param roomIDs 群ID数组 
	 * @param keyword 要添加的关键字
	 * @param status 关键字状态
	 * @return
	 * TODO 班长添加关键字
	 */
	CommonResult insertKeyword(String mID, String[] roomIDs, String keyword,String status);

	/**
	 * 
	 * @param keyword 要改变状态的关键字
	 * @param mID 班长ID
	 * @return
	 * TODO 关键字改变状态 开启 关闭
	 */
	CommonResult modifyStatus(String keyword, String mID);

	/**
	 * 
	 * @param mID 班长ID
	 * @param keyword 关键词
	 * @param roomIDs 群列表
	 * @return
	 * TODO 用户修改关键字对应的列表
	 */
	CommonResult updateKeyword(String mID, String keyword, String[] roomIDs);

	/**
	 * 
	 * @param RoomID 群ID 
	 * @return
	 * TODO 检查消息中是否有关键字
	 */
	List<String> getKeywordByRoomID(String RoomID);
	/**
	 * @param belong 
	 * 
	* @Title: initKeyWordMap 
	* @Description: TODO 初始化关键字库 
	* @param @return    设定文件 
	* @return CommonResult    返回类型 
	* @throws
	 */
	CommonResult initKeyWordMap(String belong);

	/**
	 * 
	* @Title: getKeyWordByMap 
	* @Description: TODO 查询关键字
	* @param @param param
	* @param @return    设定文件 
	* @return List<String>    返回类型 
	* @throws
	 */
	List<String> getKeyWordByMap(Map<String, String> param);

	/**
	 * 根据key查找相应全局关键字
	 * @param mapKey
	 * @return
	 */
	List<String> selectKeyWordByKey(String mapKey);

	/**
	 * 根据key查找相应自定义关键字
	 * @param mapKey
	 * @return
	 */
	List<String> selectCustomKeyWordByKey(String mapCustomKey);
	
	/**
	 * 根据delkey查找相应关键字
	 * @param mapKey
	 * @return
	 */
	List<String> selectDelKeyWordByKey(String mapDelKey);




}
