package cc.gemii.service;

import org.springframework.web.multipart.MultipartFile;

import cc.gemii.pojo.CommonResult;

public interface ExcelInfoUpdateService {

	/**
	 * 解析excel进行数据更新
	 * @param file
	 * @return
	 * @throws Exception 
	 */
	CommonResult excelInfoUpdate(MultipartFile file) throws Exception;

}
