package cc.gemii.service;

import java.util.Map;

import cc.gemii.pojo.CommonResult;

public interface APIService {

	/**
	 * 
	 * @param start 开始时间 格式yyyy-MM-dd HH:mm:ss
	 * @param end 结束时间 格式yyyy-MM-dd HH:mm:ss
	 * @return
	 * TODO 获取聊天记录
	 */
	CommonResult getChatRecord(String start, String end);
	
	/**
	 * 
	 * @param roomIDs 群ID数组
	 * @param response
	 * @return
	 * TODO 获取群信息
	 */
	CommonResult getRoomInfo(String[] roomIDs);

	/**
	 * 
	 * @param roomIDs 群ID数组
	 * @param start 开始时间
	 * @param end 结束时间
	 * @param path 路径
	 * @return
	 * TODO
	 */
	Map<String, Object> getChatRecordExcel(String[] roomIDs, String start, String end, String path);

	CommonResult getRoomMemReport(String date);

	CommonResult getRoomMsgReport(String date);

}
