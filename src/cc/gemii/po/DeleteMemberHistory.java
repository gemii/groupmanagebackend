package cc.gemii.po;

public class DeleteMemberHistory {
	
	private String HistoryId;
	
	private String RoomId;
	
	private String MemberId;
	
	private String UserDisplayName;
	
	private String UserNickName;
	
	private String CreateTime;
	
	private String MonitorName;
	
	private String U_userId;
	
	private String OpenId;
	
	private String Type;

	public String getHistoryId() {
		return HistoryId;
	}

	public void setHistoryId(String historyId) {
		HistoryId = historyId;
	}

	public String getRoomId() {
		return RoomId;
	}

	public void setRoomId(String roomId) {
		RoomId = roomId;
	}

	public String getMemberId() {
		return MemberId;
	}

	public void setMemberId(String memberId) {
		MemberId = memberId;
	}

	public String getUserDisplayName() {
		return UserDisplayName;
	}

	public void setUserDisplayName(String userDisplayName) {
		UserDisplayName = userDisplayName;
	}

	public String getUserNickName() {
		return UserNickName;
	}

	public void setUserNickName(String userNickName) {
		UserNickName = userNickName;
	}

	public String getCreateTime() {
		return CreateTime;
	}

	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}

	public String getMonitorName() {
		return MonitorName;
	}

	public void setMonitorName(String monitorName) {
		MonitorName = monitorName;
	}

	public String getU_userId() {
		return U_userId;
	}

	public void setU_userId(String u_userId) {
		U_userId = u_userId;
	}

	public String getOpenId() {
		return OpenId;
	}

	public void setOpenId(String openId) {
		OpenId = openId;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}
	
}
