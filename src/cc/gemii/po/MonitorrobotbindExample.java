package cc.gemii.po;

import java.util.ArrayList;
import java.util.List;

public class MonitorrobotbindExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MonitorrobotbindExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andMonitoridIsNull() {
            addCriterion("MonitorID is null");
            return (Criteria) this;
        }

        public Criteria andMonitoridIsNotNull() {
            addCriterion("MonitorID is not null");
            return (Criteria) this;
        }

        public Criteria andMonitoridEqualTo(Integer value) {
            addCriterion("MonitorID =", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridNotEqualTo(Integer value) {
            addCriterion("MonitorID <>", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridGreaterThan(Integer value) {
            addCriterion("MonitorID >", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridGreaterThanOrEqualTo(Integer value) {
            addCriterion("MonitorID >=", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridLessThan(Integer value) {
            addCriterion("MonitorID <", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridLessThanOrEqualTo(Integer value) {
            addCriterion("MonitorID <=", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridIn(List<Integer> values) {
            addCriterion("MonitorID in", values, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridNotIn(List<Integer> values) {
            addCriterion("MonitorID not in", values, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridBetween(Integer value1, Integer value2) {
            addCriterion("MonitorID between", value1, value2, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridNotBetween(Integer value1, Integer value2) {
            addCriterion("MonitorID not between", value1, value2, "monitorid");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("Type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("Type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("Type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("Type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("Type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("Type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("Type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("Type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("Type like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("Type not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("Type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("Type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("Type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("Type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andRobottagIsNull() {
            addCriterion("RobotTag is null");
            return (Criteria) this;
        }

        public Criteria andRobottagIsNotNull() {
            addCriterion("RobotTag is not null");
            return (Criteria) this;
        }

        public Criteria andRobottagEqualTo(String value) {
            addCriterion("RobotTag =", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagNotEqualTo(String value) {
            addCriterion("RobotTag <>", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagGreaterThan(String value) {
            addCriterion("RobotTag >", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagGreaterThanOrEqualTo(String value) {
            addCriterion("RobotTag >=", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagLessThan(String value) {
            addCriterion("RobotTag <", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagLessThanOrEqualTo(String value) {
            addCriterion("RobotTag <=", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagLike(String value) {
            addCriterion("RobotTag like", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagNotLike(String value) {
            addCriterion("RobotTag not like", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagIn(List<String> values) {
            addCriterion("RobotTag in", values, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagNotIn(List<String> values) {
            addCriterion("RobotTag not in", values, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagBetween(String value1, String value2) {
            addCriterion("RobotTag between", value1, value2, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagNotBetween(String value1, String value2) {
            addCriterion("RobotTag not between", value1, value2, "robottag");
            return (Criteria) this;
        }

        public Criteria andMonitornameIsNull() {
            addCriterion("MonitorName is null");
            return (Criteria) this;
        }

        public Criteria andMonitornameIsNotNull() {
            addCriterion("MonitorName is not null");
            return (Criteria) this;
        }

        public Criteria andMonitornameEqualTo(String value) {
            addCriterion("MonitorName =", value, "monitorname");
            return (Criteria) this;
        }

        public Criteria andMonitornameNotEqualTo(String value) {
            addCriterion("MonitorName <>", value, "monitorname");
            return (Criteria) this;
        }

        public Criteria andMonitornameGreaterThan(String value) {
            addCriterion("MonitorName >", value, "monitorname");
            return (Criteria) this;
        }

        public Criteria andMonitornameGreaterThanOrEqualTo(String value) {
            addCriterion("MonitorName >=", value, "monitorname");
            return (Criteria) this;
        }

        public Criteria andMonitornameLessThan(String value) {
            addCriterion("MonitorName <", value, "monitorname");
            return (Criteria) this;
        }

        public Criteria andMonitornameLessThanOrEqualTo(String value) {
            addCriterion("MonitorName <=", value, "monitorname");
            return (Criteria) this;
        }

        public Criteria andMonitornameLike(String value) {
            addCriterion("MonitorName like", value, "monitorname");
            return (Criteria) this;
        }

        public Criteria andMonitornameNotLike(String value) {
            addCriterion("MonitorName not like", value, "monitorname");
            return (Criteria) this;
        }

        public Criteria andMonitornameIn(List<String> values) {
            addCriterion("MonitorName in", values, "monitorname");
            return (Criteria) this;
        }

        public Criteria andMonitornameNotIn(List<String> values) {
            addCriterion("MonitorName not in", values, "monitorname");
            return (Criteria) this;
        }

        public Criteria andMonitornameBetween(String value1, String value2) {
            addCriterion("MonitorName between", value1, value2, "monitorname");
            return (Criteria) this;
        }

        public Criteria andMonitornameNotBetween(String value1, String value2) {
            addCriterion("MonitorName not between", value1, value2, "monitorname");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}