package cc.gemii.po;

public class Microtask {
    private Integer id;

    private String activitytitle;

    private String activityimage;

    private String activityshowstart;

    private String activityshowend;

    private String pushstart;

    private String pushend;

    private String pushtime;

    private String createtime;

    private String lastupdatetime;

    private Integer enable;

    private Integer isdelete;

    private Integer belong;

    private String activitycontent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivitytitle() {
        return activitytitle;
    }

    public void setActivitytitle(String activitytitle) {
        this.activitytitle = activitytitle == null ? null : activitytitle.trim();
    }

    public String getActivityimage() {
        return activityimage;
    }

    public void setActivityimage(String activityimage) {
        this.activityimage = activityimage == null ? null : activityimage.trim();
    }

    public String getActivityshowstart() {
        return activityshowstart;
    }

    public void setActivityshowstart(String activityshowstart) {
        this.activityshowstart = activityshowstart == null ? null : activityshowstart.trim();
    }

    public String getActivityshowend() {
        return activityshowend;
    }

    public void setActivityshowend(String activityshowend) {
        this.activityshowend = activityshowend == null ? null : activityshowend.trim();
    }

    public String getPushstart() {
        return pushstart;
    }

    public void setPushstart(String pushstart) {
        this.pushstart = pushstart == null ? null : pushstart.trim();
    }

    public String getPushend() {
        return pushend;
    }

    public void setPushend(String pushend) {
        this.pushend = pushend == null ? null : pushend.trim();
    }

    public String getPushtime() {
        return pushtime;
    }

    public void setPushtime(String pushtime) {
        this.pushtime = pushtime == null ? null : pushtime.trim();
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime == null ? null : createtime.trim();
    }

    public String getLastupdatetime() {
        return lastupdatetime;
    }

    public void setLastupdatetime(String lastupdatetime) {
        this.lastupdatetime = lastupdatetime == null ? null : lastupdatetime.trim();
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    public Integer getBelong() {
        return belong;
    }

    public void setBelong(Integer belong) {
        this.belong = belong;
    }

    public String getActivitycontent() {
        return activitycontent;
    }

    public void setActivitycontent(String activitycontent) {
        this.activitycontent = activitycontent == null ? null : activitycontent.trim();
    }
}