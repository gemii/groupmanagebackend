package cc.gemii.po;

import java.util.ArrayList;
import java.util.List;

public class SendapicallrecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SendapicallrecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andMonitoridIsNull() {
            addCriterion("MonitorID is null");
            return (Criteria) this;
        }

        public Criteria andMonitoridIsNotNull() {
            addCriterion("MonitorID is not null");
            return (Criteria) this;
        }

        public Criteria andMonitoridEqualTo(Integer value) {
            addCriterion("MonitorID =", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridNotEqualTo(Integer value) {
            addCriterion("MonitorID <>", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridGreaterThan(Integer value) {
            addCriterion("MonitorID >", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridGreaterThanOrEqualTo(Integer value) {
            addCriterion("MonitorID >=", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridLessThan(Integer value) {
            addCriterion("MonitorID <", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridLessThanOrEqualTo(Integer value) {
            addCriterion("MonitorID <=", value, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridIn(List<Integer> values) {
            addCriterion("MonitorID in", values, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridNotIn(List<Integer> values) {
            addCriterion("MonitorID not in", values, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridBetween(Integer value1, Integer value2) {
            addCriterion("MonitorID between", value1, value2, "monitorid");
            return (Criteria) this;
        }

        public Criteria andMonitoridNotBetween(Integer value1, Integer value2) {
            addCriterion("MonitorID not between", value1, value2, "monitorid");
            return (Criteria) this;
        }

        public Criteria andRoomidIsNull() {
            addCriterion("RoomID is null");
            return (Criteria) this;
        }

        public Criteria andRoomidIsNotNull() {
            addCriterion("RoomID is not null");
            return (Criteria) this;
        }

        public Criteria andRoomidEqualTo(String value) {
            addCriterion("RoomID =", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotEqualTo(String value) {
            addCriterion("RoomID <>", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidGreaterThan(String value) {
            addCriterion("RoomID >", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidGreaterThanOrEqualTo(String value) {
            addCriterion("RoomID >=", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLessThan(String value) {
            addCriterion("RoomID <", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLessThanOrEqualTo(String value) {
            addCriterion("RoomID <=", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLike(String value) {
            addCriterion("RoomID like", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotLike(String value) {
            addCriterion("RoomID not like", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidIn(List<String> values) {
            addCriterion("RoomID in", values, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotIn(List<String> values) {
            addCriterion("RoomID not in", values, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidBetween(String value1, String value2) {
            addCriterion("RoomID between", value1, value2, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotBetween(String value1, String value2) {
            addCriterion("RoomID not between", value1, value2, "roomid");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrIsNull() {
            addCriterion("RemoteAddr is null");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrIsNotNull() {
            addCriterion("RemoteAddr is not null");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrEqualTo(String value) {
            addCriterion("RemoteAddr =", value, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrNotEqualTo(String value) {
            addCriterion("RemoteAddr <>", value, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrGreaterThan(String value) {
            addCriterion("RemoteAddr >", value, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrGreaterThanOrEqualTo(String value) {
            addCriterion("RemoteAddr >=", value, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrLessThan(String value) {
            addCriterion("RemoteAddr <", value, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrLessThanOrEqualTo(String value) {
            addCriterion("RemoteAddr <=", value, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrLike(String value) {
            addCriterion("RemoteAddr like", value, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrNotLike(String value) {
            addCriterion("RemoteAddr not like", value, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrIn(List<String> values) {
            addCriterion("RemoteAddr in", values, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrNotIn(List<String> values) {
            addCriterion("RemoteAddr not in", values, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrBetween(String value1, String value2) {
            addCriterion("RemoteAddr between", value1, value2, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andRemoteaddrNotBetween(String value1, String value2) {
            addCriterion("RemoteAddr not between", value1, value2, "remoteaddr");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("Type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("Type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("Type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("Type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("Type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("Type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("Type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("Type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("Type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("Type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("Type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("Type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("Content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("Content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("Content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("Content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("Content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("Content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("Content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("Content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("Content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("Content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("Content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("Content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("Content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("Content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("CreateTime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("CreateTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(String value) {
            addCriterion("CreateTime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(String value) {
            addCriterion("CreateTime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(String value) {
            addCriterion("CreateTime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("CreateTime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(String value) {
            addCriterion("CreateTime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(String value) {
            addCriterion("CreateTime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLike(String value) {
            addCriterion("CreateTime like", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotLike(String value) {
            addCriterion("CreateTime not like", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<String> values) {
            addCriterion("CreateTime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<String> values) {
            addCriterion("CreateTime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(String value1, String value2) {
            addCriterion("CreateTime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(String value1, String value2) {
            addCriterion("CreateTime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andRemotehostIsNull() {
            addCriterion("RemoteHost is null");
            return (Criteria) this;
        }

        public Criteria andRemotehostIsNotNull() {
            addCriterion("RemoteHost is not null");
            return (Criteria) this;
        }

        public Criteria andRemotehostEqualTo(String value) {
            addCriterion("RemoteHost =", value, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemotehostNotEqualTo(String value) {
            addCriterion("RemoteHost <>", value, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemotehostGreaterThan(String value) {
            addCriterion("RemoteHost >", value, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemotehostGreaterThanOrEqualTo(String value) {
            addCriterion("RemoteHost >=", value, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemotehostLessThan(String value) {
            addCriterion("RemoteHost <", value, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemotehostLessThanOrEqualTo(String value) {
            addCriterion("RemoteHost <=", value, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemotehostLike(String value) {
            addCriterion("RemoteHost like", value, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemotehostNotLike(String value) {
            addCriterion("RemoteHost not like", value, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemotehostIn(List<String> values) {
            addCriterion("RemoteHost in", values, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemotehostNotIn(List<String> values) {
            addCriterion("RemoteHost not in", values, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemotehostBetween(String value1, String value2) {
            addCriterion("RemoteHost between", value1, value2, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemotehostNotBetween(String value1, String value2) {
            addCriterion("RemoteHost not between", value1, value2, "remotehost");
            return (Criteria) this;
        }

        public Criteria andRemoteportIsNull() {
            addCriterion("RemotePort is null");
            return (Criteria) this;
        }

        public Criteria andRemoteportIsNotNull() {
            addCriterion("RemotePort is not null");
            return (Criteria) this;
        }

        public Criteria andRemoteportEqualTo(String value) {
            addCriterion("RemotePort =", value, "remoteport");
            return (Criteria) this;
        }

        public Criteria andRemoteportNotEqualTo(String value) {
            addCriterion("RemotePort <>", value, "remoteport");
            return (Criteria) this;
        }

        public Criteria andRemoteportGreaterThan(String value) {
            addCriterion("RemotePort >", value, "remoteport");
            return (Criteria) this;
        }

        public Criteria andRemoteportGreaterThanOrEqualTo(String value) {
            addCriterion("RemotePort >=", value, "remoteport");
            return (Criteria) this;
        }

        public Criteria andRemoteportLessThan(String value) {
            addCriterion("RemotePort <", value, "remoteport");
            return (Criteria) this;
        }

        public Criteria andRemoteportLessThanOrEqualTo(String value) {
            addCriterion("RemotePort <=", value, "remoteport");
            return (Criteria) this;
        }

        public Criteria andRemoteportLike(String value) {
            addCriterion("RemotePort like", value, "remoteport");
            return (Criteria) this;
        }

        public Criteria andRemoteportNotLike(String value) {
            addCriterion("RemotePort not like", value, "remoteport");
            return (Criteria) this;
        }

        public Criteria andRemoteportIn(List<String> values) {
            addCriterion("RemotePort in", values, "remoteport");
            return (Criteria) this;
        }

        public Criteria andRemoteportNotIn(List<String> values) {
            addCriterion("RemotePort not in", values, "remoteport");
            return (Criteria) this;
        }

        public Criteria andRemoteportBetween(String value1, String value2) {
            addCriterion("RemotePort between", value1, value2, "remoteport");
            return (Criteria) this;
        }

        public Criteria andRemoteportNotBetween(String value1, String value2) {
            addCriterion("RemotePort not between", value1, value2, "remoteport");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}