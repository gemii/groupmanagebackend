package cc.gemii.po;

public class Monitorrobotbind {
    private Integer id;

    private Integer monitorid;

    private String type;

    private String robottag;

    private String monitorname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMonitorid() {
        return monitorid;
    }

    public void setMonitorid(Integer monitorid) {
        this.monitorid = monitorid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getRobottag() {
        return robottag;
    }

    public void setRobottag(String robottag) {
        this.robottag = robottag == null ? null : robottag.trim();
    }

    public String getMonitorname() {
        return monitorname;
    }

    public void setMonitorname(String monitorname) {
        this.monitorname = monitorname == null ? null : monitorname.trim();
    }
}