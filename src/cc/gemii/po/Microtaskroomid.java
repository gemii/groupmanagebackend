package cc.gemii.po;

public class Microtaskroomid {
    private Integer id;

    private Integer microtaskid;

    private String roomid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMicrotaskid() {
        return microtaskid;
    }

    public void setMicrotaskid(Integer microtaskid) {
        this.microtaskid = microtaskid;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid == null ? null : roomid.trim();
    }
}