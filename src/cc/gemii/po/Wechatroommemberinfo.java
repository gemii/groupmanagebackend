package cc.gemii.po;

public class Wechatroommemberinfo extends WechatroommemberinfoKey {
    private String uin;

    private String nickname;

    private String attrstatus;

    private String pyinitial;

    private String pyquanpin;

    private String remarkpyinitial;

    private String remarkpyquanpin;

    private String memberstatus;

    private String displayname;

    private String keyword;

    private String memberIcon;

    private String enterGroupTime;

    private String leaveGroupTime;

    private String alias;
    
    private String openId;
    
    private String u_userId;
    
    private String isLegal;

    public String getUin() {
        return uin;
    }

    public void setUin(String uin) {
        this.uin = uin == null ? null : uin.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getAttrstatus() {
        return attrstatus;
    }

    public void setAttrstatus(String attrstatus) {
        this.attrstatus = attrstatus == null ? null : attrstatus.trim();
    }

    public String getPyinitial() {
        return pyinitial;
    }

    public void setPyinitial(String pyinitial) {
        this.pyinitial = pyinitial == null ? null : pyinitial.trim();
    }

    public String getPyquanpin() {
        return pyquanpin;
    }

    public void setPyquanpin(String pyquanpin) {
        this.pyquanpin = pyquanpin == null ? null : pyquanpin.trim();
    }

    public String getRemarkpyinitial() {
        return remarkpyinitial;
    }

    public void setRemarkpyinitial(String remarkpyinitial) {
        this.remarkpyinitial = remarkpyinitial == null ? null : remarkpyinitial.trim();
    }

    public String getRemarkpyquanpin() {
        return remarkpyquanpin;
    }

    public void setRemarkpyquanpin(String remarkpyquanpin) {
        this.remarkpyquanpin = remarkpyquanpin == null ? null : remarkpyquanpin.trim();
    }

    public String getMemberstatus() {
        return memberstatus;
    }

    public void setMemberstatus(String memberstatus) {
        this.memberstatus = memberstatus == null ? null : memberstatus.trim();
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname == null ? null : displayname.trim();
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword == null ? null : keyword.trim();
    }

    public String getMemberIcon() {
        return memberIcon;
    }

    public void setMemberIcon(String memberIcon) {
        this.memberIcon = memberIcon == null ? null : memberIcon.trim();
    }

    public String getEnterGroupTime() {
        return enterGroupTime;
    }

    public void setEnterGroupTime(String enterGroupTime) {
        this.enterGroupTime = enterGroupTime == null ? null : enterGroupTime.trim();
    }

    public String getLeaveGroupTime() {
        return leaveGroupTime;
    }

    public void setLeaveGroupTime(String leaveGroupTime) {
        this.leaveGroupTime = leaveGroupTime == null ? null : leaveGroupTime.trim();
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias == null ? null : alias.trim();
    }

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getU_userId() {
		return u_userId;
	}

	public void setU_userId(String u_userId) {
		this.u_userId = u_userId;
	}

	public String getIsLegal() {
		return isLegal;
	}

	public void setIsLegal(String isLegal) {
		this.isLegal = isLegal;
	}
    
}