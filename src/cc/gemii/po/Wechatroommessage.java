package cc.gemii.po;

public class Wechatroommessage extends WechatroommessageKey {
    private Integer msgtype;

    private Integer appmsgtype;

    private String roomid;

    private String memberid;

    private String userdisplayname;

    private String usernickname;

    private Integer status;

    private String content;

    public Integer getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(Integer msgtype) {
        this.msgtype = msgtype;
    }

    public Integer getAppmsgtype() {
        return appmsgtype;
    }

    public void setAppmsgtype(Integer appmsgtype) {
        this.appmsgtype = appmsgtype;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid == null ? null : roomid.trim();
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid == null ? null : memberid.trim();
    }

    public String getUserdisplayname() {
        return userdisplayname;
    }

    public void setUserdisplayname(String userdisplayname) {
        this.userdisplayname = userdisplayname == null ? null : userdisplayname.trim();
    }

    public String getUsernickname() {
        return usernickname;
    }

    public void setUsernickname(String usernickname) {
        this.usernickname = usernickname == null ? null : usernickname.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}