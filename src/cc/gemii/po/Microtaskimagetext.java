package cc.gemii.po;


public class Microtaskimagetext {
	
	private Integer id;

    private Integer microtaskid;

    private String imagetexttitle;

    private String imagetextcover;

    private String imagetextremark;

    private String imagetexturl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMicrotaskid() {
        return microtaskid;
    }

    public void setMicrotaskid(Integer microtaskid) {
        this.microtaskid = microtaskid;
    }

    public String getImagetexttitle() {
        return imagetexttitle;
    }

    public void setImagetexttitle(String imagetexttitle) {
        this.imagetexttitle = imagetexttitle == null ? null : imagetexttitle.trim();
    }

    public String getImagetextcover() {
        return imagetextcover;
    }

    public void setImagetextcover(String imagetextcover) {
        this.imagetextcover = imagetextcover == null ? null : imagetextcover.trim();
    }

    public String getImagetextremark() {
        return imagetextremark;
    }

    public void setImagetextremark(String imagetextremark) {
        this.imagetextremark = imagetextremark == null ? null : imagetextremark.trim();
    }

    public String getImagetexturl() {
        return imagetexturl;
    }

    public void setImagetexturl(String imagetexturl) {
        this.imagetexturl = imagetexturl == null ? null : imagetexturl.trim();
    }
    
}