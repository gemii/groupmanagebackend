package cc.gemii.po;

public class Wechatrobotinfo {
    private String robottag;

    private String robotid;

    private String robotname;

    private String robotip;

    private String robotport;

    private String status;

    private String starttime;

    public String getRobottag() {
        return robottag;
    }

    public void setRobottag(String robottag) {
        this.robottag = robottag == null ? null : robottag.trim();
    }

    public String getRobotid() {
        return robotid;
    }

    public void setRobotid(String robotid) {
        this.robotid = robotid == null ? null : robotid.trim();
    }

    public String getRobotname() {
        return robotname;
    }

    public void setRobotname(String robotname) {
        this.robotname = robotname == null ? null : robotname.trim();
    }

    public String getRobotip() {
        return robotip;
    }

    public void setRobotip(String robotip) {
        this.robotip = robotip == null ? null : robotip.trim();
    }

    public String getRobotport() {
        return robotport;
    }

    public void setRobotport(String robotport) {
        this.robotport = robotport == null ? null : robotport.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime == null ? null : starttime.trim();
    }
}