package cc.gemii.po;

public class Wechatroommessagespecial extends WechatroommessagespecialKey {
    private Integer msgtype;

    private Integer appmsgtype;

    private String roomid;

    private String memberid;

    private String userdisplayname;

    private String usernickname;

    private Integer status;

    private Integer altmonitor;

    private Integer keyword;

    private Integer click;

    private String content;
    
    private String memberIcon;

    public Integer getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(Integer msgtype) {
        this.msgtype = msgtype;
    }

    public Integer getAppmsgtype() {
        return appmsgtype;
    }

    public void setAppmsgtype(Integer appmsgtype) {
        this.appmsgtype = appmsgtype;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid == null ? null : roomid.trim();
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid == null ? null : memberid.trim();
    }

    public String getUserdisplayname() {
        return userdisplayname;
    }

    public void setUserdisplayname(String userdisplayname) {
        this.userdisplayname = userdisplayname == null ? null : userdisplayname.trim();
    }

    public String getUsernickname() {
        return usernickname;
    }

    public void setUsernickname(String usernickname) {
        this.usernickname = usernickname == null ? null : usernickname.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAltmonitor() {
        return altmonitor;
    }

    public void setAltmonitor(Integer altmonitor) {
        this.altmonitor = altmonitor;
    }

    public Integer getKeyword() {
        return keyword;
    }

    public void setKeyword(Integer keyword) {
        this.keyword = keyword;
    }

    public Integer getClick() {
        return click;
    }

    public void setClick(Integer click) {
        this.click = click;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

	public String getMemberIcon() {
		return memberIcon;
	}

	public void setMemberIcon(String memberIcon) {
		this.memberIcon = memberIcon;
	}
    
}