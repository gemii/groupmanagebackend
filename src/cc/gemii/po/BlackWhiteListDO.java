package cc.gemii.po;

import java.io.Serializable;
import java.util.Date;

public class BlackWhiteListDO implements Serializable{
	
	public static enum BlackWhiteListType{
		WHITE,BLACK,UNKOWN
	}
	
	private int id;
	private String userKey;
	private String functionCode;
	private BlackWhiteListType type;
	private String remark;
	private Date createDate;
	private Date updateDate;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserKey() {
		return userKey;
	}
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}
	public String getFunctionCode() {
		return functionCode;
	}
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	
	public BlackWhiteListType getType() {
		return type;
	}
	public void setType(BlackWhiteListType type) {
		this.type = type;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}
