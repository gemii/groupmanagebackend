package cc.gemii.po;

public class MicrotaskWithBLOBs extends Microtask {
    private String activitycontent;

    private String roomids;

    public String getActivitycontent() {
        return activitycontent;
    }

    public void setActivitycontent(String activitycontent) {
        this.activitycontent = activitycontent == null ? null : activitycontent.trim();
    }

    public String getRoomids() {
        return roomids;
    }

    public void setRoomids(String roomids) {
        this.roomids = roomids == null ? null : roomids.trim();
    }
    
}