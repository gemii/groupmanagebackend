package cc.gemii.po;

import java.util.ArrayList;
import java.util.List;

public class MicrotaskExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MicrotaskExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andActivitytitleIsNull() {
            addCriterion("ActivityTitle is null");
            return (Criteria) this;
        }

        public Criteria andActivitytitleIsNotNull() {
            addCriterion("ActivityTitle is not null");
            return (Criteria) this;
        }

        public Criteria andActivitytitleEqualTo(String value) {
            addCriterion("ActivityTitle =", value, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivitytitleNotEqualTo(String value) {
            addCriterion("ActivityTitle <>", value, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivitytitleGreaterThan(String value) {
            addCriterion("ActivityTitle >", value, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivitytitleGreaterThanOrEqualTo(String value) {
            addCriterion("ActivityTitle >=", value, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivitytitleLessThan(String value) {
            addCriterion("ActivityTitle <", value, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivitytitleLessThanOrEqualTo(String value) {
            addCriterion("ActivityTitle <=", value, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivitytitleLike(String value) {
            addCriterion("ActivityTitle like", value, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivitytitleNotLike(String value) {
            addCriterion("ActivityTitle not like", value, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivitytitleIn(List<String> values) {
            addCriterion("ActivityTitle in", values, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivitytitleNotIn(List<String> values) {
            addCriterion("ActivityTitle not in", values, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivitytitleBetween(String value1, String value2) {
            addCriterion("ActivityTitle between", value1, value2, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivitytitleNotBetween(String value1, String value2) {
            addCriterion("ActivityTitle not between", value1, value2, "activitytitle");
            return (Criteria) this;
        }

        public Criteria andActivityimageIsNull() {
            addCriterion("ActivityImage is null");
            return (Criteria) this;
        }

        public Criteria andActivityimageIsNotNull() {
            addCriterion("ActivityImage is not null");
            return (Criteria) this;
        }

        public Criteria andActivityimageEqualTo(String value) {
            addCriterion("ActivityImage =", value, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityimageNotEqualTo(String value) {
            addCriterion("ActivityImage <>", value, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityimageGreaterThan(String value) {
            addCriterion("ActivityImage >", value, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityimageGreaterThanOrEqualTo(String value) {
            addCriterion("ActivityImage >=", value, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityimageLessThan(String value) {
            addCriterion("ActivityImage <", value, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityimageLessThanOrEqualTo(String value) {
            addCriterion("ActivityImage <=", value, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityimageLike(String value) {
            addCriterion("ActivityImage like", value, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityimageNotLike(String value) {
            addCriterion("ActivityImage not like", value, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityimageIn(List<String> values) {
            addCriterion("ActivityImage in", values, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityimageNotIn(List<String> values) {
            addCriterion("ActivityImage not in", values, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityimageBetween(String value1, String value2) {
            addCriterion("ActivityImage between", value1, value2, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityimageNotBetween(String value1, String value2) {
            addCriterion("ActivityImage not between", value1, value2, "activityimage");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartIsNull() {
            addCriterion("ActivityShowStart is null");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartIsNotNull() {
            addCriterion("ActivityShowStart is not null");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartEqualTo(String value) {
            addCriterion("ActivityShowStart =", value, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartNotEqualTo(String value) {
            addCriterion("ActivityShowStart <>", value, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartGreaterThan(String value) {
            addCriterion("ActivityShowStart >", value, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartGreaterThanOrEqualTo(String value) {
            addCriterion("ActivityShowStart >=", value, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartLessThan(String value) {
            addCriterion("ActivityShowStart <", value, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartLessThanOrEqualTo(String value) {
            addCriterion("ActivityShowStart <=", value, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartLike(String value) {
            addCriterion("ActivityShowStart like", value, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartNotLike(String value) {
            addCriterion("ActivityShowStart not like", value, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartIn(List<String> values) {
            addCriterion("ActivityShowStart in", values, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartNotIn(List<String> values) {
            addCriterion("ActivityShowStart not in", values, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartBetween(String value1, String value2) {
            addCriterion("ActivityShowStart between", value1, value2, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowstartNotBetween(String value1, String value2) {
            addCriterion("ActivityShowStart not between", value1, value2, "activityshowstart");
            return (Criteria) this;
        }

        public Criteria andActivityshowendIsNull() {
            addCriterion("ActivityShowEnd is null");
            return (Criteria) this;
        }

        public Criteria andActivityshowendIsNotNull() {
            addCriterion("ActivityShowEnd is not null");
            return (Criteria) this;
        }

        public Criteria andActivityshowendEqualTo(String value) {
            addCriterion("ActivityShowEnd =", value, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andActivityshowendNotEqualTo(String value) {
            addCriterion("ActivityShowEnd <>", value, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andActivityshowendGreaterThan(String value) {
            addCriterion("ActivityShowEnd >", value, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andActivityshowendGreaterThanOrEqualTo(String value) {
            addCriterion("ActivityShowEnd >=", value, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andActivityshowendLessThan(String value) {
            addCriterion("ActivityShowEnd <", value, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andActivityshowendLessThanOrEqualTo(String value) {
            addCriterion("ActivityShowEnd <=", value, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andActivityshowendLike(String value) {
            addCriterion("ActivityShowEnd like", value, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andActivityshowendNotLike(String value) {
            addCriterion("ActivityShowEnd not like", value, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andActivityshowendIn(List<String> values) {
            addCriterion("ActivityShowEnd in", values, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andActivityshowendNotIn(List<String> values) {
            addCriterion("ActivityShowEnd not in", values, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andActivityshowendBetween(String value1, String value2) {
            addCriterion("ActivityShowEnd between", value1, value2, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andActivityshowendNotBetween(String value1, String value2) {
            addCriterion("ActivityShowEnd not between", value1, value2, "activityshowend");
            return (Criteria) this;
        }

        public Criteria andPushstartIsNull() {
            addCriterion("PushStart is null");
            return (Criteria) this;
        }

        public Criteria andPushstartIsNotNull() {
            addCriterion("PushStart is not null");
            return (Criteria) this;
        }

        public Criteria andPushstartEqualTo(String value) {
            addCriterion("PushStart =", value, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushstartNotEqualTo(String value) {
            addCriterion("PushStart <>", value, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushstartGreaterThan(String value) {
            addCriterion("PushStart >", value, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushstartGreaterThanOrEqualTo(String value) {
            addCriterion("PushStart >=", value, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushstartLessThan(String value) {
            addCriterion("PushStart <", value, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushstartLessThanOrEqualTo(String value) {
            addCriterion("PushStart <=", value, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushstartLike(String value) {
            addCriterion("PushStart like", value, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushstartNotLike(String value) {
            addCriterion("PushStart not like", value, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushstartIn(List<String> values) {
            addCriterion("PushStart in", values, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushstartNotIn(List<String> values) {
            addCriterion("PushStart not in", values, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushstartBetween(String value1, String value2) {
            addCriterion("PushStart between", value1, value2, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushstartNotBetween(String value1, String value2) {
            addCriterion("PushStart not between", value1, value2, "pushstart");
            return (Criteria) this;
        }

        public Criteria andPushendIsNull() {
            addCriterion("PushEnd is null");
            return (Criteria) this;
        }

        public Criteria andPushendIsNotNull() {
            addCriterion("PushEnd is not null");
            return (Criteria) this;
        }

        public Criteria andPushendEqualTo(String value) {
            addCriterion("PushEnd =", value, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushendNotEqualTo(String value) {
            addCriterion("PushEnd <>", value, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushendGreaterThan(String value) {
            addCriterion("PushEnd >", value, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushendGreaterThanOrEqualTo(String value) {
            addCriterion("PushEnd >=", value, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushendLessThan(String value) {
            addCriterion("PushEnd <", value, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushendLessThanOrEqualTo(String value) {
            addCriterion("PushEnd <=", value, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushendLike(String value) {
            addCriterion("PushEnd like", value, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushendNotLike(String value) {
            addCriterion("PushEnd not like", value, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushendIn(List<String> values) {
            addCriterion("PushEnd in", values, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushendNotIn(List<String> values) {
            addCriterion("PushEnd not in", values, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushendBetween(String value1, String value2) {
            addCriterion("PushEnd between", value1, value2, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushendNotBetween(String value1, String value2) {
            addCriterion("PushEnd not between", value1, value2, "pushend");
            return (Criteria) this;
        }

        public Criteria andPushtimeIsNull() {
            addCriterion("PushTime is null");
            return (Criteria) this;
        }

        public Criteria andPushtimeIsNotNull() {
            addCriterion("PushTime is not null");
            return (Criteria) this;
        }

        public Criteria andPushtimeEqualTo(String value) {
            addCriterion("PushTime =", value, "pushtime");
            return (Criteria) this;
        }

        public Criteria andPushtimeNotEqualTo(String value) {
            addCriterion("PushTime <>", value, "pushtime");
            return (Criteria) this;
        }

        public Criteria andPushtimeGreaterThan(String value) {
            addCriterion("PushTime >", value, "pushtime");
            return (Criteria) this;
        }

        public Criteria andPushtimeGreaterThanOrEqualTo(String value) {
            addCriterion("PushTime >=", value, "pushtime");
            return (Criteria) this;
        }

        public Criteria andPushtimeLessThan(String value) {
            addCriterion("PushTime <", value, "pushtime");
            return (Criteria) this;
        }

        public Criteria andPushtimeLessThanOrEqualTo(String value) {
            addCriterion("PushTime <=", value, "pushtime");
            return (Criteria) this;
        }

        public Criteria andPushtimeLike(String value) {
            addCriterion("PushTime like", value, "pushtime");
            return (Criteria) this;
        }

        public Criteria andPushtimeNotLike(String value) {
            addCriterion("PushTime not like", value, "pushtime");
            return (Criteria) this;
        }

        public Criteria andPushtimeIn(List<String> values) {
            addCriterion("PushTime in", values, "pushtime");
            return (Criteria) this;
        }

        public Criteria andPushtimeNotIn(List<String> values) {
            addCriterion("PushTime not in", values, "pushtime");
            return (Criteria) this;
        }

        public Criteria andPushtimeBetween(String value1, String value2) {
            addCriterion("PushTime between", value1, value2, "pushtime");
            return (Criteria) this;
        }

        public Criteria andPushtimeNotBetween(String value1, String value2) {
            addCriterion("PushTime not between", value1, value2, "pushtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("CreateTime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("CreateTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(String value) {
            addCriterion("CreateTime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(String value) {
            addCriterion("CreateTime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(String value) {
            addCriterion("CreateTime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("CreateTime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(String value) {
            addCriterion("CreateTime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(String value) {
            addCriterion("CreateTime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLike(String value) {
            addCriterion("CreateTime like", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotLike(String value) {
            addCriterion("CreateTime not like", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<String> values) {
            addCriterion("CreateTime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<String> values) {
            addCriterion("CreateTime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(String value1, String value2) {
            addCriterion("CreateTime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(String value1, String value2) {
            addCriterion("CreateTime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeIsNull() {
            addCriterion("LastUpdateTime is null");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeIsNotNull() {
            addCriterion("LastUpdateTime is not null");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeEqualTo(String value) {
            addCriterion("LastUpdateTime =", value, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeNotEqualTo(String value) {
            addCriterion("LastUpdateTime <>", value, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeGreaterThan(String value) {
            addCriterion("LastUpdateTime >", value, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("LastUpdateTime >=", value, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeLessThan(String value) {
            addCriterion("LastUpdateTime <", value, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeLessThanOrEqualTo(String value) {
            addCriterion("LastUpdateTime <=", value, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeLike(String value) {
            addCriterion("LastUpdateTime like", value, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeNotLike(String value) {
            addCriterion("LastUpdateTime not like", value, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeIn(List<String> values) {
            addCriterion("LastUpdateTime in", values, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeNotIn(List<String> values) {
            addCriterion("LastUpdateTime not in", values, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeBetween(String value1, String value2) {
            addCriterion("LastUpdateTime between", value1, value2, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andLastupdatetimeNotBetween(String value1, String value2) {
            addCriterion("LastUpdateTime not between", value1, value2, "lastupdatetime");
            return (Criteria) this;
        }

        public Criteria andEnableIsNull() {
            addCriterion("Enable is null");
            return (Criteria) this;
        }

        public Criteria andEnableIsNotNull() {
            addCriterion("Enable is not null");
            return (Criteria) this;
        }

        public Criteria andEnableEqualTo(Integer value) {
            addCriterion("Enable =", value, "enable");
            return (Criteria) this;
        }

        public Criteria andEnableNotEqualTo(Integer value) {
            addCriterion("Enable <>", value, "enable");
            return (Criteria) this;
        }

        public Criteria andEnableGreaterThan(Integer value) {
            addCriterion("Enable >", value, "enable");
            return (Criteria) this;
        }

        public Criteria andEnableGreaterThanOrEqualTo(Integer value) {
            addCriterion("Enable >=", value, "enable");
            return (Criteria) this;
        }

        public Criteria andEnableLessThan(Integer value) {
            addCriterion("Enable <", value, "enable");
            return (Criteria) this;
        }

        public Criteria andEnableLessThanOrEqualTo(Integer value) {
            addCriterion("Enable <=", value, "enable");
            return (Criteria) this;
        }

        public Criteria andEnableIn(List<Integer> values) {
            addCriterion("Enable in", values, "enable");
            return (Criteria) this;
        }

        public Criteria andEnableNotIn(List<Integer> values) {
            addCriterion("Enable not in", values, "enable");
            return (Criteria) this;
        }

        public Criteria andEnableBetween(Integer value1, Integer value2) {
            addCriterion("Enable between", value1, value2, "enable");
            return (Criteria) this;
        }

        public Criteria andEnableNotBetween(Integer value1, Integer value2) {
            addCriterion("Enable not between", value1, value2, "enable");
            return (Criteria) this;
        }

        public Criteria andIsdeleteIsNull() {
            addCriterion("isDelete is null");
            return (Criteria) this;
        }

        public Criteria andIsdeleteIsNotNull() {
            addCriterion("isDelete is not null");
            return (Criteria) this;
        }

        public Criteria andIsdeleteEqualTo(Integer value) {
            addCriterion("isDelete =", value, "isdelete");
            return (Criteria) this;
        }

        public Criteria andIsdeleteNotEqualTo(Integer value) {
            addCriterion("isDelete <>", value, "isdelete");
            return (Criteria) this;
        }

        public Criteria andIsdeleteGreaterThan(Integer value) {
            addCriterion("isDelete >", value, "isdelete");
            return (Criteria) this;
        }

        public Criteria andIsdeleteGreaterThanOrEqualTo(Integer value) {
            addCriterion("isDelete >=", value, "isdelete");
            return (Criteria) this;
        }

        public Criteria andIsdeleteLessThan(Integer value) {
            addCriterion("isDelete <", value, "isdelete");
            return (Criteria) this;
        }

        public Criteria andIsdeleteLessThanOrEqualTo(Integer value) {
            addCriterion("isDelete <=", value, "isdelete");
            return (Criteria) this;
        }

        public Criteria andIsdeleteIn(List<Integer> values) {
            addCriterion("isDelete in", values, "isdelete");
            return (Criteria) this;
        }

        public Criteria andIsdeleteNotIn(List<Integer> values) {
            addCriterion("isDelete not in", values, "isdelete");
            return (Criteria) this;
        }

        public Criteria andIsdeleteBetween(Integer value1, Integer value2) {
            addCriterion("isDelete between", value1, value2, "isdelete");
            return (Criteria) this;
        }

        public Criteria andIsdeleteNotBetween(Integer value1, Integer value2) {
            addCriterion("isDelete not between", value1, value2, "isdelete");
            return (Criteria) this;
        }

        public Criteria andBelongIsNull() {
            addCriterion("Belong is null");
            return (Criteria) this;
        }

        public Criteria andBelongIsNotNull() {
            addCriterion("Belong is not null");
            return (Criteria) this;
        }

        public Criteria andBelongEqualTo(Integer value) {
            addCriterion("Belong =", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongNotEqualTo(Integer value) {
            addCriterion("Belong <>", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongGreaterThan(Integer value) {
            addCriterion("Belong >", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongGreaterThanOrEqualTo(Integer value) {
            addCriterion("Belong >=", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongLessThan(Integer value) {
            addCriterion("Belong <", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongLessThanOrEqualTo(Integer value) {
            addCriterion("Belong <=", value, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongIn(List<Integer> values) {
            addCriterion("Belong in", values, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongNotIn(List<Integer> values) {
            addCriterion("Belong not in", values, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongBetween(Integer value1, Integer value2) {
            addCriterion("Belong between", value1, value2, "belong");
            return (Criteria) this;
        }

        public Criteria andBelongNotBetween(Integer value1, Integer value2) {
            addCriterion("Belong not between", value1, value2, "belong");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}