package cc.gemii.po;

import java.util.ArrayList;
import java.util.List;

public class WechatrobotinfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WechatrobotinfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRobottagIsNull() {
            addCriterion("RobotTag is null");
            return (Criteria) this;
        }

        public Criteria andRobottagIsNotNull() {
            addCriterion("RobotTag is not null");
            return (Criteria) this;
        }

        public Criteria andRobottagEqualTo(String value) {
            addCriterion("RobotTag =", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagNotEqualTo(String value) {
            addCriterion("RobotTag <>", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagGreaterThan(String value) {
            addCriterion("RobotTag >", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagGreaterThanOrEqualTo(String value) {
            addCriterion("RobotTag >=", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagLessThan(String value) {
            addCriterion("RobotTag <", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagLessThanOrEqualTo(String value) {
            addCriterion("RobotTag <=", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagLike(String value) {
            addCriterion("RobotTag like", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagNotLike(String value) {
            addCriterion("RobotTag not like", value, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagIn(List<String> values) {
            addCriterion("RobotTag in", values, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagNotIn(List<String> values) {
            addCriterion("RobotTag not in", values, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagBetween(String value1, String value2) {
            addCriterion("RobotTag between", value1, value2, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobottagNotBetween(String value1, String value2) {
            addCriterion("RobotTag not between", value1, value2, "robottag");
            return (Criteria) this;
        }

        public Criteria andRobotidIsNull() {
            addCriterion("RobotID is null");
            return (Criteria) this;
        }

        public Criteria andRobotidIsNotNull() {
            addCriterion("RobotID is not null");
            return (Criteria) this;
        }

        public Criteria andRobotidEqualTo(String value) {
            addCriterion("RobotID =", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidNotEqualTo(String value) {
            addCriterion("RobotID <>", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidGreaterThan(String value) {
            addCriterion("RobotID >", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidGreaterThanOrEqualTo(String value) {
            addCriterion("RobotID >=", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidLessThan(String value) {
            addCriterion("RobotID <", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidLessThanOrEqualTo(String value) {
            addCriterion("RobotID <=", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidLike(String value) {
            addCriterion("RobotID like", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidNotLike(String value) {
            addCriterion("RobotID not like", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidIn(List<String> values) {
            addCriterion("RobotID in", values, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidNotIn(List<String> values) {
            addCriterion("RobotID not in", values, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidBetween(String value1, String value2) {
            addCriterion("RobotID between", value1, value2, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidNotBetween(String value1, String value2) {
            addCriterion("RobotID not between", value1, value2, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotnameIsNull() {
            addCriterion("RobotName is null");
            return (Criteria) this;
        }

        public Criteria andRobotnameIsNotNull() {
            addCriterion("RobotName is not null");
            return (Criteria) this;
        }

        public Criteria andRobotnameEqualTo(String value) {
            addCriterion("RobotName =", value, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotnameNotEqualTo(String value) {
            addCriterion("RobotName <>", value, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotnameGreaterThan(String value) {
            addCriterion("RobotName >", value, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotnameGreaterThanOrEqualTo(String value) {
            addCriterion("RobotName >=", value, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotnameLessThan(String value) {
            addCriterion("RobotName <", value, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotnameLessThanOrEqualTo(String value) {
            addCriterion("RobotName <=", value, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotnameLike(String value) {
            addCriterion("RobotName like", value, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotnameNotLike(String value) {
            addCriterion("RobotName not like", value, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotnameIn(List<String> values) {
            addCriterion("RobotName in", values, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotnameNotIn(List<String> values) {
            addCriterion("RobotName not in", values, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotnameBetween(String value1, String value2) {
            addCriterion("RobotName between", value1, value2, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotnameNotBetween(String value1, String value2) {
            addCriterion("RobotName not between", value1, value2, "robotname");
            return (Criteria) this;
        }

        public Criteria andRobotipIsNull() {
            addCriterion("RobotIP is null");
            return (Criteria) this;
        }

        public Criteria andRobotipIsNotNull() {
            addCriterion("RobotIP is not null");
            return (Criteria) this;
        }

        public Criteria andRobotipEqualTo(String value) {
            addCriterion("RobotIP =", value, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotipNotEqualTo(String value) {
            addCriterion("RobotIP <>", value, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotipGreaterThan(String value) {
            addCriterion("RobotIP >", value, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotipGreaterThanOrEqualTo(String value) {
            addCriterion("RobotIP >=", value, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotipLessThan(String value) {
            addCriterion("RobotIP <", value, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotipLessThanOrEqualTo(String value) {
            addCriterion("RobotIP <=", value, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotipLike(String value) {
            addCriterion("RobotIP like", value, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotipNotLike(String value) {
            addCriterion("RobotIP not like", value, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotipIn(List<String> values) {
            addCriterion("RobotIP in", values, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotipNotIn(List<String> values) {
            addCriterion("RobotIP not in", values, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotipBetween(String value1, String value2) {
            addCriterion("RobotIP between", value1, value2, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotipNotBetween(String value1, String value2) {
            addCriterion("RobotIP not between", value1, value2, "robotip");
            return (Criteria) this;
        }

        public Criteria andRobotportIsNull() {
            addCriterion("RobotPort is null");
            return (Criteria) this;
        }

        public Criteria andRobotportIsNotNull() {
            addCriterion("RobotPort is not null");
            return (Criteria) this;
        }

        public Criteria andRobotportEqualTo(String value) {
            addCriterion("RobotPort =", value, "robotport");
            return (Criteria) this;
        }

        public Criteria andRobotportNotEqualTo(String value) {
            addCriterion("RobotPort <>", value, "robotport");
            return (Criteria) this;
        }

        public Criteria andRobotportGreaterThan(String value) {
            addCriterion("RobotPort >", value, "robotport");
            return (Criteria) this;
        }

        public Criteria andRobotportGreaterThanOrEqualTo(String value) {
            addCriterion("RobotPort >=", value, "robotport");
            return (Criteria) this;
        }

        public Criteria andRobotportLessThan(String value) {
            addCriterion("RobotPort <", value, "robotport");
            return (Criteria) this;
        }

        public Criteria andRobotportLessThanOrEqualTo(String value) {
            addCriterion("RobotPort <=", value, "robotport");
            return (Criteria) this;
        }

        public Criteria andRobotportLike(String value) {
            addCriterion("RobotPort like", value, "robotport");
            return (Criteria) this;
        }

        public Criteria andRobotportNotLike(String value) {
            addCriterion("RobotPort not like", value, "robotport");
            return (Criteria) this;
        }

        public Criteria andRobotportIn(List<String> values) {
            addCriterion("RobotPort in", values, "robotport");
            return (Criteria) this;
        }

        public Criteria andRobotportNotIn(List<String> values) {
            addCriterion("RobotPort not in", values, "robotport");
            return (Criteria) this;
        }

        public Criteria andRobotportBetween(String value1, String value2) {
            addCriterion("RobotPort between", value1, value2, "robotport");
            return (Criteria) this;
        }

        public Criteria andRobotportNotBetween(String value1, String value2) {
            addCriterion("RobotPort not between", value1, value2, "robotport");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("Status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("Status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNull() {
            addCriterion("StartTime is null");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNotNull() {
            addCriterion("StartTime is not null");
            return (Criteria) this;
        }

        public Criteria andStarttimeEqualTo(String value) {
            addCriterion("StartTime =", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotEqualTo(String value) {
            addCriterion("StartTime <>", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThan(String value) {
            addCriterion("StartTime >", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThanOrEqualTo(String value) {
            addCriterion("StartTime >=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThan(String value) {
            addCriterion("StartTime <", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThanOrEqualTo(String value) {
            addCriterion("StartTime <=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLike(String value) {
            addCriterion("StartTime like", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotLike(String value) {
            addCriterion("StartTime not like", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeIn(List<String> values) {
            addCriterion("StartTime in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotIn(List<String> values) {
            addCriterion("StartTime not in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeBetween(String value1, String value2) {
            addCriterion("StartTime between", value1, value2, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotBetween(String value1, String value2) {
            addCriterion("StartTime not between", value1, value2, "starttime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}