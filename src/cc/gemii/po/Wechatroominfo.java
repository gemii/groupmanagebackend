package cc.gemii.po;

public class Wechatroominfo {
    private String roomid;

    private String roomname;

    private String city;

    private String area;

    private String province;

    private String department;

    private String subname;

    private String channel;

    private String alias;

    private String groupattr;

    private String hospitalcode;

    private String hospitalname;

    private String uptime;

    private String starttime;

    private String endtime;

    private Integer monitorcount;

    private Integer helpercount;

    private Integer robotcount;

    private Integer whitelist;

    private Integer komcount;

    private Integer addcount;

    private Integer backcount;

    private Integer currentcount;

    private Integer totalcount;

    private Integer passcount;

    private String nowroomname;
    
    private String owner;
    
    private String status;
    
    private String innerid;
    
    private String grouptag;
    
    private String taskid;
    
    private String createdate;
    
    public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

	public String getTaskid() {
		return taskid;
	}

	public void setTaskid(String taskid) {
		this.taskid = taskid;
	}

	public String getGrouptag() {
		return grouptag;
	}

	public void setGrouptag(String grouptag) {
		this.grouptag = grouptag;
	}

	public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid == null ? null : roomid.trim();
    }

    public String getRoomname() {
        return roomname;
    }

    public void setRoomname(String roomname) {
        this.roomname = roomname == null ? null : roomname.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department == null ? null : department.trim();
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname == null ? null : subname.trim();
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel == null ? null : channel.trim();
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias == null ? null : alias.trim();
    }

    public String getGroupattr() {
        return groupattr;
    }

    public void setGroupattr(String groupattr) {
        this.groupattr = groupattr == null ? null : groupattr.trim();
    }

    public String getHospitalcode() {
        return hospitalcode;
    }

    public void setHospitalcode(String hospitalcode) {
        this.hospitalcode = hospitalcode == null ? null : hospitalcode.trim();
    }

    public String getHospitalname() {
        return hospitalname;
    }

    public void setHospitalname(String hospitalname) {
        this.hospitalname = hospitalname == null ? null : hospitalname.trim();
    }

    public String getUptime() {
        return uptime;
    }

    public void setUptime(String uptime) {
        this.uptime = uptime == null ? null : uptime.trim();
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime == null ? null : starttime.trim();
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime == null ? null : endtime.trim();
    }

    public Integer getMonitorcount() {
        return monitorcount;
    }

    public void setMonitorcount(Integer monitorcount) {
        this.monitorcount = monitorcount;
    }

    public Integer getHelpercount() {
        return helpercount;
    }

    public void setHelpercount(Integer helpercount) {
        this.helpercount = helpercount;
    }

    public Integer getRobotcount() {
        return robotcount;
    }

    public void setRobotcount(Integer robotcount) {
        this.robotcount = robotcount;
    }

    public Integer getWhitelist() {
        return whitelist;
    }

    public void setWhitelist(Integer whitelist) {
        this.whitelist = whitelist;
    }

    public Integer getKomcount() {
        return komcount;
    }

    public void setKomcount(Integer komcount) {
        this.komcount = komcount;
    }

    public Integer getAddcount() {
        return addcount;
    }

    public void setAddcount(Integer addcount) {
        this.addcount = addcount;
    }

    public Integer getBackcount() {
        return backcount;
    }

    public void setBackcount(Integer backcount) {
        this.backcount = backcount;
    }

    public Integer getCurrentcount() {
        return currentcount;
    }

    public void setCurrentcount(Integer currentcount) {
        this.currentcount = currentcount;
    }

    public Integer getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(Integer totalcount) {
        this.totalcount = totalcount;
    }

    public Integer getPasscount() {
        return passcount;
    }

    public void setPasscount(Integer passcount) {
        this.passcount = passcount;
    }

    public String getNowroomname() {
        return nowroomname;
    }

    public void setNowroomname(String nowroomname) {
        this.nowroomname = nowroomname == null ? null : nowroomname.trim();
    }

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInnerid() {
		return innerid;
	}

	public void setInnerid(String innerid) {
		this.innerid = innerid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addcount == null) ? 0 : addcount.hashCode());
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((backcount == null) ? 0 : backcount.hashCode());
		result = prime * result + ((channel == null) ? 0 : channel.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((currentcount == null) ? 0 : currentcount.hashCode());
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + ((endtime == null) ? 0 : endtime.hashCode());
		result = prime * result + ((groupattr == null) ? 0 : groupattr.hashCode());
		result = prime * result + ((grouptag == null) ? 0 : grouptag.hashCode());
		result = prime * result + ((helpercount == null) ? 0 : helpercount.hashCode());
		result = prime * result + ((hospitalcode == null) ? 0 : hospitalcode.hashCode());
		result = prime * result + ((hospitalname == null) ? 0 : hospitalname.hashCode());
		result = prime * result + ((innerid == null) ? 0 : innerid.hashCode());
		result = prime * result + ((komcount == null) ? 0 : komcount.hashCode());
		result = prime * result + ((monitorcount == null) ? 0 : monitorcount.hashCode());
		result = prime * result + ((nowroomname == null) ? 0 : nowroomname.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + ((passcount == null) ? 0 : passcount.hashCode());
		result = prime * result + ((province == null) ? 0 : province.hashCode());
		result = prime * result + ((robotcount == null) ? 0 : robotcount.hashCode());
		result = prime * result + ((roomid == null) ? 0 : roomid.hashCode());
		result = prime * result + ((roomname == null) ? 0 : roomname.hashCode());
		result = prime * result + ((starttime == null) ? 0 : starttime.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((subname == null) ? 0 : subname.hashCode());
		result = prime * result + ((totalcount == null) ? 0 : totalcount.hashCode());
		result = prime * result + ((uptime == null) ? 0 : uptime.hashCode());
		result = prime * result + ((whitelist == null) ? 0 : whitelist.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Wechatroominfo other = (Wechatroominfo) obj;
		if (addcount == null) {
			if (other.addcount != null)
				return false;
		} else if (!addcount.equals(other.addcount))
			return false;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.equals(other.area))
			return false;
		if (backcount == null) {
			if (other.backcount != null)
				return false;
		} else if (!backcount.equals(other.backcount))
			return false;
		if (channel == null) {
			if (other.channel != null)
				return false;
		} else if (!channel.equals(other.channel))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (currentcount == null) {
			if (other.currentcount != null)
				return false;
		} else if (!currentcount.equals(other.currentcount))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (endtime == null) {
			if (other.endtime != null)
				return false;
		} else if (!endtime.equals(other.endtime))
			return false;
		if (groupattr == null) {
			if (other.groupattr != null)
				return false;
		} else if (!groupattr.equals(other.groupattr))
			return false;
		if (grouptag == null) {
			if (other.grouptag != null)
				return false;
		} else if (!grouptag.equals(other.grouptag))
			return false;
		if (helpercount == null) {
			if (other.helpercount != null)
				return false;
		} else if (!helpercount.equals(other.helpercount))
			return false;
		if (hospitalcode == null) {
			if (other.hospitalcode != null)
				return false;
		} else if (!hospitalcode.equals(other.hospitalcode))
			return false;
		if (hospitalname == null) {
			if (other.hospitalname != null)
				return false;
		} else if (!hospitalname.equals(other.hospitalname))
			return false;
		if (innerid == null) {
			if (other.innerid != null)
				return false;
		} else if (!innerid.equals(other.innerid))
			return false;
		if (komcount == null) {
			if (other.komcount != null)
				return false;
		} else if (!komcount.equals(other.komcount))
			return false;
		if (monitorcount == null) {
			if (other.monitorcount != null)
				return false;
		} else if (!monitorcount.equals(other.monitorcount))
			return false;
		if (nowroomname == null) {
			if (other.nowroomname != null)
				return false;
		} else if (!nowroomname.equals(other.nowroomname))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (passcount == null) {
			if (other.passcount != null)
				return false;
		} else if (!passcount.equals(other.passcount))
			return false;
		if (province == null) {
			if (other.province != null)
				return false;
		} else if (!province.equals(other.province))
			return false;
		if (robotcount == null) {
			if (other.robotcount != null)
				return false;
		} else if (!robotcount.equals(other.robotcount))
			return false;
		if (roomid == null) {
			if (other.roomid != null)
				return false;
		} else if (!roomid.equals(other.roomid))
			return false;
		if (roomname == null) {
			if (other.roomname != null)
				return false;
		} else if (!roomname.equals(other.roomname))
			return false;
		if (starttime == null) {
			if (other.starttime != null)
				return false;
		} else if (!starttime.equals(other.starttime))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (subname == null) {
			if (other.subname != null)
				return false;
		} else if (!subname.equals(other.subname))
			return false;
		if (totalcount == null) {
			if (other.totalcount != null)
				return false;
		} else if (!totalcount.equals(other.totalcount))
			return false;
		if (uptime == null) {
			if (other.uptime != null)
				return false;
		} else if (!uptime.equals(other.uptime))
			return false;
		if (whitelist == null) {
			if (other.whitelist != null)
				return false;
		} else if (!whitelist.equals(other.whitelist))
			return false;
		return true;
	}
	
    
}