package cc.gemii.po;

import java.util.ArrayList;
import java.util.List;

public class MicrotaskimagetextExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MicrotaskimagetextExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidIsNull() {
            addCriterion("MicroTaskID is null");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidIsNotNull() {
            addCriterion("MicroTaskID is not null");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidEqualTo(Integer value) {
            addCriterion("MicroTaskID =", value, "microtaskid");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidNotEqualTo(Integer value) {
            addCriterion("MicroTaskID <>", value, "microtaskid");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidGreaterThan(Integer value) {
            addCriterion("MicroTaskID >", value, "microtaskid");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidGreaterThanOrEqualTo(Integer value) {
            addCriterion("MicroTaskID >=", value, "microtaskid");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidLessThan(Integer value) {
            addCriterion("MicroTaskID <", value, "microtaskid");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidLessThanOrEqualTo(Integer value) {
            addCriterion("MicroTaskID <=", value, "microtaskid");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidIn(List<Integer> values) {
            addCriterion("MicroTaskID in", values, "microtaskid");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidNotIn(List<Integer> values) {
            addCriterion("MicroTaskID not in", values, "microtaskid");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidBetween(Integer value1, Integer value2) {
            addCriterion("MicroTaskID between", value1, value2, "microtaskid");
            return (Criteria) this;
        }

        public Criteria andMicrotaskidNotBetween(Integer value1, Integer value2) {
            addCriterion("MicroTaskID not between", value1, value2, "microtaskid");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleIsNull() {
            addCriterion("ImageTextTitle is null");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleIsNotNull() {
            addCriterion("ImageTextTitle is not null");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleEqualTo(String value) {
            addCriterion("ImageTextTitle =", value, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleNotEqualTo(String value) {
            addCriterion("ImageTextTitle <>", value, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleGreaterThan(String value) {
            addCriterion("ImageTextTitle >", value, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleGreaterThanOrEqualTo(String value) {
            addCriterion("ImageTextTitle >=", value, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleLessThan(String value) {
            addCriterion("ImageTextTitle <", value, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleLessThanOrEqualTo(String value) {
            addCriterion("ImageTextTitle <=", value, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleLike(String value) {
            addCriterion("ImageTextTitle like", value, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleNotLike(String value) {
            addCriterion("ImageTextTitle not like", value, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleIn(List<String> values) {
            addCriterion("ImageTextTitle in", values, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleNotIn(List<String> values) {
            addCriterion("ImageTextTitle not in", values, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleBetween(String value1, String value2) {
            addCriterion("ImageTextTitle between", value1, value2, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetexttitleNotBetween(String value1, String value2) {
            addCriterion("ImageTextTitle not between", value1, value2, "imagetexttitle");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverIsNull() {
            addCriterion("ImageTextCover is null");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverIsNotNull() {
            addCriterion("ImageTextCover is not null");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverEqualTo(String value) {
            addCriterion("ImageTextCover =", value, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverNotEqualTo(String value) {
            addCriterion("ImageTextCover <>", value, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverGreaterThan(String value) {
            addCriterion("ImageTextCover >", value, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverGreaterThanOrEqualTo(String value) {
            addCriterion("ImageTextCover >=", value, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverLessThan(String value) {
            addCriterion("ImageTextCover <", value, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverLessThanOrEqualTo(String value) {
            addCriterion("ImageTextCover <=", value, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverLike(String value) {
            addCriterion("ImageTextCover like", value, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverNotLike(String value) {
            addCriterion("ImageTextCover not like", value, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverIn(List<String> values) {
            addCriterion("ImageTextCover in", values, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverNotIn(List<String> values) {
            addCriterion("ImageTextCover not in", values, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverBetween(String value1, String value2) {
            addCriterion("ImageTextCover between", value1, value2, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextcoverNotBetween(String value1, String value2) {
            addCriterion("ImageTextCover not between", value1, value2, "imagetextcover");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkIsNull() {
            addCriterion("ImageTextRemark is null");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkIsNotNull() {
            addCriterion("ImageTextRemark is not null");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkEqualTo(String value) {
            addCriterion("ImageTextRemark =", value, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkNotEqualTo(String value) {
            addCriterion("ImageTextRemark <>", value, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkGreaterThan(String value) {
            addCriterion("ImageTextRemark >", value, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkGreaterThanOrEqualTo(String value) {
            addCriterion("ImageTextRemark >=", value, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkLessThan(String value) {
            addCriterion("ImageTextRemark <", value, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkLessThanOrEqualTo(String value) {
            addCriterion("ImageTextRemark <=", value, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkLike(String value) {
            addCriterion("ImageTextRemark like", value, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkNotLike(String value) {
            addCriterion("ImageTextRemark not like", value, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkIn(List<String> values) {
            addCriterion("ImageTextRemark in", values, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkNotIn(List<String> values) {
            addCriterion("ImageTextRemark not in", values, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkBetween(String value1, String value2) {
            addCriterion("ImageTextRemark between", value1, value2, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetextremarkNotBetween(String value1, String value2) {
            addCriterion("ImageTextRemark not between", value1, value2, "imagetextremark");
            return (Criteria) this;
        }

        public Criteria andImagetexturlIsNull() {
            addCriterion("ImageTextURL is null");
            return (Criteria) this;
        }

        public Criteria andImagetexturlIsNotNull() {
            addCriterion("ImageTextURL is not null");
            return (Criteria) this;
        }

        public Criteria andImagetexturlEqualTo(String value) {
            addCriterion("ImageTextURL =", value, "imagetexturl");
            return (Criteria) this;
        }

        public Criteria andImagetexturlNotEqualTo(String value) {
            addCriterion("ImageTextURL <>", value, "imagetexturl");
            return (Criteria) this;
        }

        public Criteria andImagetexturlGreaterThan(String value) {
            addCriterion("ImageTextURL >", value, "imagetexturl");
            return (Criteria) this;
        }

        public Criteria andImagetexturlGreaterThanOrEqualTo(String value) {
            addCriterion("ImageTextURL >=", value, "imagetexturl");
            return (Criteria) this;
        }

        public Criteria andImagetexturlLessThan(String value) {
            addCriterion("ImageTextURL <", value, "imagetexturl");
            return (Criteria) this;
        }

        public Criteria andImagetexturlLessThanOrEqualTo(String value) {
            addCriterion("ImageTextURL <=", value, "imagetexturl");
            return (Criteria) this;
        }

        public Criteria andImagetexturlLike(String value) {
            addCriterion("ImageTextURL like", value, "imagetexturl");
            return (Criteria) this;
        }

        public Criteria andImagetexturlNotLike(String value) {
            addCriterion("ImageTextURL not like", value, "imagetexturl");
            return (Criteria) this;
        }

        public Criteria andImagetexturlIn(List<String> values) {
            addCriterion("ImageTextURL in", values, "imagetexturl");
            return (Criteria) this;
        }

        public Criteria andImagetexturlNotIn(List<String> values) {
            addCriterion("ImageTextURL not in", values, "imagetexturl");
            return (Criteria) this;
        }

        public Criteria andImagetexturlBetween(String value1, String value2) {
            addCriterion("ImageTextURL between", value1, value2, "imagetexturl");
            return (Criteria) this;
        }

        public Criteria andImagetexturlNotBetween(String value1, String value2) {
            addCriterion("ImageTextURL not between", value1, value2, "imagetexturl");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}