/**
 * Project Name:liz-common-utils
 * File Name:GeneralResult.java
 * Package Name:com.yy.cloud.common.dto
 * Date:Jul 11, 20164:25:52 PM
 * Copyright (c) 2016, chenxj All Rights Reserved.
 *
*/

package cc.gemii.data;

import java.io.Serializable;

import com.github.pagehelper.PageInfo;

/**
 * ClassName:GeneralPagingResult <br/>
 * Function: 带分页结果数据的返回结果. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     Jul 11, 2016 4:25:52 PM <br/>
 * @author   chenxj
 * @version  
 * @since    JDK 1.8
 * @see 	 
 */
public class GeneralPagingResult<T> implements Serializable {

	/**
	 * serialVersionUID:TODO Description.
	 */
	private static final long serialVersionUID = 1540315626080625718L;

	private String resultCode;
	private String detailDescription;
	/**
	 * pageInfo: For paging result ONLY.
	 */
	private PageInfo<T> pageInfo;
	
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getDetailDescription() {
		return detailDescription;
	}
	public void setDetailDescription(String detailDescription) {
		this.detailDescription = detailDescription;
	}
	public PageInfo<T> getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(PageInfo<T> pageInfo) {
		this.pageInfo = pageInfo;
	}
}

