/**
 * Project Name:liz-mgadapter
 * File Name:ShortMessage.java
 * Package Name:com.gemii.lizcloud.core.mgadapter.data.dto
 * Date:2017年5月17日下午2:30:42
 * Copyright (c) 2017, yukang All Rights Reserved.
 *
*/

package cc.gemii.data;

/**
 * ClassName:ShortMessage <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2017年5月17日 下午2:30:42 <br/>
 * @author   yk
 * @version  
 * @since    JDK 1.8
 * @see 	 
 */
public class ShortMessage {

	private String reciverPhone;
	private String content;
	private String code;
	private String templateid;
	
	public String getReciverPhone() {
		return reciverPhone;
	}
	public void setReciverPhone(String reciverPhone) {
		this.reciverPhone = reciverPhone;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTemplateid() {
		return templateid;
	}
	public void setTemplateid(String templateid) {
		this.templateid = templateid;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
}

