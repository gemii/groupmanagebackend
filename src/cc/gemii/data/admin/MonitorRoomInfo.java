package cc.gemii.data.admin;

public class MonitorRoomInfo {
	
	private String monitorRoomId;

	private String roomId;
	
	private String innerId;
	
	private String roomName;
	
	private String owner;
	
	private String monitorId;
	
	private String monitorUserName;
	
	public String getMonitorRoomId() {
		return monitorRoomId;
	}

	public void setMonitorRoomId(String monitorRoomId) {
		this.monitorRoomId = monitorRoomId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getInnerId() {
		return innerId;
	}

	public void setInnerId(String innerId) {
		this.innerId = innerId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(String monitorId) {
		this.monitorId = monitorId;
	}

	public String getMonitorUserName() {
		return monitorUserName;
	}

	public void setMonitorUserName(String monitorUserName) {
		this.monitorUserName = monitorUserName;
	}
	
	
	
}
