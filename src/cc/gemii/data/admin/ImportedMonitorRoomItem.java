package cc.gemii.data.admin;

public class ImportedMonitorRoomItem {

	/*
	 * 用于确定群的字段
	 * */
	private String roomId;
	private String innerId;
	private String roomName;
	/*
	 * 用于确定班长的字段
	 * */
	private String monitorId;
	private String userName;
	private String name;
	
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public String getInnerId() {
		return innerId;
	}
	public void setInnerId(String innerId) {
		this.innerId = innerId;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getMonitorId() {
		return monitorId;
	}
	public void setMonitorId(String monitorId) {
		this.monitorId = monitorId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "ImportedMonitorRoomItem [roomId=" + roomId + ", innerId=" + innerId + ", roomName=" + roomName
				+ ", monitorId=" + monitorId + ", userName=" + userName + ", name=" + name + "]";
	}
	
	
}
