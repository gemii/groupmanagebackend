/**
 * Project Name:liz-common-utils
 * File Name:GeneralContentRsult.java
 * Package Name:com.gemii.lizcloud.common.data
 * Date:Oct 17, 20161:09:15 PM
 * Copyright (c) 2016, chenxj All Rights Reserved.
 *
*/

package cc.gemii.data;

import java.io.Serializable;

/**
 * ClassName:GeneralContentRsult <br/>
 * Function: 带普通结果数据的返回结果. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     Oct 17, 2016 1:09:15 PM <br/>
 * @author   chenxj
 * @version  
 * @since    JDK 1.8
 * @see 	 
 */
public class GeneralContentResult<T> implements Serializable {

	/**
	 * serialVersionUID:TODO Description.
	 */
	private static final long serialVersionUID = -8104955278209569617L;

	private String resultCode;
	private String detailDescription;
	private T resultContent;
	
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getDetailDescription() {
		return detailDescription;
	}
	public void setDetailDescription(String detailDescription) {
		this.detailDescription = detailDescription;
	}
	public T getResultContent() {
		return resultContent;
	}
	public void setResultContent(T resultContent) {
		this.resultContent = resultContent;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}

