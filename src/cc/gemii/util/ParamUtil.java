package cc.gemii.util;

public class ParamUtil {

	//add robot url
	public static final String START_ROBOT_URL = "http://localhost:8082/cmd?act=start&bot=BOT&new=NEW";
	
	//add robot response : start success
	public static final String START_ROBOT_SUCCESS = "0";
	
	//redis : login
	public static final String ITEM_LOGIN = "login";
	
	//redis : subscribe
	public static final String ITEM_SUBSCRIBE = "monitor_subscribe";

	//not found QR code url
	public static final String NOT_FOUND_QRCODE = "未获取到二维码URL";
	
	//message : get last page
	public static final String MESSAGE_LAST = "last";
	
	//message : get next page
	public static final String MESSAGE_NEXT = "next";
	
	//message : get limit
	public static final String MESSAGE_LIMIT = "limit";
	
	//message : get keyword message last
	public static final String MESSAGE_KEYWORD = "keyword";
	
	//message : get @admin message last
	public static final String MESSAGE_ADMIN = "admin";
	
	//message : pageSize
	public static final Integer MESSAGE_PAGESIZE = 20;
	
	//redis MsgType
	public static final String ADD_CHANNEL = "add";
	
	public static final String REMOVE_CHANNEL = "remove";
	
	public static final String SEND_MSG_RESPONSE = "200001";
	
	public static final String INIT_SUBSCRIBE = "init_subscribe";
	
	//ping pong
	public static final String WEBSOCKET_PING = "PING";
	
	public static final String WEBSOCKET_PONG = "PONG";
	
	//add robot tag
	public static final String ADD_ROBOT_TAG = "NextTag";
	
	//S3 dir
	public static final String MICRO_TASK_UPLOAD_DIR = "microtask";
	
	public static final String MONITOR_SEND_DIR = "monitors";
	
	public static  final String HAOQI_KEYWORDS_SWITCH = "haoqi_keywords_switch";
}
