package cc.gemii.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;


public class WechatUtil {

	private static Logger logger = Logger.getLogger(WechatUtil.class);

	public static String getSignature(Map<String,String> map){
		
		List<String> strList = new ArrayList<String>(map.size());
		for (String key : map.keySet()) {
			strList.add(key+"="+map.get(key));
		}
		Object[] strArray = (Object[]) strList.toArray();
		Arrays.sort(strArray);
		StringBuffer sb = new StringBuffer();
	    for (Object string : strArray) {
	    	sb.append((String)string+"&");
		}
	    logger.info(sb.toString().substring(0,sb.length()-1));
		return MD5Utils.md5Encode(sb.toString().substring(0,sb.length()-1)).toLowerCase();
	}
	
	public static String getTimestamp(){
		return String.valueOf(Calendar.getInstance().getTimeInMillis());
	}
	
	public static void main(String[] args) {
		Map<String,String> map = new HashMap<String,String>();
		map.put("key1","value1");
		map.put("key2", "value2");
		logger.info(WechatUtil.getSignature(map)); 
	}
}
