package cc.gemii.util;

/**
 * 
 * @author gemii.yangyang
 *
 */
public class ReflectionUtils {

	public static String createGetMethodName(String methodName) {
		String method ="get"+methodName.substring(0,1).toUpperCase()+methodName.substring(1);
		return method;
	}

	public static String createSetMethodName(String methodName) {
		String method ="set"+methodName.substring(0,1).toUpperCase()+methodName.substring(1);
		return method;
	}

}
