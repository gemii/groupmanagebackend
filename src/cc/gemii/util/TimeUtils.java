package cc.gemii.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

public class TimeUtils {

	private static Logger logger = Logger.getLogger(TimeUtils.class);
	/**
	 * 
	 * @param timestamp
	 *            时间戳
	 * @param type
	 *            类型
	 * @return TODO
	 */
	public static Date getDate(String timestamp, String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);// 设置日期格式
		try {
			return df.parse(timestamp);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param 时间类型
	 *            如：yyyy-mm-dd HH:mm:ss
	 * @return 格式化后的当前时间
	 */
	public static String getTime(String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);// 设置日期格式
		return df.format(new Date());
	}

	/**
	 * 
	 * @param date
	 *            日期
	 * @param type
	 *            类型
	 * @return TODO
	 */
	public static String getTime(Date date, String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);// 设置日期格式
		return df.format(date);
	}

	public static Date getDate(String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);// 设置日期格式
		try {
			return df.parse(df.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String timestamp2date(String timestamp, String fromType,
			String toType) {
		SimpleDateFormat fromsdf = new SimpleDateFormat(fromType);// 设置日期格式
		SimpleDateFormat tosdf = new SimpleDateFormat(toType);// 设置日期格式
		try {
			return tosdf.format(fromsdf.parse(timestamp));
		} catch (ParseException e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 
	 * @Title 获取时间差
	 * @param 要比较的时间
	 * @return 该时间距离今天还有多少天 负数代表该天已经过去
	 * @throws
	 */
	public static long dateDiff(Date date) {
		Date date1 = TimeUtils.getDate("yyyy-MM-dd HH:mm:ss");
		long l = date.getTime() - date1.getTime();
		long day = l / (24 * 60 * 60 * 1000);
		return day;
	}

	/**
	 * 
	 * @param date_string
	 * @return TODO yyyyMMddHH:mm:ss to yyyy-MM-dd HH:mm:ss
	 */
	public static String patternConvert(String date_string) {
		try {
			SimpleDateFormat sdf_start = new SimpleDateFormat("yyyyMMddHHmmss");
			SimpleDateFormat sdf_end = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			return sdf_end.format(sdf_start.parse(date_string));
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 
	 * @param timestamp
	 *            格式yyyy-MM-dd HH:mm:ss
	 * @return TODO
	 */
	public static String getDayStart(String timestamp) {
		return timestamp2date(timestamp, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd") + " 00:00:00";
	}

	public static String getDayEnd(String timestamp) {
		return timestamp2date(timestamp, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd")
				+ " 23:59:59";
	}

	/**
	 * 
	 * @param timestamp 时间戳
	 * @return
	 * TODO 获取时间戳前一天的零点
	 */
	public static String getXDayStart(String timestamp, int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(timestamp == null ? new Date() : TimeUtils.getDate(timestamp, "yyyy-MM-dd HH:mm:ss"));
		calendar.add(Calendar.DATE, day);
		return getDayStart(getTime(calendar.getTime(), "yyyy-MM-dd HH:mm:ss"));
	}

	/**
	 * 
	 * @return
	 * TODO 获取当前时间前一天的零点
	 */
	public static String getToDayStart() {
		return getXDayStart(null, 0);
	}

	/**
	 * 
	 * @param x 天数
	 * @return 
	 * TODO 获取当前时间前x天的开始时间
	 */
	public static String getXDayStart(int x){
		return getXDayStart(null, x);
	}
	
	/**
	 * 
	 * @param timestamp 时间戳
	 * @return
	 * TODO 获取时间戳前一天的结束时间
	 */
	public static String getXDayEnd(String timestamp, int day){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(timestamp == null ? new Date() : TimeUtils.getDate(timestamp, "yyyy-MM-dd HH:mm:ss"));
		calendar.add(Calendar.DATE, day);
		return getDayEnd(getTime(calendar.getTime(), "yyyy-MM-dd HH:mm:ss"));
	}
	
	/**
	 * 
	 * @return
	 * TODO 获取当前时间前一天的结束时间
	 */
	public static String getLastDayEnd(){
		return getXDayEnd(null, -1);
	}

	/**
	 * 
	 * @param timestamp 时间戳
	 * @param type 类型
	 * TODO 验证格式
	 */
	public static boolean validateFormat(String timestamp, String type) {
		SimpleDateFormat sdf = new SimpleDateFormat(type);
		try {
			sdf.parse(timestamp);
			return true;
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 
	 * @param count 上几周
	 * @return
	 * TODO 获取上count周的周日的日期,格式为type
	 */
	public static String getCountWeekSunday(int count, String type){
		SimpleDateFormat sdf = new SimpleDateFormat(type);
		Calendar date = Calendar.getInstance(Locale.CHINA);
		date.setFirstDayOfWeek(Calendar.MONDAY);
		date.add(Calendar.WEEK_OF_MONTH, count);
		date.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		return sdf.format(date.getTime());
	}
	
}
