package cc.gemii.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jets3t.service.S3Service;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;
import org.springframework.web.multipart.MultipartFile;

import cc.gemii.pojo.ExcelTemplate;

public class FileUtil {

	private static String awsAccessKey;
	private static String awsSecreyKey;
	private static String defaultBucketName = "nfs.gemii.cc";
	private static String url_prex = "http://nfs.gemii.cc/";
	
	public FileUtil(String accessKey,String secreyKey){
		this.awsAccessKey = accessKey;
		this.awsSecreyKey = secreyKey;
	}
	
	/**
	 * 
	 * @param bytes
	 * @return
	 * TODO 上传图片
	 */
	public static String uploadFile(String bucketName, String dir, String filename, byte[] bytes) {
		try {
			String dir_filename = (dir == null?"":(dir + "/")) + filename;
			AWSCredentials awsCredentials = new AWSCredentials(awsAccessKey,awsSecreyKey);
			S3Service s3Service = new RestS3Service(awsCredentials);
			S3Bucket s3Bucket = s3Service.getBucket(bucketName == null?defaultBucketName : bucketName);
			S3Object s3Object = new S3Object(dir_filename);
			ByteArrayInputStream greetingIS = new ByteArrayInputStream(bytes);
			s3Object.setDataInputStream(greetingIS);
			s3Object.setContentLength(bytes.length);
			s3Service.putObject(s3Bucket, s3Object);
			// return url
			return url_prex + dir_filename;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String uploadFile(String dir, String filename, byte[] bytes){
		return uploadFile(null, dir, filename, bytes);
	}
	
	public static String uploadFile(String filename, byte[] bytes){
		return uploadFile(null, null, filename, bytes);
	}
	
	/**
	 * 
	 * @param file 文件
	 * @return
	 * TODO 文件转字节数组
	 */
	public static byte[] getBytesFromFile(File file) {
		byte[] bytes = null;
		try {
			InputStream is = new FileInputStream(file);
			// 获取文件大小
			long length = file.length();
			if (length > Integer.MAX_VALUE) {
				// 文件太大，无法读取
				throw new IOException("File is to large " + file.getName());
			}
			// 创建一个数据来保存文件数据
			bytes = new byte[(int) length];
			// 读取数据到byte数组中
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length
					&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}
			// 确保所有数据均被读取
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file " + file.getName());
			}
			// Close the input stream and return bytes
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bytes;
	}
	
	/**
	 * 
	 * @param b 字节数组
	 * @param outputFile 输出文件path
	 * @return 字节数组转文件
	 * TODO
	 */
	public static File getFileFromBytes(byte[] b, String outputFile) {    
        File ret = null;    
        BufferedOutputStream stream = null;    
        try {    
            ret = new File(outputFile);    
            FileOutputStream fstream = new FileOutputStream(ret);    
            stream = new BufferedOutputStream(fstream);    
            stream.write(b);    
        } catch (Exception e) {    
            // log.error("helper:get file from byte process error!");    
            e.printStackTrace();    
        } finally {    
            if (stream != null) {    
                try {    
                    stream.close();    
                } catch (IOException e) {    
                    // log.error("helper:get file from byte process error!");    
                    e.printStackTrace();    
                }    
            }    
        }    
        return ret;    
    }

	/**
	 * excel文件上传
	 * @param file
	 * @return
	 * @throws Exception 
	 */
	public static List<ExcelTemplate> uploadExcel(MultipartFile file) throws Exception {
		List<ExcelTemplate> list =  new ArrayList<ExcelTemplate>();
		//读取excel文件内容
		try {
			InputStream inputStream = file.getInputStream();
			//excel版本判断
			//judgeExcelVersion(inputStream);
			//list = read2003Excel(inputStream);//excel2003版
			list = read2007Excel(inputStream);//excel2007版
		} catch (IOException e) {
			//System.out.println("---------------excel文件读取异常-----------------");
			throw new Exception("excel文件读取异常!");
		}
		return list;
	}
	
	/**
	 * excel2007读取
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	private static List<ExcelTemplate> read2007Excel(InputStream inputStream) throws IOException {
		List<ExcelTemplate> list =  new ArrayList<ExcelTemplate>();
		XSSFWorkbook xsb = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = xsb.getSheetAt(0);
		int firstRowNum = sheet.getFirstRowNum();
		int endRowNum = sheet.getLastRowNum();
		XSSFRow nowRow = null;
		ExcelTemplate template =null;
		for(int i=firstRowNum+1; i<=endRowNum; i++){//从第二行开始读取Excel文件
			template = new ExcelTemplate();
			nowRow = sheet.getRow(i);
			if (nowRow == null) {  
                continue;  
            }
			for(int j=nowRow.getFirstCellNum(); j<nowRow.getLastCellNum(); j++){//读取每行的每个单元格
				XSSFCell cell = nowRow.getCell(j);
				//将单元格数据的转为string
				String value = transformToString(cell);
				switch(j){
				case 0:
					template.setRoomId(value);
					break;
				case 1:
					template.setRoomName(value);
					break;
				case 2:
					template.setUsername(value);
					break;
				case 3:
					template.setCity(value);
					break;
				case 4:
					template.setMemberNums(value);
					break;
					default:
						System.out.println("模版不正确,请上传正确的模版！");
						break;
				}
			}
			list.add(template);
		}
		return list;
	}

	/**
	 * 转化为String
	 * @param cell
	 * @return
	 */
	private static String transformToString(XSSFCell cell) {
		String value = null;
		DecimalFormat df = new DecimalFormat("0");// 格式化 number String  
        switch (cell.getCellType()) {  
        case XSSFCell.CELL_TYPE_STRING:  
            value = cell.getStringCellValue().trim();  
            break;  
        case XSSFCell.CELL_TYPE_NUMERIC:  
            value = df.format(cell.getNumericCellValue());  
            break;  
        case XSSFCell.CELL_TYPE_BLANK:  
            value = "";  
            break;  
        default:  
            value = cell.toString();  
        }  
		return value;
	}  
	
}
