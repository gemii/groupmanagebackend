package cc.gemii.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import cc.gemii.pojo.LoveBabyInsert;


public class FileUpUtil {
	
	/**
	 * excel文件上传
	 * @param file
	 * @return
	 * @throws Exception 
	 */
	public static List<LoveBabyInsert> uploadExcel(MultipartFile file) throws Exception {
		List<LoveBabyInsert> list =  new ArrayList<LoveBabyInsert>();
		//读取excel文件内容
		try {
			InputStream inputStream = file.getInputStream();
			list=readExcel(inputStream);
		} catch (IOException e) {
			//System.out.println("---------------excel文件读取异常-----------------");
			throw new Exception("excel文件读取异常!");
		}
		return list;
	}
	
	
	public static List<LoveBabyInsert> readExcel(InputStream inputStream) throws IOException{
		List<LoveBabyInsert> list =new ArrayList<LoveBabyInsert>();
		XSSFWorkbook xsb = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = xsb.getSheetAt(0);
		int firstRowNum = sheet.getFirstRowNum();
		int endRowNum = sheet.getLastRowNum();
		XSSFRow nowRow = null;
		LoveBabyInsert tInsert=null;
		for (int i=firstRowNum+1; i<=endRowNum; i++) {
			tInsert=new LoveBabyInsert();
			nowRow=sheet.getRow(i);
			if(nowRow == null){
				continue;
			}
			for (int j=nowRow.getFirstCellNum(); j<nowRow.getLastCellNum(); j++) {
				XSSFCell cell=nowRow.getCell(j);
				String value;
				if(cell==null){
					value="";
				}else {
					value=transformToString(cell);
				}
				switch (j) {
				case 0:
					tInsert.setNcCode(value);
					break;
				case 1:
					tInsert.setStoreName(value);
					break;
				case 2:
					tInsert.setProvince(value);
					break;
				case 3:
					tInsert.setCity(value);
					break;
				case 4:
					tInsert.setRoomName(value);
					break;
				case 5:
					tInsert.setId(value);
					break;
				default:
					System.out.println("不需要此列信息");
					break;
				}
			}
			list.add(tInsert);
		}
		return list;
		
	}
	
	/**
	 * 转化为String
	 * @param cell
	 * @return
	 */
	private static String transformToString(XSSFCell cell) {
		String value = null;
		DecimalFormat df = new DecimalFormat("0");// 格式化 number String  
        switch (cell.getCellType()) {  
        case XSSFCell.CELL_TYPE_STRING:  
            value = cell.getStringCellValue().trim();  
            break;  
        case XSSFCell.CELL_TYPE_NUMERIC:  
            value = df.format(cell.getNumericCellValue());  
            break;  
        case XSSFCell.CELL_TYPE_BLANK:  
            value = "";  
            break;  
        default:  
            value = cell.toString();  
        }  
		return value;
	}  
	

}
